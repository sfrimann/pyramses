#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, absolute_import, division

from .errors import check_sentinels
from .utils import detect_single_double, rd_time, convert_units
from .sink import rsink
from random import randint
from subprocess import Popen, PIPE

try:
  from astropy.io import fits as pyfits
except:
  import pyfits

from . import constants as c
import numpy as np
import os

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def get_cube(*args, **kw):
  """
  get_cube is top level routine for getting RAMSES cube data.
  Returns dictionary of relevant data.
  Takes noutput and conversion factors as input
  The function also takes all keywords accepted by make_cube
  """

  # ------- Handle keywords
  deltar         = kw.get('deltar',[1.0, 1.0, 1.0]) # alias
  dr             = kw.get('dr',deltar)              # whole box
  dryrun         = kw.get('dryrun',False)           # just print command
  jet_factor     = kw.get('jet_factor',0.5)         # mass loss factor
  lmax           = kw.get('lmax',6)                 # alias
  lvlmax         = kw.get('lvlmax',lmax)            # amr level
  main_directory = kw.get('main_directory','.')     # alias
  datadir        = kw.get('datadir',main_directory) # data directory for snapshots
  sink_number    = kw.get('sink_number',False)      # sink number
  scales_file    = kw.get('scales_file','./scales.fits')
  r              = kw.get('r',[0.5,0.5,0.5])        # centre of box. Suppressed if sink_number is active
  do_velocity    = kw.get('do_velocity',False)

  # ------- Check arguments
  if len(args) == 1:
    nout    = args[0]
    try:
      header = pyfits.getheader(scales_file)
      physical_units = True
      scale_d, scale_l, scale_t, scale_m = header['scale_d'], header['scale_l'], header['scale_t'], header['scale_m']
    except IOError:
      scale_d = 1.
      scale_l = 1. 
      scale_t = 1.
      scale_m = 1.
      physical_units = False
      print("get_cube() in cube.py * Warning: Conversion scales are not given. I will not convert to physical units")
  elif len(args) == 2 and len(args[1]) == 4:
    nout    = args[0]
    scale_d = args[1][0]
    scale_l = args[1][1]
    scale_t = args[1][2]
    scale_m = args[1][3]
    physical_units = True
  elif len(args) == 5:
    nout    = args[0]
    scale_d = args[1]
    scale_l = args[2]
    scale_t = args[3]
    scale_m = args[4]
    physical_units = True
  else:
    print("syntax: result = get_cube(nout, keywords=values)")
    print("        result = get_cube(nout, scales, keywords=values) where scales = [scale_d, scale_l, scale_t, scale_m]")
    print("        result = get_cube(nout, scale_d, scale_l, scale_t, scale_m, keywords=values)")
    raise IOError("Wrong number of arguments")
  
  # ------- expand datadir
  datadir = os.path.abspath(os.path.expanduser(datadir))
  
  # ------- Handle dr
  # if dr is string extract length and convert
  # examples are dr = '11e22 cm', dr = '20000 au', dr = '0.2 parsec'
  if isinstance(dr,str):
    if physical_units is False:
      raise ValueError('dr cannot be in physical units when we work in numerical units')
    dr, unit = dr.lower().split(' ')
    dr = float(dr) # convert to number
    if not unit in ['cm', 'au', 'parsec']:
      raise IOError("length unit %s not in list of allowed units (cm, au, parsec)" % unit)
    if unit == 'cm':
      dr /= scale_l
    elif unit == 'au':
      dr  = dr * c.au_to_cm / scale_l
    elif unit == 'parsec':
      dr  = dr * c.parsec_to_cm / scale_l
   
  # if dr is scalar make it a three element list
  if isinstance(dr,float):
    dr = [dr, dr, dr]
  
  # check if any elements in dr is greater than box length
  if any([i > 1 for i in dr]):
    raise IOError("One or more elements in dr greater than box size")

  # ------- sink particles
  # read sink particles
  sink          = rsink(nout,datadir=datadir)
  snapshot_time = sink['snapshot_time']*scale_t
  sink_age      = snapshot_time - sink['tcreate']*scale_t
  sink_mass     = sink['m'] * scale_m * jet_factor
  if 'tflush' in sink:
    dmdt = np.array([np.log10(dm) if dm > 0 else -np.inf for dm in sink['dm']]) + np.log10(scale_m) + np.log10(jet_factor) - np.log10(snapshot_time - sink['tflush']*scale_t)
  else:
    dmdt = False # you have to do the derivative yourself

  # new centre if sink_number is set
  if sink_number is not False:
    if sink_mass[sink_number] < 0:
      raise ValueError("sink %i has already exploded as a super nova" % sink_number)
    r = [sink['x'][sink_number], sink['y'][sink_number], sink['z'][sink_number]]
  
  # ------- sink coordinates relative to cutout centre
  sinkr = np.vstack((sink['x']-r[0],sink['y']-r[1],sink['z']-r[2])).transpose()
  
  # rearrange sinks around cutout centre
  index = np.where(sinkr < -0.5)
  sinkr[index] += 1
  index = np.where(sinkr > 0.5)
  sinkr[index] -= 1
  
  # ------- Determine which sink particles are inside cut-out
  sink_index = np.arange(sinkr.shape[0])
  sinks_inside_cutout = sink_index[(np.abs(sinkr[:,0]) <= dr[0]/2.) & \
                                   (np.abs(sinkr[:,1]) <= dr[1]/2.) & \
                                   (np.abs(sinkr[:,2]) <= dr[2]/2.)]
  if len(sinks_inside_cutout) == 0:
    sinks_inside_cutout = False
  else:
    sinks_inside_cutout = np.asarray(sinks_inside_cutout)

  # ------- make cube
  kw['r']  = r
  kw['dr'] = dr
  density, offset = make_cube(nout, **kw)
  if do_velocity:
    kw['silent'] = True
    vx, offset = make_cube(nout, type=2, **kw)
    vy, offset = make_cube(nout, type=3, **kw)
    vz, offset = make_cube(nout, type=4, **kw)
  
  # ------- if dryrun just return
  if dryrun is True:
    return density
  
  # ------- convert data to physical units
  density *= scale_d
  sinkr   *= scale_l
  if do_velocity:
    vx      *= scale_l/scale_t
    vy      *= scale_l/scale_t
    vz      *= scale_l/scale_t
  else:
    vx, vy, vz = False, False, False
  
  # cutout centre in data units
  r[0] *= scale_l
  r[1] *= scale_l
  r[2] *= scale_l

  # ------- projection directions
  npix   = 2**lvlmax
  xrange = ((np.arange(offset[0],offset[1]+1)-0.5)/npix*scale_l - r[0])
  yrange = ((np.arange(offset[2],offset[3]+1)-0.5)/npix*scale_l - r[1])
  zrange = ((np.arange(offset[4],offset[5]+1)-0.5)/npix*scale_l - r[2])
  
  # ------- Write dictionary and return
  cube = {'datadir'            : datadir, \
          'density'            : density, \
          'dictype'            : 'cube',\
          'jet_factor'         : jet_factor, \
          'lvlmax'             : lvlmax, \
          'nout'               : nout, \
          'original_centre'    : r, \
          'physical_units'     : physical_units, \
          'sinkr'              : sinkr, \
          'sink_age'           : sink_age, \
          'sink_mass'          : sink_mass, \
          'sink_number'        : sink_number, \
          'sinks_inside_cutout': sinks_inside_cutout, \
          'time'               : snapshot_time, \
          'vx'                 : vx, \
          'vy'                 : vy, \
          'vz'                 : vz, \
          'xrange'             : xrange, \
          'yrange'             : yrange, \
          'zrange'             : zrange}
  
  return cube
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def make_cube(nout, **kw):
  """
  make_cube parses and calls amr2cube fortran command, reads the resulting file, and returns
  map
  types =   -1, 1,  2,  3,  4, -5, -6, -7, 11,  -11
  names = lgd , d, vx, vy, vz, bx, by, bz, pg, temp
  """
  # ------- Handle keywords
  #cyclic         = kw.get('cyclic',True)            # if true stitch boundaries together
  r              = kw.get('r',[0.5, 0.5, 0.5])      # centre of box
  deltar         = kw.get('deltar',[1.0, 1.0, 1.0]) # alias
  dr             = kw.get('dr',deltar)              # whole box
  lmax           = kw.get('lmax', 6)                # alias
  lvlmax         = kw.get('lvlmax', lmax)           # amr level
  dryrun         = kw.get('dryrun', False)          # print command
  shm            = kw.get('shm', True)              # binary location
  tmp            = kw.get('tmp', False)             # binary location
  main_directory = kw.get('main_directory','.')     # alias
  datadir        = kw.get('datadir',main_directory) # data directory for snapshots
  exe            = kw.get('exe', None)              # name of fortran executable. Autodetected if None
  quiet          = kw.get('quiet', False)           # alias
  silent         = kw.get('silent', quiet)          # supress output from amr2cell
  tempdir        = kw.get('tempdir',None)
  type           = kw.get('type', 1)                # data type. Default = 1 (density)
  
  assert len(dr) == 3, "dr must contain three items"
  assert len(r) == 3, "r must contain three items"
  
  # ------- Handle directories and files
  cube_dir = '/dev/shm' if shm is True else '.'
  if tmp is True: cell_dir = '/tmp'
  if isinstance(tempdir,str): cell_dir = tempdir
  
  cube_file = os.path.join(cube_dir,'temp_cube'+str(randint(10000,99999)))
  datadir   = os.path.abspath(os.path.expanduser(datadir))
  inp       = os.path.join(datadir,'output_'+str(nout).zfill(5))
  
  # At present I assume the name of the executable, and that it is available in path.
  # A future update will search for the executable if not given
  if exe is None:
    lexe = 'amr2cube'
  else:
    lexe = exe
  
  # ------- Begin parsing fortran command
  executable  = lexe
  executable += ' -inp ' + inp
  executable += ' -out ' + cube_file
  executable += ' -xmi ' + str(r[0] - dr[0]/2.) + ' -xma ' + str(r[0]+dr[0]/2.)
  executable += ' -ymi ' + str(r[1] - dr[1]/2.) + ' -yma ' + str(r[1]+dr[1]/2.)
  executable += ' -zmi ' + str(r[2] - dr[2]/2.) + ' -zma ' + str(r[2]+dr[2]/2.)
  executable += ' -lma ' + str(lvlmax)
  executable += ' -typ ' + str(type)
  executable += ' -fil bin'
  executable += ' -blo on'
  
  # if dryrun only return command string
  if dryrun is True:
    print(executable)
    return executable, None
  
  # if silent suppress output
  proc = Popen(executable.split(), stdout=PIPE)
  proc.wait()
  output = proc.stdout.read()
  proc.stdout.close()
  if silent is False:
    print(output)
  
  # check if command ran properly
  if proc.returncode != 0:
    raise IOError('Something happened while running Fortran command. Exit status: %i' % proc.returncode)
  
  try:
    # unpack binary file
    cube = rd_cube(cube_file+'.01')
  finally:
    # delete binary file
    os.remove(cube_file+'.01')
  
  # determine offset
  offset = output.split('\n')[5].split()
  offset = [int(offset[2][:-1]),int(offset[3]),\
            int(offset[4][:-1]),int(offset[5]),\
            int(offset[6][:-1]),int(offset[7])]
  
  return cube, offset
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def rd_cube(filename):
  """
  Reads RAMSES binary cube file as output by amr2cube, given the filename as input.
  """
  
  # open file
  f = open(filename, mode='rb')
  
  try:
    ########## read header begin ##########
    dt = np.dtype('u4, i4, i4, i4, u4')
    sentinel1, nx, ny, nz, sentinel2 = np.fromfile(f,dtype=dt,count=1)[0]
    void = check_sentinels(sentinel1,sentinel2)
    ########## read header end ##########
  
    ########## start reading data ##########
    dt = np.dtype('u4, (%i,)f4 , u4' % (nx*ny*nz))
    
    # read data record
    sentinel1, cube, sentinel2 = np.fromfile(f,dtype=dt,count=1)[0]
    void = check_sentinels(sentinel1,sentinel2)
  finally:
    # close file
    f.close()
  
  #reshape cube
  cube = cube.reshape((nz, ny, nx))
  
  return cube
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#