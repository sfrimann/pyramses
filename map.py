#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, absolute_import, division

from .errors import check_sentinels
from .utils import detect_single_double, rd_time, convert_units
from .sink import rsink
from . import constants as c
import time

try:
  from astropy.io import fits as pyfits
except:
  import pyfits

import numpy as np
import os
import random
import subprocess
#import pyradmc3d as pyrad

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def get_map(*args, **kw):
  """
  get_map is top level routine for making maps from RAMSES data.
  Returns dictionary of relevant data.
  Takes noutput and conversion factors as input
  The function also takes all keywords accepted by make_mape
  """
  # ------- Handle keywords
  deltar         = kw.get('deltar',[1.0, 1.0, 1.0]) # alias
  dr             = kw.get('dr',deltar)              # whole box
  dryrun         = kw.get('dryrun', False)          # print command
  dir            = kw.get('dir','z')                # projection direction
  jet_factor     = kw.get('jet_factor',None)        # bipolar jet mass loss factor
  lmax           = kw.get('lmax',None)              # alias
  lvlmax         = kw.get('lvlmax',lmax)            # amr level
  main_directory = kw.get('main_directory','.')     # alias
  datadir        = kw.get('datadir',main_directory) # data directory for snapshots
  scales_file    = kw.get('scales_file','./scales.fits')
  sink_number    = kw.get('sink_number',None)       # sink number
  r              = kw.get('r',[0.5,0.5,0.5])        # centre of box. Suppressed if sink_number is active
  dryrun         = kw.get('dryrun',False)           # print fortran command instead of running it
  
  # ------- Check arguments
  if len(args) == 1:
    nout    = args[0]
    try:
      header = pyfits.getheader(scales_file)
      physical_units = True
      scale_d, scale_l, scale_t, scale_m = header['scale_d'], header['scale_l'], header['scale_t'], header['scale_m']
      jet_factor = header['jet_factor'] if jet_factor is None else jet_factor
      lvlmax = header['lvlmax'] if lvlmax is None else lvlmax
      kw['lvlmax'] = header['lvlmax'] if lvlmax is None else lvlmax
    except IOError:
      scale_d = 1.
      scale_l = 1. 
      scale_t = 1.
      scale_m = 1.
      physical_units = False
      print("get_map() in cell.py * Warning: Conversion scales are not given. I will not convert to physical units")
  elif len(args) == 2 and len(args[1]) == 4:
    nout    = args[0]
    scale_d = args[1][0]
    scale_l = args[1][1]
    scale_t = args[1][2]
    scale_m = args[1][3]
    physical_units = True
  elif len(args) == 5:
    nout    = args[0]
    scale_d = args[1]
    scale_l = args[2]
    scale_t = args[3]
    scale_m = args[4]
    physical_units = True
  else:
    print("syntax: result = get_map(nout, keywords=values)")
    print("        result = get_map(nout, scales, keywords=values) where scales = [scale_d, scale_l, scale_t, scale_m]")
    print("        result = get_map(nout, scale_d, scale_l, scale_t, scale_m, keywords=values)")
    raise IOError("Wrong number of arguments")
  
  jet_factor = 1. if jet_factor is None else jet_factor
  lvlmax = 6 if lvlmax is None else lvlmax
  
  # ------- expand datadir
  datadir = os.path.abspath(os.path.expanduser(datadir))
  
  # ------- Handle dr
  # if dr is string extract length and convert
  # examples are dr = '11e22 cm', dr = '20000 au', dr = '0.2 parsec'
  if isinstance(dr,str):
    if physical_units is False:
      raise ValueError('dr cannot be in physical units when we work in numerical units')
    dr, unit = dr.lower().split(' ')
    dr = float(dr) # convert to number
    if not unit in ['cm', 'au', 'parsec']:
      raise IOError("length unit %s not in list of allowed units (cm, au, parsec)" % unit)
    if unit == 'cm':
      dr /= scale_l
    elif unit == 'au':
      dr  = dr * c.au_to_cm / scale_l
    elif unit == 'parsec':
      dr  = dr * c.parsec_to_cm / scale_l
   
  # if dr is scalar make it a three element list
  if isinstance(dr,float):
    dr = [dr, dr, dr]
  
  # check if any elements in dr is greater than box length
  if any([i > 1 for i in dr]):
    raise IOError("One or more elements in dr greater than box size")

  # ------- sink particles
  # read sink particles
  sink      = rsink(nout,datadir=datadir)
  snapshot_time = sink['snapshot_time']*scale_t
  sink_age  = snapshot_time - sink['tcreate']*scale_t
  sink_mass = sink['m'] * scale_m * jet_factor

  # new centre if sink_number is set
  if sink_number is not None:
    r = [sink['x'][sink_number], sink['y'][sink_number], sink['z'][sink_number]]
   
  # create sink coordinates
  sinkx = (sink['x'] - r[0]) # sink coordinates relative to cutout centre
  sinky = (sink['y'] - r[1])
  sinkz = (sink['z'] - r[2])
  
  # rearrange sinks around cutout centre
  sinkx = ((0.5 + sinkx) % 1) - 0.5
  sinky = ((0.5 + sinky) % 1) - 0.5
  sinkz = ((0.5 + sinkz) % 1) - 0.5
  
  #index1, index2 = np.where(sinkx < -0.5), np.where(sinkx > 0.5)
  #sinkx[index1] += 1
  #sinkx[index2] -= 1
  #index1, index2 = np.where(sinky < -0.5), np.where(sinky > 0.5)
  #sinky[index1] += 1
  #sinky[index2] -= 1
  #index1, index2 = np.where(sinkz < -0.5), np.where(sinkz > 0.5)
  #sinkz[index1] += 1
  #sinkz[index2] -= 1

  # ------- Determine which sink particles are inside cut-out
  sink_index = np.arange(len(sinkx))
  sinks_inside_cutout = sink_index[(np.abs(sinkx) <= dr[0]/2.) & \
                                   (np.abs(sinky) <= dr[1]/2.) & \
                                   (np.abs(sinkz) <= dr[2]/2.)]
  if len(sinks_inside_cutout) == 0:
    sinks_inside_cutout = None
  else:
    sinks_inside_cutout = list(sinks_inside_cutout)

  # ------- make map
  kw['r'], kw['dr']  = r, dr
  map = make_map(nout, **kw)
  if dryrun is True:
    return map
  
  # ------- convert map to physical units
  map *= scale_d

  # ------- projection directions
  ny, nx = map.shape
  pxs    = scale_l/2.**lvlmax #pixel size
  lx, ly = (nx-1)*pxs, (ny-1)*pxs
  xrange = np.linspace(-lx/2.,lx/2.,num=nx)
  yrange = np.linspace(-ly/2.,ly/2.,num=ny)
  if dir == 'x':
    ssinkx  = sinky * scale_l
    ssinky  = sinkz * scale_l
    cd_factor = dr[0] * scale_l
  elif dir == 'y':
    ssinkx  = sinkz * scale_l
    ssinky  = sinkx * scale_l
    cd_factor = dr[1] * scale_l
  elif dir == 'z':
    ssinkx  = sinkx * scale_l
    ssinky  = sinky * scale_l
    cd_factor = dr[2] * scale_l
  else:
    raise IOError('Problem with projection direction')

  # cutout centre in data units
  r[0] *= scale_l
  r[1] *= scale_l
  r[2] *= scale_l

  # ------- Write dictionary and return
  map  = {'column_density'     : cd_factor, \
          'datadir'            : datadir, \
          'density'            : map, \
          'dictype'            : 'map', \
          'dir'                : dir, \
          'jet_factor'         : jet_factor, \
          'lvlmax'             : lvlmax, \
          'nout'               : nout, \
          'original_centre'    : r, \
          'physical_units'     : physical_units, \
          'sinkx'              : ssinkx, \
          'sinky'              : ssinky, \
          'sink_age'           : sink_age, \
          'sink_mass'          : sink_mass, \
          'sink_number'        : sink_number, \
          'sinks_inside_cutout': sinks_inside_cutout, \
          'time'               : snapshot_time, \
          'xrange'             : xrange, \
          'yrange'             : yrange}
  
  return map
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def make_map(nout, **kw):
  """
  make_map parses and calls amr2map fortran command, reads the resulting file, and returns
  map
  """
  # ------- Handle keywords
  cyclic         = kw.get('cyclic',True)            # if true stitch boundaries together
  r              = kw.get('r',[0.5, 0.5, 0.5])      # centre of box
  deltar         = kw.get('deltar',[1.0, 1.0, 1.0]) # alias
  dir            = kw.get('dir','z')                # projection direction
  dr             = kw.get('dr',deltar)              # whole box
  lmax           = kw.get('lmax', 6)                # alias
  lvlmax         = kw.get('lvlmax', lmax)           # amr level
  dryrun         = kw.get('dryrun', False)          # print command
  shm            = kw.get('shm', True)              # binary location
  tmp            = kw.get('tmp', False)             # binary location
  main_directory = kw.get('main_directory', '.')    # alias
  datadir        = kw.get('datadir',main_directory) # root directory, looking for snapshots
  exe            = kw.get('exe', None)              # name of fortran executable. Autodetected if None
  quiet          = kw.get('quiet', False)           # alias
  silent         = kw.get('silent', quiet)          # supress output from amr2cell
  do_max         = kw.get('do_max', False)          # print max value along line of sight
  type           = kw.get('type', 1)                # data type. Default = 1 (density)
  
  
  assert len(dr) == 3, "dr must contain three items"
  assert len(r) == 3, "r must contain three items"
  assert dir in ['x', 'y', 'z'], '%s not in list of allowed projection directions' % dir
  
  # ------- Handle directories and files
  map_dir = '/dev/shm' if shm is True else '.'
  if tmp is True: map_dir = '/tmp'
  
  map_file = os.path.join(map_dir,'temp_map'+str(random.randint(1000,9999)))
  datadir  = os.path.abspath(os.path.expanduser(datadir))
  inp      = os.path.join(datadir,'output_'+str(nout).zfill(5))
  
  # At present I assume the name of the executable, and that it is available in path.
  # A future update will search for the executable if not given
  if exe is None:
    lexe = 'amr2map'
  else:
    lexe = exe

  # ------- xyz limits in numerical units
  xlim = [r[0] - dr[0]/2., r[0] + dr[0]/2.]
  ylim = [r[1] - dr[1]/2., r[1] + dr[1]/2.]
  zlim = [r[2] - dr[2]/2., r[2] + dr[2]/2.]
  
  # ------- check if any of the limits go beyond the box, and split limits appropiately
  if cyclic is True and dryrun is False:
    for lim in [xlim, ylim, zlim]:
      if lim[0] < 0:
        lim[0] += 1
        lim.insert(1,0.)
        lim.insert(1,1.)
      if lim[-1] > 1:
        lim[-1] -= 1
        lim.insert(-1,1.)
        lim.insert(-1,0.)
    xlim, ylim, zlim = np.asarray(xlim), np.asarray(ylim), np.asarray(zlim)
    # ------ Get all combinations
    xin, yin, zin = np.mgrid[:len(xlim):2,:len(ylim):2,:len(zlim):2]
    xin, yin, zin = xin.ravel(), yin.ravel(), zin.ravel()
    xmi, xma = xlim[xin], xlim[xin+1]
    ymi, yma = ylim[yin], ylim[yin+1]
    zmi, zma = zlim[zin], zlim[zin+1]
  else:
    xmi, xma, ymi, yma, zmi, zma = [xlim[0]], [xlim[-1]], [ylim[0]], [ylim[-1]], [zlim[0]], [zlim[-1]]

  executable = []
  for i in range(len(xmi)):
    # ------- Begin parsing fortran command
    exe  = lexe
    exe += ' -inp ' + inp
    exe += ' -out ' + map_file + '_%i' % i
    exe += ' -xmi ' + str(xmi[i]) + ' -xma ' + str(xma[i])
    exe += ' -ymi ' + str(ymi[i]) + ' -yma ' + str(yma[i])
    exe += ' -zmi ' + str(zmi[i]) + ' -zma ' + str(zma[i])
    exe += ' -lma ' + str(lvlmax)
    exe += ' -typ ' + str(type)
    exe += ' -dir ' + dir
    exe += ' -fil bin'
    if do_max is True: executable += ' -maxi 1'
    executable.append(exe)
  
  # if dryrun only return command string
  if dryrun is True:
    print(executable[0])
    return executable[0]
  
  map = []
  for i, exe in enumerate(executable):
    # if silent suppress output
    proc = subprocess.Popen(exe.split(), stdout=subprocess.PIPE)
    stdout, stderr = proc.communicate() # proc.wait() hangs. probably due to buffer overload. thankfully communicate is just as good
    #proc.wait()
    #output = proc.stdout.read()
    #proc.stdout.close()
    if silent is False:
      print(stdout.decode('utf-8'))
    
    # check if command ran properly
    if proc.returncode != 0:
      raise IOError('Something happened while running Fortran command. Exit status: %i' % proc.returncode)
    
    try:
      # unpack binary file
      mmap = rd_map(map_file + '_%i' % i)
      mmap = np.transpose(mmap) if dir == 'y' else mmap # exchange axes in y direction to make coordinate axes cyclic in all projections
      map.append(mmap)
    finally:
      # delete binary file
      os.remove(map_file + '_%i' % i)
  
  assert len(map) in [1,2,4,8], "len(map) = %i - something must've gone wrong" % len(map)

  if dir == 'x' and len(xlim) > 2:
    df = np.diff(xlim)
  elif dir == 'y' and len(ylim) > 2:
    df = np.diff(ylim)
  elif dir == 'z' and len(zlim) > 2:
    df = np.diff(zlim)
  else:
    df = [0.5, 0.5]
  f1, f2 = df[0]/(df[0]+df[-1]), df[-1]/(df[0]+df[-1])
  
  if len(map) > 1:
    while len(map) > 1:
      mmap = []
      for i in range(0,len(map),2):
        map1, map2 = map[i], map[i+1]
        sh1 , sh2  = map1.shape, map2.shape
        if sh1 == sh2:
          mmap.append(np.dstack((f1*map1,f2*map2)).mean(axis=2))
        elif sh1[0] == sh2[0]:
          mmap.append(np.concatenate((map1,map2),axis=1))
        elif sh1[1] == sh2[1]:
          mmap.append(np.concatenate((map1,map2),axis=0))
      map = mmap
  
  return map[0]

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def rd_map(filename):
  """
  Reads RAMSES binary map file as output by amr2map, given the filename as input.
  """
  
  # open file
  f = open(filename, mode='rb')
  
  try:
    ########## read header begin ##########
    dt = np.dtype('u4, i4, i4, u4')
    sentinel1, n1, n2, sentinel2 = np.fromfile(f,dtype=dt,count=1)[0]
    void = check_sentinels(sentinel1,sentinel2)
    ########## read header end ##########
    
    ########## start reading data ##########
    dt = np.dtype('u4, (%i,)f4 , u4' % (n1*n2))
    
    # read data record
    sentinel1, map, sentinel2 = np.fromfile(f,dtype=dt,count=1)[0]
    void = check_sentinels(sentinel1,sentinel2)
  finally:
    # close file
    f.close()
  
  #reshape map
  map = map.reshape((n2, n1))
  
  return map
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def map2image(map,temp=10.,wavelength=850.,dpc=150.):
  
  opac_file  = pyrad.opac_file('oh5')
  kappa_dict = pyrad.readdustkappa(opac_file)
  
  kappa_abs  = 10**np.interp(np.log10(wavelength),np.log10(kappa_dict['lambda']),np.log10(kappa_dict['kappa_abs']))
  
  gas2dust = 100.
  
  xrange = map['xrange']
  yrange = map['yrange']
  nx, ny = len(xrange), len(yrange)
  sx, sy = np.median(np.diff(xrange)), np.median(np.diff(yrange))
  sx_rad, sy_rad = sx/(dpc*c.pc2cm), sy/(dpc*c.pc2cm)
  nlam = 1
  
  flux = map['density']*map['column_density']*kappa_abs*pyrad.planck(wavelength,temp)*sx_rad*sy_rad/gas2dust
  
  print(kappa_abs)
  
  return {'flux':flux,'pixnum':[nx,ny],'pixsize':[sx,sy],'nlam':nlam,\
          'lambdas':wavelength,'xrange':xrange,'yrange':yrange,'dpc':dpc}
