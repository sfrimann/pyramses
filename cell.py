#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, absolute_import, division

from .errors import check_sentinels
from .utils import detect_single_double, rd_time, convert_units
from .sink import rsink
from scipy.interpolate import griddata
from random import randint
from subprocess import Popen, PIPE
from scipy.spatial import cKDTree

try:
  from astropy.io import fits as pyfits
except:
  import pyfits

from . import constants as c
import numpy as np
import os

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def get_cell(*args, **kw):
  """
  get_cell is top level routine for getting RAMSES cell data.
  Returns dictionary of relevant data.
  Takes noutput and conversion factors as input
  The function also takes all keywords accepted by make_cell
  """
  # ------- Handle keywords
  deltar         = kw.get('deltar',[1.0, 1.0, 1.0]) # alias
  dr             = kw.get('dr',deltar)              # whole box
  dryrun         = kw.get('dryrun',False)           # just print command
  jet_factor     = kw.get('jet_factor',None)        # mass loss factor
  lmax           = kw.get('lmax',None)              # alias
  lvlmax         = kw.get('lvlmax',lmax)            # max amr level
  main_directory = kw.get('main_directory','.')     # alias
  datadir        = kw.get('datadir',main_directory) # data directory for snapshots
  sink_number    = kw.get('sink_number',False)      # sink number
  scales_file    = kw.get('scales_file','./scales.fits')
  r              = kw.get('r',[0.5,0.5,0.5])        # centre of box. Suppressed if sink_number is active
  
  # ------- Check arguments
  if len(args) == 1:
    nout    = args[0]
    try:
      header = pyfits.getheader(scales_file)
      physical_units = True
      scale_d, scale_l, scale_t, scale_m = header['scale_d'], header['scale_l'], header['scale_t'], header['scale_m']
      jet_factor = header['jet_factor'] if jet_factor is None else jet_factor
      lvlmax = header['lvlmax'] if lvlmax is None else lvlmax
      kw['lvlmax'] = header['lvlmax'] if lvlmax is None else lvlmax
    except IOError:
      scale_d = 1.
      scale_l = 1. 
      scale_t = 1.
      scale_m = 1.
      physical_units = False
      print("get_cell() in cell.py * Warning: Conversion scales are not given. I will not convert to physical units")
  elif len(args) == 2 and len(args[1]) == 4:
    nout    = args[0]
    scale_d = args[1][0]
    scale_l = args[1][1]
    scale_t = args[1][2]
    scale_m = args[1][3]
    physical_units = True
  elif len(args) == 5:
    nout    = args[0]
    scale_d = args[1]
    scale_l = args[2]
    scale_t = args[3]
    scale_m = args[4]
    physical_units = True
  else:
    print("syntax: result = get_cell(nout, keywords=values)")
    print("        result = get_cell(nout, scales, keywords=values) where scales = [scale_d, scale_l, scale_t, scale_m]")
    print("        result = get_cell(nout, scale_d, scale_l, scale_t, scale_m, keywords=values)")
    raise IOError("Wrong number of arguments")
  
  jet_factor = 1. if jet_factor is None else jet_factor
  lvlmax = 6 if lvlmax is None else lvlmax
  
  # ------- expand datadir
  datadir = os.path.abspath(os.path.expanduser(datadir))
  
  # ------- Handle dr
  # if dr is string extract length and convert
  # examples are dr = '11e22 cm', dr = '20000 au', dr = '0.2 parsec'
  if isinstance(dr,str):
    if physical_units is False:
      raise ValueError('dr cannot be in physical units when we work in numerical units')
    dr, unit = dr.lower().split(' ')
    dr = float(dr) # convert to number
    if not unit in ['cm', 'au', 'parsec']:
      raise IOError("length unit %s not in list of allowed units (cm, au, parsec)" % unit)
    if unit == 'cm':
      dr /= scale_l
    elif unit == 'au':
      dr  = dr * c.au_to_cm / scale_l
    elif unit == 'parsec':
      dr  = dr * c.parsec_to_cm / scale_l
   
  # if dr is scalar make it a three element list
  if isinstance(dr,float):
    dr = [dr, dr, dr]
  
  # check if any elements in dr is greater than box length
  if any([i > 1 for i in dr]):
    raise IOError("One or more elements in dr greater than box size")

  # ------- sink particles
  # read sink particles
  sink          = rsink(nout,datadir=datadir)
  snapshot_time = sink['snapshot_time']*scale_t
  sink_age      = snapshot_time - sink['tcreate']*scale_t
  sink_mass     = sink['m'] * scale_m * jet_factor
  sink_velocity = np.vstack((sink['ux'],sink['uy'],sink['uz'])).transpose() * scale_l/scale_t
  if 'tflush' in sink:
    dmdt = np.array([np.log10(dm) if dm > 0 else -np.inf for dm in sink['dm']]) + np.log10(scale_m) + np.log10(jet_factor) - np.log10(snapshot_time - sink['tflush']*scale_t)
  else:
    dmdt = False # you have to do the derivative yourself

  # new centre if sink_number is set
  if sink_number is not False:
    if sink_mass[sink_number] < 0:
      raise ValueError("sink %i has already exploded as a super nova" % sink_number)
    r = [sink['x'][sink_number], sink['y'][sink_number], sink['z'][sink_number]]
  
  # ------- sink coordinates relative to cutout centre
  sinkr = np.vstack((sink['x']-r[0],sink['y']-r[1],sink['z']-r[2])).transpose()
  
  # rearrange sinks around cutout centre
  index = np.where(sinkr < -0.5)
  sinkr[index] += 1
  index = np.where(sinkr > 0.5)
  sinkr[index] -= 1

  # ------- Determine which sink particles are inside cut-out
  sink_index = np.arange(sinkr.shape[0])
  sinks_inside_cutout = sink_index[(np.abs(sinkr[:,0]) <= dr[0]/2.) & \
                                   (np.abs(sinkr[:,1]) <= dr[1]/2.) & \
                                   (np.abs(sinkr[:,2]) <= dr[2]/2.)]
  if len(sinks_inside_cutout) == 0:
    sinks_inside_cutout = False
  else:
    sinks_inside_cutout = np.asarray(sinks_inside_cutout)

  # ------- make cell
  kw['r'], kw['dr']  = r, dr
  cell = make_cell(nout, **kw)
  
  # ------- if dryrun just return
  if dryrun is True:
    return cell
  
  # ------- convert numerical to physical units
  cell['r'][:,0]  = (cell['r'][:,0] - r[0]) * scale_l # cell coordinates relative to cutout centre
  cell['r'][:,1]  = (cell['r'][:,1] - r[1]) * scale_l
  cell['r'][:,2]  = (cell['r'][:,2] - r[2]) * scale_l
  cell['dx']     *= scale_l
  sinkr          *= scale_l
  
  # ------- Retrieve data
  density         = cell['v'][:,0] * scale_d
  velocity        = cell['v'][:,1:4] * scale_l/scale_t
#   Bleft           = cell['v'][:,4:7]
#   Bright          = cell['v'][:,7:10]
#   pressure        = cell['v'][:,10]
  
  # cutout centre in physical units
  r[0] *= scale_l
  r[1] *= scale_l
  r[2] *= scale_l

  # ------- Write dictionary and return
  cell = {'datadir'            : datadir, \
          'density'            : density, \
          'dictype'            : 'cell', \
          'dx'                 : cell['dx'], \
          'jet_factor'         : jet_factor, \
          'level'              : cell['ilevel'], \
          'lvlmax'             : lvlmax, \
          'nout'               : nout, \
          'original_centre'    : r, \
          'physical_units'     : physical_units, \
          'r'                  : cell['r'], \
          'sinkr'              : sinkr, \
          'sink_age'           : sink_age, \
          'sink_mass'          : sink_mass, \
          'sink_number'        : sink_number, \
          'sink_velocity'      : sink_velocity, \
          'sinks_inside_cutout': sinks_inside_cutout, \
          'logdmdt'            : dmdt, \
          'time'               : snapshot_time, \
          'velocity'           : velocity}
  
  return cell
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def make_cell(nout, **kw):
  """
  make_cell parses and calls amr2cell fortran command, reads the resulting file, and returns
  cell dictionary
  """
  # ------- Handle keywords
  cyclic         = kw.get('cyclic',True)            # if true stitch boundaries together
  r              = kw.get('r',[0.5, 0.5, 0.5])      # centre of box
  deltar         = kw.get('deltar',[1.0, 1.0, 1.0]) # alias
  dr             = kw.get('dr',deltar)              # whole box
  lmin           = kw.get('lmin', None)             # alias
  lvlmin         = kw.get('lvlmin',lmin)            # min amr level 
  lmax           = kw.get('lmax', 6)                # alias
  lvlmax         = kw.get('lvlmax', lmax)           # max amr level
  dryrun         = kw.get('dryrun', False)          # print command
  shm            = kw.get('shm', True)              # binary location
  tmp            = kw.get('tmp', False)             # binary location
  main_directory = kw.get('main_directory','.')     # alias
  datadir        = kw.get('datadir',main_directory) # data directory for snapshots
  exe            = kw.get('exe', None)              # name of fortran executable. Autodetected if None
  quiet          = kw.get('quiet', False)           # alias
  silent         = kw.get('silent', quiet)          # supress output from amr2cell
  tempdir        = kw.get('tempdir',None)
  full_oct       = kw.get('full_oct',False)
  
  assert len(dr) == 3, "dr must contain three items"
  assert len(r) == 3, "r must contain three items"
  
  # ------- Handle directories and files
  cell_dir = '/dev/shm' if shm is True else '.'
  if tmp is True: cell_dir = '/tmp'
  if isinstance(tempdir,str): cell_dir = tempdir
  
  cell_file = os.path.join(cell_dir,'temp_cell'+str(randint(10000,99999)))
  datadir   = os.path.abspath(os.path.expanduser(datadir))
  inp       = os.path.join(datadir,'output_'+str(nout).zfill(5))
  
  # At present I assume the name of the executable, and that it is available in path.
  # A future update will search for the executable if not given
  if exe is None:
    lexe = 'amr2cell'
  else:
    lexe = exe
  
  # ------- xyz limits in numerical units
  xlim = [r[0] - dr[0]/2., r[0] + dr[0]/2.]
  ylim = [r[1] - dr[1]/2., r[1] + dr[1]/2.]
  zlim = [r[2] - dr[2]/2., r[2] + dr[2]/2.]
  
  # if full_oct make sure that the limit fall on the edges of the root grid (as traced by
  # the lvlmin keyword)
  if full_oct:
    assert isinstance(lvlmin,int), "lvlmin must be an interger when using full_oct"
    assert lvlmin > 0, 'lvlmin must be a positive integer'
    assert lvlmin <= lvlmax, 'lvlmin must be smaller than or equal to lvlmax'
    
    edges = np.linspace(-1,2,num=(3*2**lvlmin + 1)) # edges of the root grid
    for lim in [xlim, ylim, zlim]:
      indices = np.interp(lim,edges,np.arange(edges.size))
      lim[0]  = edges[int(np.floor(indices[0]))]
      lim[1]  = edges[int(np.ceil(indices[1]))]
      if (lim[1]-lim[0]) > 1:
        lim[1]  = edges[int(np.floor(indices[1]))]
  
  # ------- check if any of the limits go beyond the box, and split limits appropiately# 
#   # make copies of original xyz limits (before possibly changing them)
#   xlim_orig, ylim_orig, zlim_orig = xlim.copy(), ylim.copy(), zlim.copy()
  if cyclic is True and dryrun is False:
    for lim in [xlim, ylim, zlim]:
      if lim[0] < 0:
        lim[0] += 1
        lim.insert(1,0.)
        lim.insert(1,1.)
      if lim[-1] > 1:
        lim[-1] -= 1
        lim.insert(-1,1.)
        lim.insert(-1,0.)
    xlim, ylim, zlim = np.asarray(xlim), np.asarray(ylim), np.asarray(zlim)
    # ------ Get all combinations
    xin, yin, zin = np.mgrid[:len(xlim):2,:len(ylim):2,:len(zlim):2]
    xin, yin, zin = xin.ravel(), yin.ravel(), zin.ravel()
    xmi, xma = xlim[xin], xlim[xin+1]
    ymi, yma = ylim[yin], ylim[yin+1]
    zmi, zma = zlim[zin], zlim[zin+1]
  else:
    xmi, xma, ymi, yma, zmi, zma = [xlim[0]], [xlim[-1]], [ylim[0]], [ylim[-1]], [zlim[0]], [zlim[-1]]
  
  # ------- loop over limits
  executable = []
  for i in range(len(xmi)):
    # ------- Begin parsing fortran command
    exe  = lexe
    exe += ' -inp ' + inp
    exe += ' -out ' + cell_file + '_%i' % i
    exe += ' -xmi ' + str(xmi[i]) + ' -xma ' + str(xma[i])
    exe += ' -ymi ' + str(ymi[i]) + ' -yma ' + str(yma[i])
    exe += ' -zmi ' + str(zmi[i]) + ' -zma ' + str(zma[i])
    exe += ' -lma ' + str(lvlmax)
    exe += ' -typ bin'
    executable.append(exe)
  
  # ------- if dryrun only return command string
  if dryrun is True:
    print(executable[0])
    return executable[0]
  
  cell = []
  for i, exe in enumerate(executable):
    try:
      # if silent suppress output
      proc = Popen(exe.split(), stdout=PIPE)
      proc.wait()
      output = proc.stdout.read()
      proc.stdout.close()
      if silent is False:
        print(output.decode('utf-8'))
      
      # check if command ran properly
      if proc.returncode != 0:
        raise IOError('Something happened while running Fortran command. Exit status: %i4' % proc.returncode)
      
      # unpack binary file
      ccell, header = rd_cell(cell_file + '_%i' % i)
      # ------- Removed 24 April 2016
      #         This is actually very wrong. All positions should be absolute in make_cell.
      #         Only in get_cell and in the cell object are the positions made relative to 
      #         some central position.
      # reorder positions
#       for j,lim in enumerate(zip([xmi[i],ymi[i],zmi[i]],[xma[i],yma[i],zma[i]])):
#         if r[j] > lim[0] and r[j] > lim[1]:
#           ccell['r'][:,j] += 1
#         elif r[j] < lim[0] and r[j] < lim[1]:
#           ccell['r'][:,j] -= 1

      # ------- 24 April 2016: Moved into here from bottom. Having this after the loop was
      #         a bug. One needs to check the positions inside the loop to ensure that some
      #         cells are not repeated.
      # if full_oct make sure that only data from that oct is included
      # ------- 24 April 2016: Changed the full_oct behavour to default behaviour
      #if full_oct:
      index = (ccell['r'][:,0] > xmi[i]) & (ccell['r'][:,0] < xma[i]) & \
              (ccell['r'][:,1] > ymi[i]) & (ccell['r'][:,1] < yma[i]) & \
              (ccell['r'][:,2] > zmi[i]) & (ccell['r'][:,2] < zma[i])
      for key in ccell:
	      ccell[key] = ccell[key][index]
      cell.append(ccell)
    finally:
      # delete binary file
      os.remove(cell_file + '_%i' % i)
  
  # ------- finally concatenate all calls to amr2cell into one cell structure
  if len(cell) > 1:
    new_cell = {}
    for key in cell[0]:
      new_cell[key] = np.concatenate(tuple(c[key] for c in cell))
  else:
    new_cell = cell[0]
  
  # ------- 24 April 2016: Moved into the for loop
  # if full_oct make sure that only data from that oct is included
#   if full_oct:
#     index = (new_cell['r'][:,0] > xlim_orig[0]) & (new_cell['r'][:,0] < xlim_orig[1]) & \
#             (new_cell['r'][:,1] > ylim_orig[0]) & (new_cell['r'][:,1] < ylim_orig[1]) & \
#             (new_cell['r'][:,2] > zlim_orig[0]) & (new_cell['r'][:,2] < zlim_orig[1])
#     for key in new_cell:
#       new_cell[key] = new_cell[key][index]
  
  return new_cell

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def rd_cell(filename):
  """
  Reads RAMSES binary cell file as output by amr2cell, given the filename as input.
  """
  # open file
  f = open(filename, mode='rb')
  
  try:
    ########## read header begin ##########
    # read first record
    dt = np.dtype('u4, i4, i4, i4, u4')
    sentinel1, io_format, prec, mfile, sentinel2 = np.fromfile(f,dtype=dt,count=1)[0]
    void = check_sentinels(sentinel1,sentinel2)
    
    # read second record
    dt = np.dtype('u4, i4, i4, i4, (2,)d, (2,)d, (2,)d, i4, i4, u4')
    sentinel1, nvarh, lmax, nlevelmax, xrange, yrange, zrange, ncpu, ncpu_read, sentinel2 = np.fromfile(f,dtype=dt,count=1)[0]
    void = check_sentinels(sentinel1,sentinel2)
    
    # read third record
    sentinel1 = np.fromfile(f,dtype=np.uint32,count=1)[0]
    fname     = np.fromfile(f,dtype='S'+str(sentinel1),count=1)[0]
    sentinel2 = np.fromfile(f,dtype=np.uint32,count=1)[0]
    void = check_sentinels(sentinel1,sentinel2)
    
    # read fourth record
    dt = np.dtype('u4, ('+str(ncpu)+',)i4, u4')
    sentinel1, cpu_list, sentinel2 = np.fromfile(f,dtype=dt,count=1)[0]
    void = check_sentinels(sentinel1,sentinel2)
    
    # read fifth record
    dt = np.dtype('u4, i8, u4')
    sentinel1, ncell, sentinel2 = np.fromfile(f,dtype=dt,count=1)[0]
    void = check_sentinels(sentinel1,sentinel2)
    ########## read header end ##########
    
    ########## write header dictionary ##########
    header = {'io_format': io_format, 'precision': prec, 'mfile': mfile, 'nvarh': nvarh, \
              'lmax': lmax, 'nlevelmax': nlevelmax, 'xrange': xrange, 'yrange': yrange,  \
              'zrange': zrange, 'ncpu': ncpu, 'ncpu_read': ncpu_read, 'filename': filename, \
              'cpu_list': cpu_list, 'ncell': ncell}
  
    
    ########## start reading data ##########
    # create data type depending on snapshot precision
    if prec == 4:
      dt = np.dtype([('r', 'f4', (3,)), ('dx', 'f8'), ('icpu', 'i4'), ('ilevel', 'i4'), ('v', 'f4', (nvarh,))])
      record_size =   4*3             + 8           + 4             + 4               + 4*nvarh
    elif prec == 8:
      dt = np.dtype([('r', 'f8', (3,)), ('dx', 'f8'), ('icpu', 'i4'), ('ilevel', 'i4'), ('v', 'f8', (nvarh,))])
      record_size =   8*3             + 8           + 4             + 4               + 8*nvarh
    
    # determine current position in file, and total file size
    current_pos = f.tell() # current position in bytes
    f.seek(0,2) # go to end of file to determine file size
    file_size = f.tell() # file size in bytes
    f.seek(current_pos,0) # go back to previous position
    
    ncellf = (file_size - current_pos - 16) // record_size # number of cells from file
    
    # check that remainder of file has expected size
    if ncellf*record_size != (file_size - current_pos - 16):
      print('ncell from header  :', ncell)
      print('ncell from file    :', ncellf)
      print('record size        :', record_size)
      print('expected bytes     :', ncellf*record_size + 16)
      print('Bytes left in file :', file_size - current_pos)
      raise IOError('The number of cells is a floating point.')
    if ncell != ncellf:
      raise IOError("Number of cells in header doesn't match number of cells in file. %i4 != %i4" % (ncell, ncellf))
  
    # read data record
    sentinel1 = np.fromfile(f,dtype='u8',count=1)[0]
    cell      = np.fromfile(f,dtype=dt,count=ncell)  
    sentinel2 = np.fromfile(f,dtype='u8',count=1)[0]
    void = check_sentinels(sentinel1,sentinel2)
  finally:
    # close file
    f.close()
  
  return {'r': cell['r'], 'dx': cell['dx'], 'icpu': cell['icpu'], 'ilevel': cell['ilevel'], 'v': cell['v']}, header
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def cell2map(cell,**kw):
  """
  convert cell dictionary to map
  """

  # ------- Handle keywords
  zoomau         = kw.get('zoomau',None)            # boxsize in au
  npix           = kw.get('npix',None)              # number of pixels
  dir            = kw.get('dir','z')                # projection direction
  density        = kw.get('density',True)
  vx             = kw.get('vx',False)
  vy             = kw.get('vy',False)
  vz             = kw.get('vz',False)
  
  if zoomau is None:
    zoomcm = list(cell['r'].max(axis=0) - cell['r'].min(axis=0))
  else:
    zoomau = [zoomau,zoomau,zoomau] if np.rank(zoomau) == 0 else zoomau  
    zoomcm = [i*c.au2cm for i in zoomau]
  
  dxmin = np.min(cell['dx'])
  
  npixx, npixy, npixz = int(zoomcm[0]/dxmin), int(zoomcm[1]/dxmin), int(zoomcm[2]/dxmin)
  
  # ------- if too many pixels downsize
  if any([pixel > 200 for pixel in [npixx, npixy, npixz]]):
    factor = max([npixx, npixy, npixz])/200.
    npixx, npixy, npixz = int(npixx/factor), int(npixy/factor), int(npixz/factor)
  
  if npix is None:
    npix = [npixx, npixy, npixz]
  if np.rank(npix) == 0:
    npix = [npix, npix, npix]
  
  xrange = np.linspace(-zoomcm[0]/2.,zoomcm[0]/2.,num=npix[0])
  yrange = np.linspace(-zoomcm[1]/2.,zoomcm[1]/2.,num=npix[1])
  zrange = np.linspace(-zoomcm[2]/2.,zoomcm[2]/2.,num=npix[2])
  
  ranges = [xrange,yrange,zrange]
  
  if dir == 'x':
    vin, axis = 0, 2
    xin, yin, zin = 1, 2, 0
  elif dir == 'y':
    vin, axis = 1, 1
    xin, yin, zin = 2, 0, 1
  elif dir == 'z':
    vin, axis = 2, 0
    xin, yin, zin = 0, 1, 2
  else:
    raise IOError('Problem with projection direction')
  
  sinkx, sinky = cell['sinkr'][:,xin], cell['sinkr'][:,yin]
  sinkvx, sinkvy = cell['sink_velocity'][:,xin], cell['sink_velocity'][:,yin]
  
  index = np.vstack((cell['r'][:,0] >= xrange[0], cell['r'][:,0] <= xrange[-1], \
                     cell['r'][:,1] >= yrange[0], cell['r'][:,1] <= yrange[-1], \
                     cell['r'][:,2] >= zrange[0], cell['r'][:,2] <= zrange[-1])).all(axis=0)
  
  zz, yy, xx = np.meshgrid(zrange,yrange,xrange,indexing='ij')
  
  # density and velocities. Note that vz is multiplied with -1 to enforce the astronomical sign
  # convention for radial velocities
  if density is True:
    density =  griddata(cell['r'][index,:],cell['density'][index],(xx,yy,zz),method='nearest').mean(axis=axis)
    density = density.T if axis == 1 else density
  if vx is True:
    vx = griddata(cell['r'][index,:],cell['velocity'][index,xin],(xx,yy,zz),method='nearest').mean(axis=axis)
    vx = vx.T if axis == 1 else vx
  if vy is True:
    vy = griddata(cell['r'][index,:],cell['velocity'][index,yin],(xx,yy,zz),method='nearest').mean(axis=axis)
    vy = vy.T if axis == 1 else vy
  if vz is True:
    vz = -griddata(cell['r'][index,:],cell['velocity'][index,zin],(xx,yy,zz),method='nearest').mean(axis=axis)
    vz = vz.T if axis == 1 else vz
  
  # ------- Determine which sink particles are inside cut-out
  sink_index = np.arange(cell['sinkr'].shape[0])
  sinks_inside_cutout = sink_index[(np.abs(cell['sinkr'][:,0]) <= zoomcm[0]/2.) & \
                                   (np.abs(cell['sinkr'][:,1]) <= zoomcm[1]/2.) & \
                                   (np.abs(cell['sinkr'][:,2]) <= zoomcm[2]/2.)]
  if len(sinks_inside_cutout) == 0:
    sinks_inside_cutout = None
  else:
    sinks_inside_cutout = list(sinks_inside_cutout)
  
  # column density factor (depth of plot in cm)
  cd_factor = ranges[zin][-1]-ranges[zin][0]
  
  map  = {'column_density'     : cd_factor, \
          'datadir'            : cell['datadir'], \
          'density'            : density, \
          'dictype'            : 'cell2map', \
          'dir'                : dir, \
          'jet_factor'         : cell['jet_factor'], \
          'lvlmax'             : None, \
          'nout'               : cell['nout'], \
          'original_centre'    : cell['original_centre'], \
          'physical_units'     : cell['physical_units'], \
          'sinkx'              : sinkx, \
          'sinky'              : sinky, \
          'sinkvx'             : sinkvx, \
          'sinkvy'             : sinkvy, \
          'sink_age'           : cell['sink_age'], \
          'sink_mass'          : cell['sink_mass'], \
          'sink_number'        : cell['sink_number'], \
          'sinks_inside_cutout': sinks_inside_cutout, \
          'time'               : cell['time'], \
          'vx'                 : vx, \
          'vy'                 : vy, \
          'vz'                 : vz, \
          'xrange'             : ranges[xin], \
          'yrange'             : ranges[yin]}
  
  return map
