#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ----- __init__.py file for pyramses package -----

"""
PyRamses
========

PyRamses is a package designed for doing post-processing of RAMSES data in 
Python. The package is designed to work in conjunction with the three FORTRAN
routines amr2cell, amr2cube, and amr2map. These commands are assumed by the
package to be available in the path.

The PyRamses package contain a number of modules:

sink
  Contains routines for reading sink data from RAMSES snapshots
cell
  Contains routines for reading leaf cell data from RAMSES snapshot.
  Is essentially a wrapper of amr2cell
cube
  Contains routines for reading cutouts from RAMSES snapshots interpolated
  onto a uniform grid. Is essentially a wrapper of amr2cube
map
  Contains routines for reading RAMSES data projected onto xyz directions.
  Is essentially a wrapper of amr2map
utils
  Contains various utilities used when reading data or when postprocessing.
plotutils
  Various plotting routines using matplotlib. Primarily focused on plotting
  map data
constants
  Various physical constants used throughout the code

"""

from __future__ import absolute_import

from .classes import *
from .cell import *
from .cube import *
from .map import *
from .errors import *
from .utils import *
from .sink import *
from .particle import *
from .constants import *
from .plotutils import *
from .plot import *
from .hyperion_rt import *