#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, absolute_import, division

from . import constants as c
import numpy as np
from .errors import check_sentinels, PrecisionError
from os.path import join
from scipy.interpolate import griddata
from timeseries import hampel
from scipy.optimize import leastsq

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def rd_info(filename):
  """
  Read RAMSES info file, which typically has the name info_00001.txt where the number is
  the output number.
  
  Input:
    filename : path to the info file
  Returns:
    d        : Dictionary containing the key value pairs of the content of the file
  """
  
  d = {}
  # open file and step through
  with open(filename,'r') as f:
    for line in f:
      keyval = line.rstrip('\n').split('=')
      
      # We're only interested in values that are formattet 'key = val' for now
      if not len(keyval) == 2:
        continue
      
      key, val = keyval
      
      try: # is val an integer?
        val = int(val)
      except ValueError:
        try: # no, is val then a float?
          val = float(val)
        except ValueError: # no, in that case continue
          continue
      
      d[key.rstrip()] = val
  
  return d

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def inside_mass(cell,radius,**kw):
  """
  Calculate the mass inside a sphere with given radius
  cell is output dictionary from get_cell. The sphere is centered around the origin of the coordinate system.
  Keywords:
    au    : Calculate radius in AU, els cm (True/False Default: True)
    msun  : Return mass in solar masses, else g (True/False Defult: True)
  """
  # ------- Keywords
  au   = kw.get('au',True)
  msun = kw.get('msun',True)
  returnsink = kw.get('returnsink',False)
  
  r       = np.sqrt(np.sum(cell['r']**2,axis=1)) # distance from origin
  sinkr   = np.sqrt((cell['sinkr']**2).sum(axis=1))
  dx      = cell['dx']
  density = cell['density']
  
  rxmax = np.abs(cell['r'][:,0]).max()
  rymax = np.abs(cell['r'][:,1]).max()
  rzmax = np.abs(cell['r'][:,2]).max()
  
  if au is True:
    r     *= c.cm2au
    sinkr *= c.cm2au
    rxmax *= c.cm2au
    rymax *= c.cm2au
    rzmax *= c.cm2au
    
  if any([rxmax < radius, rymax < radius, rzmax < radius]):
    print("inside_mass() in utils.py * Warning: limit goes beyond cut-out range")
  
  sink_mass = np.sum(cell['sink_mass'][sinkr <= radius] * c.g2msun)
  nsink     = np.sum(sinkr <= radius)
  
  index = np.where(r <= radius)
  if len(index[0]) == 0:
    if returnsink:
      return 0., sink_mass, nsink
    else:
      return 0.
  mass  = np.sum(density[index]*dx[index]**3)
  mass  = mass*c.g2msun if msun is True else mass
  
  if returnsink:
    return mass, sink_mass, nsink
  else:
    return mass
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def detect_single_double(nout, datadir='.'):
  """
  Detect if RAMSES snapshot is single or double precision.
  Soeren Frimann May 2013. Translated from detect_single_double.pro by Troels Haugboelle
  """
  # filename and location
  ext   = str(nout).zfill(5)
  fname = join(datadir,'output_'+ext,'amr_'+ext+'.out00001')
  
  f = open(fname, mode='rb') # open file
  # jump to location of boxlen which is the first record that may either be in double or precision
  f.seek(92,0)
  sentinel1 = np.fromfile(f, dtype='u4', count=1)[0] # first sentinel
  f.seek(sentinel1,1) # jump over data
  sentinel2 = np.fromfile(f, dtype='u4', count=1)[0] # second sentinel
  f.close() # close file
  # check if sentinels are equal
  void = check_sentinels(sentinel1,sentinel2)
  
  if sentinel1 == 8:
    return 'double'
  elif sentinel1 == 4:
    return 'single'
  else:
    raise PrecisionError("Snapshot apperently neither double (8 bytes) or single (4 bytes) precision. Bytes: %i4" % sentinel1)

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def rd_time(nout, datadir='.'):
  """
  Read numerical time from RAMSES snapshot.
  Soeren Frimann May 2013
  """
  # filename and location
  ext   = str(nout).zfill(5)
  fname = join(datadir,'output_'+ext,'amr_'+ext+'.out00001')
  
  f = open(fname, mode='rb') # open file
  for i in range(11): # jump over first 11 records (time is in 12th record)
    sentinel = np.fromfile(f, dtype='u4', count=1)[0] # first sentinel
    f.seek(sentinel+4,1) # jump over data
  sentinel1 = np.fromfile(f, dtype='u4', count=1)[0] # first sentinel
  time      = np.fromfile(f, dtype='f' if sentinel1 == 4 else 'd', count=1)[0]
  sentinel2 = np.fromfile(f, dtype='u4', count=1)[0] # second sentinel
  f.close() # close file
  
  # check if sentinels are equal
  void = check_sentinels(sentinel1,sentinel2)
  
  return time
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def convert_units(box_size_in_parsec, velocity_unit_in_km_s, box_mass_in_msun):
  """
  Given box size in parsec, velocity unit in km/s, and box mas in msun, calculate scaling
  factors for converting numerical density, length, time, and mass into cgs units.
  Soeren Frimann May 2013
  """

  box_size_in_meter    = box_size_in_parsec * c.parsec_to_meter
  box_size_in_au       = box_size_in_parsec * c.parsec_to_au
  box_mass_in_kg       = box_mass_in_msun * c.solar_mass_to_kg
  mass_density_in_cgs  = box_mass_in_kg / (box_size_in_meter**3) * 1e-3
  number_density       = mass_density_in_cgs / (c.molecular_mass * c.unit_mass_in_g)
  number_density_in_si = number_density * 1e6
  velocity_unit_in_m_s = velocity_unit_in_km_s * 1e3
  time_unit_in_s       = box_size_in_meter / velocity_unit_in_m_s
  time_unit_in_myr     = time_unit_in_s * c.second_to_year * 1e-6

  # calculate scales
  scale_d = mass_density_in_cgs
  scale_l = box_size_in_meter * 100
  scale_t = time_unit_in_s
  scale_m = box_mass_in_kg * 1e3

  return scale_d, scale_l, scale_t, scale_m
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def fit_powerlaw(r,ddata,**kw):
  """
  fit powerlaw to profile
  """
  # ------- Handle Keywords
  nbin  = kw.get('nbin',100)
  xlog  = kw.get('xlog',True)
  ylog  = kw.get('ylog',True)
  wweights = kw.get('weights',np.ones_like(ddata))
  
  # ------- Sort by distance from centre
  index   = np.argsort(r)
  rr      = r[index]
  data    = ddata[index]
  weights = wweights[index]
  
  if ylog is True:
    index = data > 0
    rr    = rr[index]
    data  = data[index]
    weights = weights[index]
  
  # ------- log or not
  rr      = np.log10(rr) if xlog is True else rr
  data    = np.log10(data) if ylog is True else data
  
  # ------- min/max values
  rr_min, rr_max = rr.min(), rr.max()

  # ------- Bin the data
  bin_edges = np.linspace(rr_min,rr_max,num=nbin+1)
  bin_spacing = np.median(np.diff(bin_edges))
  
  # ------- Initialise Arrays
  x = 0.5 * (bin_edges[:-1] + bin_edges[1:])
  
  # ------- Calculate median in each bin
  y = np.empty(len(x))
  for i,bin_center in enumerate(x):
    cnt, const = 0, 1.
    while cnt <= 5:
      #print np.where((rr >= (bin_center-const*bin_spacing/2.)) & (rr <= (bin_center+const*bin_spacing/2.)))
      index = (rr >= (bin_center-const*bin_spacing/2.)) & (rr <= (bin_center+const*bin_spacing/2.))
      cnt = index.sum()
      const += 1
    y[i]  = wmedian(data[index],weights[index])

  # ------- If logarithmic transform back to linear coordinates
  x = 10**x if xlog is True else x
  y = 10**y if ylog is True else y
  
  A = np.vstack([np.log10(x), np.ones(len(x))]).T
  a, b = np.linalg.lstsq(A, np.log10(y))[0]
  
  return a, 10**b
  
  
def make_profile(r,ddata,**kw):
  """
  Make Density Profile
  """
  
  # ------- Handle Keywords
  nbin  = kw.get('nbin',100)
  nbinx = kw.get('nbinx',nbin)
  nbiny = kw.get('nbiny',nbin)
  xlog  = kw.get('xlog',True)
  ylog  = kw.get('ylog',True)
  wweights = kw.get('weights',np.ones_like(ddata))
  
  # ------- Sort by distance from centre
  index   = np.argsort(r)
  rr      = r[index]
  data    = ddata[index]
  weights = wweights[index]
  
  if ylog is True:
    index = np.where(data > 0)
    rr    = rr[index]
    data  = data[index]
    weights = weights[index]
  
  # ------- log or not
  rr      = np.log10(rr) if xlog is True else rr
  data    = np.log10(data) if ylog is True else data
  
  # ------- min/max values
  rr_min, rr_max = rr.min(), rr.max()
  data_min, data_max = data.min(), data.max()

  # ------- Bin the data
  bin_edges = np.linspace(rr_min,rr_max,num=nbinx+1)
  bin_index = np.searchsorted(bin_edges,rr,side='right')
  bin_spacing = np.median(np.diff(bin_edges))
  
  # ------- Initialise Arrays
  x  = np.interp(np.arange(nbinx)+0.5,np.arange(nbinx+1),bin_edges)
  #y  = np.empty(nbinx)
  #s0 = np.empty(nbinx)
  
  # ------- Calculate median in each bin
  #y, s0 = hampel(rr,data,dx=np.median(np.diff(bin_edges)),threshold=0,weights=weights,timeit=True)
  #s0[s0 == 0] = np.percentile(s0,50)
  #ix = np.argsort(rr)
  #y = np.interp(x,rr[ix],y[ix])
  #s0 = np.interp(x,rr[ix],s0[ix])
  y, s0 = np.empty(len(x)), np.empty(len(x))
  for i,bin_center in enumerate(x):
    cnt, const = 0, 1.
    while cnt <= 5:
      #print np.where((rr >= (bin_center-const*bin_spacing/2.)) & (rr <= (bin_center+const*bin_spacing/2.)))
      index, = np.where((rr >= (bin_center-const*bin_spacing/2.)) & (rr <= (bin_center+const*bin_spacing/2.)))
      cnt = len(index)
      const += 1
    y[i]  = wmedian(data[index],weights[index])
    s0[i] = 1.4826*wmedian(np.abs(data[index] - y[i]), weights[index])
  
  # ------- If logarithmic transform back to linear coordinates
  x = 10**x if xlog is True else x
  if ylog is True:
    upper_error = 10**(y + s0)
    lower_error = 10**(y - s0)
    y = 10**y
    s0 = 10**s0
  else:
    upper_error = y + s0
    lower_error = y - s0
  
  # ------- Calculate 2d Histogram
  histxy, yedges, xedges = np.histogram2d(data,rr,bins=[nbiny,nbinx],weights=weights)
  histx = np.interp(np.arange(nbinx)+0.5,np.arange(nbinx+1),xedges)
  histy = np.interp(np.arange(nbiny)+0.5,np.arange(nbiny+1),yedges)
  histx = 10**histx if xlog is True else histx
  histy = 10**histy if ylog is True else histy

  # ------- Shell volumes
#   shell = np.linspace(rr_min,rr_max,num=nbinx+1)
#   shell = 10**shell if xlog is True else shell
#   shell = 4./3 * np.pi * np.diff(shell**3)
#   shell = np.tile(np.array(shell),(nbiny,1))
#   histxy /= shell
  
  sumy = histxy.sum(axis=0)
  for i,s in enumerate(sumy):
    if s > 0.:
      histxy[:,i] /= s
  
  xfit = np.log10(x[~np.isnan(y)])
  yfit = np.log10(y[~np.isnan(y)])
  A = np.vstack([xfit, np.ones(len(xfit))]).T
  a, b = np.linalg.lstsq(A, yfit)[0]
  
  #ii = (rr <= np.log10(2000*c.au2cm)) & (rr >= np.log10(100*c.au2cm))
  
  #a, b = np.polyfit(rr[ii],data[ii],1,w=data[ii])

  return {'x':x,'y':y,'s0':s0,'lower_error':lower_error,'upper_error':upper_error,\
          'nbin':nbin,'xlog':xlog,'ylog':ylog,'histxy':histxy,'histx':histx,\
          'histy':histy,'coefficients':[a,10**b]}

# ------- Helper Function
def wmedian(x,w):
  """
  Calculate weighted median
  """
  ix = np.argsort(x)
  xx, ww = x[ix], w[ix]
  nx = len(xx)
  Sn = np.cumsum(ww)
  pn = 1./Sn[-1] * (Sn - ww/2) # weighted percentiles
#   print pn
  index = np.searchsorted(pn,0.5)
#   print index
  if index == 0:
    return xx[index]
  elif index == nx:
    return xx[index-1]
  elif np.abs(pn[index]-0.5) < np.abs(pn[index-1]-0.5):
    return xx[index]
  elif np.abs(pn[index]-0.5) > np.abs(pn[index-1]-0.5):
    return xx[index-1]
  elif np.abs(pn[index]-0.5) == np.abs(pn[index-1]-0.5):
    return np.mean(xx[index-1:index+1])
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def convert2spherical(vector,degree=False):
  """
  Convert rectangular vector to spherical coordinates
  """
  assert len(vector) is 3, "vector must have three coordinates (xyz)"
  r     = np.linalg.norm(vector)
  theta = np.arccos(vector[2]/r)
  phi   = np.arctan2(vector[1],vector[0])
  if degree is True:
    theta *= 360/2/np.pi
    phi   *= 360/2/np.pi
  return np.array([r,theta,phi])
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def rotation_axis(cc,**kw):
  """
  Determine unit vector with same direction as the angular momentum vector of a system 
  given a cell or cube dictionary.
  """
  # ------- Handle Keywords
  #densitycap = kw.get('densitycap',True)
  rcut      = kw.get('rcut',500)        # alias
  rout      = kw.get('rout',rcut)       # radius for calculation cut-off in AU
  rin       = kw.get('rin',0)           # innermost radius to be used in AU
  
  cm2au  = 6.68458712e-14
  
  if cc['dictype'] == 'cube':
    z, y, x = np.meshgrid(cc['zrange'],cc['yrange'],cc['xrange'],indexing='ij')
    r       = np.vstack((x.ravel(),y.ravel(),z.ravel())).T
    svx, svy, svz = cc['sink_velocity'][cc['sink_number']]
    v       = np.vstack((cc['vx'].ravel()-svx,cc['vy'].ravel()-svy,cc['vz'].ravel()-svz)).T
    mass    = cc['density'].ravel() * np.median(np.diff(cc['xrange']))**3
    density = cc['density'].ravel()/c.unit_mass_in_g/c.molecular_mass
  elif cc['dictype'] == 'cell':
    r    = cc['r']
    v    = cc['velocity'] - cc['sink_velocity'][cc['sink_number']]
    mass = cc['density']*cc['dx']**3
    density = cc['density']/c.unit_mass_in_g/c.molecular_mass
  else:
    raise IOError('Input dictionary needs to contain either cube or cell data')
  
  assert rout > rin, "Error: rin <= rout, %f %f" % (rin, rout)
  
  #dcap = np.percentile(density,98)
  
  rrin  = np.sqrt((r**2).sum(axis=1)) >  rin/cm2au
  rrout = np.sqrt((r**2).sum(axis=1)) <= rout/cm2au
  #ddens = density >= densitycap
  
  #if densitycap:
    #index = rrin & rrout & ddens
  #else:
    #index = rrin & rrout
  
  index = rrin & rrout
  
  omega  = (density*np.cross(r,v).T)[:,index].sum(axis=1)/density[index].sum() # angular momentum vector
  omega /= np.linalg.norm(omega) # make unit vector
  
  return omega
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def alpha_parameter(c,**kw):
  """
  Determine alpha parameter (alpha = atan[vrad/vphi])
  """
  # ------- Handle Keywords
  rcut   = kw.get('rcut',500) # alias
  rout      = kw.get('rout',rcut)       # radius for calculation cut-off in AU
  omega  = kw.get('omega',None) # rotation axis
  
  cm2au = 6.68458712e-14
  
  if c['dictype'] == 'cube':
    z, y, x = np.meshgrid(c['zrange'],c['yrange'],c['xrange'],indexing='ij')
    r       = np.vstack((x.ravel(),y.ravel(),z.ravel())).T
    svx, svy, svz = c['sink_velocity'][c['sink_number']]
    v       = np.vstack((c['vx'].ravel()-svx,c['vy'].ravel()-svy,c['vz'].ravel()-svz)).T
    mass    = c['density'].ravel() * np.median(np.diff(c['xrange']))**3
    density = c['density']
  elif c['dictype'] == 'cell':
    r    = c['r']
    v    = c['velocity'] - c['sink_velocity'][c['sink_number']]
    mass = c['density']*c['dx']**3
    density = c['density']
  else:
    raise IOError('Input dictionary needs to contain either cube or cell data')
  
  omega = rotation_axis(c,**kw) if omega is None else omega
    
  # distance from orego
  distance = np.sqrt((r**2).sum(axis=1))
  
  h = np.dot(r,omega)
  h_vec = np.outer(h,omega)
  r_plane = np.sqrt(((r - np.outer(h,omega))**2).sum(axis=1))
  
  # calculate phi unit vectors
  phi = np.cross(omega,r)
  phi = (phi.T/np.sqrt((phi**2).sum(axis=1))).T
  
  # radial velocity and phi velocity
  vrad = (v*r).sum(axis=1)/distance
  vphi = (v*phi).sum(axis=1)
    
  # mass weighted inside rcut
  index = (distance <= rcut/cm2au)# & (np.abs(h) <= r_plane**1.5)
  if index.sum() == 0:
    return np.nan
  #vrad  = -(vrad[index]*mass[index]).sum()/(mass[index]).sum() # make inward positive
  #vphi  = (vphi[index]*mass[index]).sum()/(mass[index]).sum()
  vrad  = -(vrad[index]*density[index]).sum()/(density[index]).sum() # make inward positive
  vphi  = (vphi[index]*density[index]).sum()/(density[index]).sum()
  alpha = np.arctan(vrad/np.abs(vphi))
  
  return alpha
  
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def trace_density(c, r, v,**kw):
  """
  Trace density along v (unit vector) at distances r (measured in AU) from centre of
  coordinate system.
  """
  
  # ------- Check Arguments
  assert round(np.linalg.norm(v)-1.,7) == 0., "v must be a unit vector"
  
  cm2au  = 6.68458712e-14
  eps    = 100/cm2au
  
  if c['dictype'] == 'cube':
    z , y , x  = np.meshgrid(c['zrange'],c['yrange'],c['xrange'],indexing='ij')
    z , y , x  = z.ravel(), y.ravel(), x.ravel()
    density    = c['density'].ravel()
  elif c['dictype'] == 'cell':
    x , y , z  = c['r'][:,0], c['r'][:,1], c['r'][:,2]
    density    = c['density']
  else:
    raise IOError('Input dictionary needs to contain either cube or cell data')
  
  coordinate_vectors = np.vstack((x,y,z))
  projection_scalars = np.dot(v,coordinate_vectors)
  projection_vectors = np.outer(v,projection_scalars)
  distances          = np.sqrt(((coordinate_vectors - projection_vectors)**2).sum(axis=0))
  print(np.min(distances), np.max(distances))
  
  index = np.where(distances <= eps)
  
  xx, yy, zz = v[0]*r/cm2au, v[1]*r/cm2au, v[2]*r/cm2au
  
  #minxx, maxxx = np.min(xx), np.max(xx)
  #minyy, maxyy = np.min(yy), np.max(yy)
  #minzz, maxzz = np.min(zz), np.max(zz)
 
  #index = np.vstack((x >= minxx-eps, x <= maxxx+eps, \
                     #y >= minyy-eps, y <= maxyy+eps, \
                     #z >= minzz-eps, z <= maxzz+eps)).all(axis=0)
  #count = index.sum()
  print(len(x), len(index[0]))
  
  return griddata((x[index],y[index],z[index]),density[index],(xx,yy,zz),method='nearest')
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def trace_densities(c, r, v,**kw):
  """
  Trace several density vectors. Calculate average and sigma and return
  """
  
  # ------- Keywords
  dtheta = kw.get('dtheta',45.)
  num    = kw.get('num',10)
  
  # convert v to spherical coordinates
  v_spher = convert2spherical(v,degree=False)
  # convert add dtheta and convert back to rectangular
  dtheta *= 2*np.pi/360. # convert to radian
  v_rect  = np.array([v_spher[0]*np.sin(v_spher[1] + dtheta)*np.cos(v_spher[2]),\
                      v_spher[0]*np.sin(v_spher[1] + dtheta)*np.sin(v_spher[2]),\
                      v_spher[0]*np.cos(v_spher[1] + dtheta)])
  
  dphi = np.linspace(0,2*np.pi,num=num,endpoint=False)
  
  cphi, sphi = np.cos(dphi), np.sin(dphi)
  # rotate v_rect around v with dphi
  v_rot_x =      (cphi + v[0]**2*(1-cphi))*v_rect[0] + (v[0]*v[1]*(1-cphi)-v[2]*sphi)*v_rect[1] + (v[0]*v[2]*(1-cphi) + v[1]*sphi)*v_rect[2]
  v_rot_y = (v[1]*v[0]*(1-cphi)+v[2]*sphi)*v_rect[0] +      (cphi + v[1]**2*(1-cphi))*v_rect[1] + (v[1]*v[2]*(1-cphi) - v[0]*sphi)*v_rect[2]
  v_rot_z = (v[2]*v[0]*(1-cphi)-v[1]*sphi)*v_rect[0] + (v[2]*v[1]*(1-cphi)+v[0]*sphi)*v_rect[1] +          (cphi+v[2]**2*(1-cphi))*v_rect[2]
  
  v_rot = np.vstack((v_rot_x,v_rot_y,v_rot_z))
  
  interp = np.empty((num,len(r))) # initialize interpolated array
  for i in range(num): # run trace_desity for each rotation
    interp[i,:] = trace_density(c,r,v_rot[:,i])
  
  mn  = np.mean(interp,axis=0) # mean
  std = np.std(interp,axis=0) # standard deviation
  
  return mn, std
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def _rotcoord(data,axis,inverse=False):
  """
  Rotate coordinates so that new z axis coincides with new axis.
  The coordinate system is orientated so that the new x axis is pointing towards the
  ascending node.
  I use Euler angles. See e.g. description on http://en.wikipedia.org/wiki/Euler_angles
  Calling Sequence:
    _rotcoord(data,axis)
  Input:
    data           :  data coordinates in cartesian coordinatex (ndata,3)
    axis           :  direction of new z axis
  Returns:
    reoriented coordinates
  """
  
  # Euler angles
  
  if len(axis) != 3:
    raise ValueError('New z-axis must contain three elements')
  
  unit_axis = np.asarray(axis)/np.linalg.norm(axis)
  
  alpha, beta, gamma = np.arctan2(unit_axis[0],-unit_axis[1]), np.arccos(unit_axis[2]), 0.
  
  # Trigonometry
  s1, c1 = np.sin(alpha), np.cos(alpha)
  s2, c2 = np.sin(beta) , np.cos(beta)
  s3, c3 = np.sin(gamma), np.cos(gamma)
  
  # Rotation Matrix
  R = np.array([[c1*c3-c2*s1*s3, -c1*s3-c2*c3*s1,  s1*s2 ],\
                [c3*s1+c1*c2*s3,  c1*c2*c3-s1*s3, -c1*s2 ],\
                [s2*s3         ,  c3*s2         ,  c2    ]])
  
  if inverse is True:
    return np.dot(R,data)
  else:
    return np.dot(data,R)
  
def rotate_coordinates(c,**kw):
  """
  Rotate coordinates so that new z axis coincides with new axis.
  The coordinate system is orientated so that the new x axis is pointing towards the
  ascending node.
  I use Euler angles. See e.g. description on http://en.wikipedia.org/wiki/Euler_angles
  """
  
  # keywords
  omega = kw.get('omega',None)
  
  if c['dictype'] == 'cube':
    raise NotImplementedError("This only works for cell data so far")
  
  omega = rotation_axis(c,**kw) if omega is None else omega
  
  new_cell = c.copy()
  
  new_cell['r']             = _rotcoord(c['r'],omega)
  new_cell['velocity']      = _rotcoord(c['velocity'],omega)
  new_cell['sinkr']         = _rotcoord(c['sinkr'],omega)
  new_cell['sink_velocity'] = _rotcoord(c['sink_velocity'],omega)
  
  return new_cell
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def make_disk(ccell,**kw):
  """
  make_disk
  """
  
  # ------- keyword parameters 
  nbin     = kw.get('nbin',None)
  rout     = kw.get('rout',500)
  zoomau   = kw.get('zoomau',None)
  quadrant = kw.get('quadrant',1)
  
  # ------- sanity checks
  assert quadrant in [1,2,4], "you plot either 1, 2 or 4 quadrants, not %i" % quadrant
  
  if zoomau is None:
    zoomau    = [2*rout,2*rout]
    zoomau[0] = zoomau[0]/2. if quadrant in [1,2] else zoomau[0]
    zoomau[1] = zoomau[1]/2. if quadrant == 1 else zoomau[1]
  
  if np.rank(zoomau) == 0:
    zoomau = [zoomau,zoomau]
  elif np.rank(zoomau) == 1:
    assert len(zoomau) == 2, "zoomau must have two elements"
  else:
    raise ValueError("zoomau cannot have rank > 1")
  
  rcut = zoomau[0]/2 if quadrant == 4 else zoomau[0]
  zcut = zoomau[1]/2 if quadrant in [2,4] else zoomau[1]
  
  if nbin is None:
    dxmin = np.min(ccell['dx'])*c.cm2au
    nbin  = [int(zoom/dxmin) for zoom in zoomau]
  elif np.rank(nbin) == 0:
    nbin  = [nbin,nbin]
  elif np.rank(nbin) == 1:
    assert len(nbin) == 2, "nbin must have two elements"
  else:
    raise ValueError("nbin cannot have rank > 1")
  
  # ------- rotate coordinate system to put disk in xy-plane
  cell = rotate_coordinates(ccell,**kw)
  
  # ------- Read and convert coordinates
  pos, dx = cell['r']*c.cm2au, cell['dx']*c.cm2au # convert to AU
  v       = cell['velocity']*1e-5                 # convert to km/s
  density = cell['density']/c.molecular_mass/c.unit_mass_in_g # convert to cm-3
  
  # ------- Disk coordinates
  distance = np.sqrt((pos**2).sum(axis=1)) # distance from orego
  r        = np.sqrt(pos[:,0]**2 + pos[:,1]**2)
  z        = pos[:,2]
  theta    = np.arctan2(pos[:,1],pos[:,0])
  
  # ------- cut-out
  index    = np.vstack((np.abs(r) <= rcut,np.abs(z) <= zcut)).all(axis=0)
  
  if index.sum() <= 10: # this'll never be a disk
    return None
  
  r        = r[index]
  z        = z[index]
  theta    = theta[index]
  v        = v[index,:]
  pos      = pos[index,:]
  density  = density[index]
  dx       = dx[index]
  distance = distance[index]
  
#   v -= v.mean(axis=0)
  v -= cell['sink_velocity'][cell['sink_number']]*1e-5
  
  # ------- calculate velocity component along the disk (v_phi)
  L    = np.array([0.,0.,1.])
  phi  = np.cross(L,pos)
  phi  = (phi.T/np.sqrt((phi**2).sum(axis=1))).T # make unit vectors
  vphi = (v*phi).sum(axis=1)
  vr   = (v[:,:2]*pos[:,:2]).sum(axis=1)/r
  vz   = v[:,2]
  
  # ------- handle sign of data
  if quadrant == 1:
    index,     = np.where(z < 0)
    z[index]  *= -1
    vz[index] *= -1
  elif quadrant == 4:
    index,       = np.where(theta < 0)
    r[index]    *= -1
    vphi[index] *= -1
    vr[index]   *= -1

  # ------- bin data (disk)
  bincount, redges, zedges = np.histogram2d(r,z,bins=nbin,weights=dx**3)
  bin_dens = np.histogram2d(r,z,bins=nbin,weights=density*dx**3)[0]/bincount
  bin_vphi = np.histogram2d(r,z,bins=nbin,weights=vphi*dx**3)[0]/bincount
  bin_vr   = np.histogram2d(r,z,bins=nbin,weights=vr*dx**3)[0]/bincount
  bin_vz   = np.histogram2d(r,z,bins=nbin,weights=vz*dx**3)[0]/bincount
  bin_dens, bin_vphi, bin_vr, bin_vz = bin_dens.T, bin_vphi.T, bin_vr.T, bin_vz.T
  
  # ------- edges to ranges
  rrange = np.interp(np.arange(len(redges)-1)+0.5,np.arange(len(redges)),redges)
  zrange = np.interp(np.arange(len(zedges)-1)+0.5,np.arange(len(zedges)),zedges)
  
  # ------- 2d interpolation
  grid_r, grid_z = np.meshgrid(rrange,zrange)
  index     = np.isfinite(bin_dens)
  bin_dens  = griddata(np.vstack((grid_r[index],grid_z[index])).T,bin_dens[index],(grid_r,grid_z),method='nearest')
  index     = np.isfinite(bin_vphi)
  bin_vphi  = griddata(np.vstack((grid_r[index],grid_z[index])).T,bin_vphi[index],(grid_r,grid_z),method='nearest')
  index     = np.isfinite(bin_vr)
  bin_vr    = griddata(np.vstack((grid_r[index],grid_z[index])).T,bin_vr[index],(grid_r,grid_z),method='nearest')
  index     = np.isfinite(bin_vz)
  bin_vz    = griddata(np.vstack((grid_r[index],grid_z[index])).T,bin_vz[index],(grid_r,grid_z),method='nearest')
  
  bin_vel   = np.dstack((bin_vr,bin_vz,bin_vphi))

  return bin_dens, bin_vel, redges, zedges, np.vstack((vr,vz,vphi)).T, r, z, density, dx

def fit_disk(bin_dens,rrange,zrange):
  
  def model(params,rdata,zdata):
    
    ppdisk_sig0, ppdisk_hrdisk, ppdisk_rout, envelope_rho0, envelope_rout, bg_value = 10**params
    
    ppdisk_plsig1, envelope_plrho = -1., -1.5
    
    sigma = ppdisk_sig0 * (np.sqrt(rdata**2 + zdata**2) / ppdisk_rout)**ppdisk_plsig1
    hr    = ppdisk_hrdisk * np.sqrt(rdata**2 + zdata**2)
    rho0  = sigma / (hr * np.sqrt(2.*np.pi))
    rho1  = np.exp(-0.5*(np.abs(zdata)/hr)**2)
    rho_ppdisk = rho0 * rho1
   
    rho_ppdisk = np.where(np.sqrt(rdata**2 + zdata**2) >= ppdisk_rout,0.,rho_ppdisk)
    
    rho_envelope = envelope_rho0 * (np.sqrt(rdata**2 + zdata**2) / envelope_rout)**envelope_plrho
    rho_envelope = np.where(np.sqrt(rdata**2 + zdata**2) >= envelope_rout,0.,rho_envelope)
    
    rho_bg = np.empty(rdata.shape)
    rho_bg.fill(bg_value)
    
    print(params)
    print(np.log10(rho_ppdisk).max(), np.log10(rho_envelope).max(), np.log10(rho_bg).max())
    
    #return np.maximum(np.maximum(rho_ppdisk,rho_envelope),rho_bg)
    return rho_ppdisk + rho_envelope + rho_bg
  
  def err_fun(params,rdata,zdata,dens):
    return (dens.ravel() - model(params,rdata,zdata).ravel())
  
  x0 = np.array([9.,.5,2.,4.,3.5,3.])
  
  #rdata,zdata = np.meshgrid(rrange,zrange)
  rdata, zdata = rrange, zrange
  
  res, ier = leastsq(err_fun, x0, args=(rdata,zdata,bin_dens))
  
  print(res, ier)
  
  return res
