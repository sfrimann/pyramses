#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, absolute_import, division

import numpy as np
import os
import h5py
import astropy

import astropy.units as u

class Container(object):
  """
  Container class for pyramses objects. Contains various constants defined for RAMSES outputs as well as scale factors to physical units. The class is not typically called directly, but is a parent class to several pyramses subclasses
  Calling Sequence:
    container = Container(nout,**keywords)
  
  """
  
  def __init__(self,*args,**kw):
    # ------- Handle keywords
    jet_factor     = kw.get('jet_factor',None)             # mass loss factor
    lmax           = kw.get('lmax',None)                   # alias
    lvlmax         = kw.get('lvlmax',lmax)                 # max amr level
    main_directory = kw.get('main_directory',None)         # alias
    datadir        = kw.get('datadir',main_directory)      # data directory for snapshots
    scales_file    = kw.get('scales_file','./scales.fits') # path to scales.fits
    scale_d        = kw.get('scale_d',None)                # Convert code density to g/cm3
    scale_l        = kw.get('scale_l',None)                # Convert code length to cm
    scale_t        = kw.get('scale_t',None)                # Convert code time to s
    reference      = kw.get('reference',None)              # reference dictionary for position and velocity
    
    # ------- parse input
    if len(args) == 0:
      self.nout = None # initialize empty class. Indicates that data should be loaded
    elif len(args) == 1:
      self.nout = args[0]
    
      # ------- read info files
      self.scales_dict = self.read_scales_file(scales_file)
      self.datadir = datadir
      self.info_dict = self.read_info_file(self.nout,self.datadir)
    
      # ------- Set parameters
      self.lvlmax      = lvlmax
      self.lvlmin      = None
      self.time        = None
      self.jet_factor  = jet_factor
      self.scale_d     = scale_d
      self.scale_l     = scale_l
      self.scale_t     = scale_t
      self.reference   = reference
  ############### Save method ###############
  def save(self,grp):
    """
    save data
    """
    
    try:
      abspos = self.reference['abspos'].to(self.code_length).value
    except:
      abspos = self.reference['abspos']
    
    try:
      absvel = self.reference['absvel'].to(self.code_velocity).value
    except:
      absvel = self.reference['absvel']
    
    grp.attrs['nout']          = self.nout
    grp.attrs['datadir']       = self.datadir
    grp.attrs['lvlmin']        = self.lvlmin
    grp.attrs['lvlmax']        = self.lvlmax
    grp.attrs['time']          = self._time
    grp.attrs['jet_factor']    = self.jet_factor
    grp.attrs['scale_d']       = self.scale_d
    grp.attrs['scale_l']       = self.scale_l
    grp.attrs['scale_t']       = self.scale_t
    grp.attrs['ref_abspos']    = abspos
    grp.attrs['ref_absvel']    = absvel
    grp.attrs['reftype']       = self.reference['reftype']
    
  ############### Load method ###############
  def load(self,grp):
    
    self.nout        = grp.attrs['nout']
    self.datadir     = grp.attrs['datadir']
    self.lvlmax      = grp.attrs['lvlmax']
    self.lvlmin      = grp.attrs['lvlmin']
    self.time        = grp.attrs['time']
    self.jet_factor  = grp.attrs['jet_factor']
    self.scale_d     = grp.attrs['scale_d']
    self.scale_l     = grp.attrs['scale_l']
    self.scale_t     = grp.attrs['scale_t']
    self.reference   = dict(abspos=grp.attrs['ref_abspos']*self.code_length,
                            absvel=grp.attrs['ref_absvel']*self.code_velocity,
                            reftype=grp.attrs['reftype'])
  
  ############### read_scales_file method ###############
  def read_scales_file(self,fname):
    """
    read scales file. Takes the filename (with path) as input
    """
    try:
      from astropy.io import fits as pyfits
    except:
      import pyfits
    
    try:
      header = pyfits.getheader(fname,0)
    except IOError:
      header = None
    return header
  
  def read_info_file(self,nout,datadir):
    """
    Read info_nout.txt file from RAMSES outputs. Takes the output number and model
    directory as input
    """
    from .utils import rd_info
    
    try:
      fname = os.path.join(datadir,'output_{0:05d}/info_{0:05d}.txt'.format(nout))
      info  = rd_info(fname)
    except:
      info = None
    return info
  
  @property
  def reference(self):
    """
    Reference dictionary. Must contain three keys:
      abspos        : Reference position in either physical or code units (three element 
                      array. Default: [0.,0.,0.])
      absvel   : Reference velocity in physical or code units (three element array. 
                      Default: [0.,0.,0.])
      reftype : String telling where the reference is from
    """
    return self._reference
  
  @reference.setter
  def reference(self,value):
    if value is None:
      self._reference = dict(abspos = np.array([0.5,0.5,0.5]), absvel = np.array([0.,0.,0.]), reftype = 'default')
    else:
      assert isinstance(value,dict), "The reference has to be a dictionary"
      assert all([key in value for key in ['abspos', 'absvel', 'reftype']]), \
            "reference dictionary needs to have keys: abspos, absvel and reftype"
      assert len(value['abspos']) == 3, "reference['abspos'] must have length 3"
      assert len(value['absvel']) == 3, "reference['absvel'] must have length 3"
      
      if isinstance(value['abspos'],(list,tuple)):
        raise NotImplementedError('Make sure abspos is a numpy array. Lists and tuples not implemented yet')
      if isinstance(value['absvel'],(list,tuple)):
        raise NotImplementedError('Make sure absvel is a numpy array. Lists and tuples not implemented yet')
      
      self._reference = value
  
  ######## snapshot_time property
  @property
  def time(self):
    """
    Snapshot time from info file. Returns None if not found in info file
    """
    return self._time * self.code_time
  
  @time.setter
  def time(self,value):
    if value is not None:
      self._time = value
      return
    
    try:
      self._time = self.info_dict['time']
      return
    except:
      self._time = 0.
      return
    
  ######## datadir property
  @property
  def datadir(self):
    """
    Data directory. Can be given as input or gotten from scales file, and is otherwise
    set to '.'
    """
    return self._datadir
  
  @datadir.setter
  def datadir(self,value):
    if value is not None:
      self._datadir = value
      return
    
    try:
      self._datadir = self.scales_dict['datadir']
    except:
      self._datadir = '.'
  
  ######### lvlmax property
  @property
  def lvlmax(self):
    """
    Maximum AMR level. Can be given as input or gotten from scales file or info file, and
    is otherwise set to 6
    """
    return self._lvlmax
  
  @lvlmax.setter
  def lvlmax(self,value):
    if value is not None:
      self._lvlmax = value
      return
    
    try:
      self._lvlmax = self.scales_dict['lvlmax']
    except:
      try:
        self._lvlmax = self.info_dict['levelmax']
      except:
        self._lvlmax = 6
  
  ######## lvlmin property
  @property
  def lvlmin(self):
    """
    Minimum AMR level. Important for finding the base grid. Can be given as input or
    gotten from the info file, and is otherwise set to 6.
    It is always checked if lvlmin is <= to lvlmax and if not lvlmin is set to lvlmax.
    """
    return min(self._lvlmin,self.lvlmax)
  
  @lvlmin.setter
  def lvlmin(self,value):
    if value is not None:
      self._lvlmin = value
      return
    
    try:
      self._lvlmin = self.scales_dict['lvlmin']
      return
    except:
      try:
        self._lvlmin = self.info_dict['levelmin']
      except:
        self._lvlmin = 6
  
  ######## jet_factor property
  @property
  def jet_factor(self):
    """
    Assumed mass loss factor for sink particles. Can be given as input or gotten from
    scales file and is otherwise set to 1.
    """
    return self._jet_factor
  
  @jet_factor.setter
  def jet_factor(self,value):
    if value is not None:
      self._jet_factor = value
      return
    
    try:
      self._jet_factor = self.scales_dict['jet_factor']
    except:
      self._jet_factor = 1.
  
  ######## scale_d property
  @property
  def scale_d(self):
    """
    Convertion factor from code density to density in g/cm3. Given as input or gotten from
    scales or info file. If none is set an exception is thrown
    """
    return self._scale_d
  
  @scale_d.setter
  def scale_d(self,value):
    if value is not None:
      self._scale_d = value
      return
    
    try:
      self._scale_d = self.scales_dict['scale_d']
      return
    except:
      try:
        self._scale_d = self.info_dict['unit_d']
      except:
        raise ValueError("scale_d has to be set")

  ######## scale_l property
  @property
  def scale_l(self):
    """
    Conversion factor from code length to length in cm. Given as input or gotten from
    scales or info file. If none is set an exeption is thrown
    """
    return self._scale_l
  
  @scale_l.setter
  def scale_l(self,value):
    if value is not None:
      self._scale_l = value
      return
    
    try:
      self._scale_l = self.scales_dict['scale_l']
      return
    except:
      try:
        self._scale_l = self.info_dict['unit_l']
      except:
        raise ValueError("scale_l has to be set")

  ######## scale_t property
  @property
  def scale_t(self):
    """
    Conversion factor from code time to time in s. Given as input or gotten from scales or
    info file. If none is set an exception is thrown
    """
    return self._scale_t
  
  @scale_t.setter
  def scale_t(self,value):
    if value is not None:
      self._scale_t = value
      return
    
    try:
      self._scale_t = self.scales_dict['scale_t']
      return
    except:
      try:
        self._scale_t = self.info_dict['unit_t']
      except:
        raise ValueError("scale_t has to be set")

  ######## scale_m property
  @property
  def scale_m(self):
    """
    Conversion factor from code mass to mass in g. Calculated from scale_d and scale_l
    """
    return self.scale_d*self.scale_l**3

  ######## scale_e property
  @property
  def scale_e(self):
    """
    Conversion factor from code energy density to energy density in erg/cm3. Calculated
    from scale_d, scale_l, and scale_t
    """
    return self.scale_d*(self.scale_l/self.scale_t)**2

  ######## scale_B property
  @property
  def scale_B(self):
    """
    Conversion factor from code magnetic field to magnetic field in gauss. Calculated from
    scale_d, scale_l, and scale_t
    """
    return np.sqrt(8*np.pi*self.scale_d)*(self.scale_l/self.scale_t)
  
  ######## Code units
  @property
  def code_density(self):
    """
    code density unit using astropy.units, with conversion to physical units
    """
    return u.def_unit('code_density', self.scale_d * u.g/u.cm**3)
  
  @property
  def code_length(self):
    """
    code length unit using astropy.units, with conversion to physical units
    """
    return u.def_unit('code_length', self.scale_l * u.cm)
  
  @property
  def code_time(self):
    """
    code time unit using astropy.units, with conversion to physical units
    """
    return u.def_unit('code_time', self.scale_t * u.s)
  
  @property
  def code_velocity(self):
    """
    code velocity unit using astropy.units, with conversion to physical units
    """
    return u.def_unit('code_velocity', self.scale_l/self.scale_t * u.cm/u.s)
  
  @property
  def code_mass(self):
    """
    code mass unit using astropy.units, with conversion to physical units
    """
    return u.def_unit('code_mass', self.scale_m * u.g)
  
  @property
  def code_pressure(self):
    """
    code pressure unit using astropy.units, with conversion to physical units
    """
    return u.def_unit('code_pressure', self.scale_e * u.erg/u.cm**3)
  
  @property
  def code_Bfield(self):
    """
    code magnetic field unit using astropy.units, with conversion to physical units
    """
    return u.def_unit('code_Bfield', self.scale_B * u.gauss)
  
  @property
  def code_logAccretion(self):
    """
    code log accretion unit using astropy.units, with conversion to physical units
    """
    return u.dex(u.def_unit('code_accretion',self.scale_m/self.scale_t * u.g/u.s))

##########################################################################################
######################## Cell Container ##################################################
##########################################################################################
class Cell(Container,object):
  """
  Cell class for loading RAMSES data
  """
  def __init__(self,*args,**kw):
    
    # ------- Run parent class init
    Container.__init__(self,*args,**kw)
    # ------- Set default properties
    self._pos        = None
    self._dx         = None
    self._velocity   = None
    self._Bleft      = None
    self._Bright     = None
    self.level       = None
    self.amrtree     = None
    self.full_oct    = None
    self.field_names = None
    self.field_types = None
    self.quantities = {}
  
  def save(self,filename='cell.hdf5',directory='.',group='/'):
    """
    save method
    """
    
    f = h5py.File(os.path.join(directory,filename), "w")
    try:
      grp = f if group == '/' else f.create_group(group)
      dset = grp.create_dataset('pos',data=self._pos,compression='gzip')
      dset = grp.create_dataset('dx',data=self._dx,compression='gzip')
      dset = grp.create_dataset('level',data=self.level,compression='gzip')
      dset = grp.create_dataset('amrtree',data=self.amrtree,compression='gzip')
      
      grp.attrs['full_oct'] = self.full_oct
      Container.save(self,grp)
      
      grp2 = grp.create_group('quantities')
      for fn,ft in zip(self.field_names,self.field_types):
        dset = grp2.create_dataset(fn,data=self.quantities[fn],compression='gzip')
        dset.attrs['field_type'] = ft
    finally:
      f.close()
    
  def load(self,filename='cell.hdf5',directory='.',group='/'):
    """
    load method
    """
    
    f = h5py.File(os.path.join(directory,filename), 'r')
    try:
      grp = f if group == '/' else f.require_group(group)
      self._pos    = np.asarray(grp['pos'])
      self._dx     = np.asarray(grp['dx'])
      self.level   = np.asarray(grp['level'])
      self.amrtree = np.asarray(grp['amrtree'])
      
      self.full_oct = grp.attrs['full_oct'] if 'full_oct' in grp.attrs else False
      
      Container.load(self,grp)
      
      self.field_names, self.field_types = [], []
      
      grp2 = grp.require_group('quantities')
      for fn in grp2.keys():
        self.quantities[fn] = np.asarray(grp2[fn])
        self.field_names.append(fn)
        self.field_types.append(grp2[fn].attrs['field_type'])
    finally:
      f.close()
      
  def amr2cell(self,**kw):
    """
    Method calling amr2cell through the make_cell function in cell.py. Takes the same
    keywords as make_cell except for 'r', 'datadir', 'lvlmax', and 'lvlmin', which are set
    during the call to __init__
    """
    from .cell import make_cell
  
    # ------- Handle keywords
    deltar         = kw.get('deltar',[1.0, 1.0, 1.0]) # alias
    dr             = kw.get('dr',deltar)              # whole box
    field_names    = kw.get('field_names',['density','vx','vy','vz','Blx','Bly','Blz','Brx','Bry','Brz','pressure'])
    field_types    = kw.get('field_types',['density','velocity','velocity','velocity','Bfield','Bfield','Bfield','Bfield','Bfield','Bfield','pressure'])
    
    if isinstance(self.reference['abspos'],astropy.units.quantity.Quantity):
      r = self.reference['abspos'].to(self.code_length).value
    elif isinstance(self.reference['abspos'],np.ndarray):
      r = self.reference['abspos']
    
    # ------- Handle dr
    # if dr is string extract length and convert
    # examples are dr = '11e22 cm', dr = '20000 au', dr = '0.2 parsec'
    if isinstance(dr,str):
      dr, unit = dr.lower().split(' ')
      dr = float(dr) # convert to number
      if not unit in ['cm', 'au', 'parsec']:
        raise ValueError("length unit %s not in list of allowed units (cm, au, parsec)" % unit)
      if unit == 'cm':
        dr = (dr * u.cm).to(self.code_length).value
      elif unit == 'au':
        dr = (dr * u.au).to(self.code_length).value
      elif unit == 'parsec':
        dr = (dr * u.pc).to(self.code_length).value
    
    # if dr is scalar make it a three element list
    if isinstance(dr,float):
      dr = [dr, dr, dr]
  
    # check if any elements in dr is greater than box length
    if any([i > 1 for i in dr]):
      raise IOError("One or more elements in dr greater than box size")
    
    # ------- set keywords for make_cell
    kw['r']  = r
    kw['dr'] = dr
    kw['datadir'] = self.datadir
    kw['lvlmax'] = self.lvlmax
    kw['lvlmin'] = self.lvlmin
    
    # ------- Call make_cell
    cell_dict = make_cell(self.nout,**kw)
    
    # ------- Make the output quantities ready for the class
    self._pos   = cell_dict['r']
    self._dx    = cell_dict['dx']
    self.level  = cell_dict['ilevel']
    self.lvlmin = cell_dict['ilevel'].min()
    
    self.field_names = []
    self.field_types = []
    for i,field_name in enumerate(field_names):
      try:
        self.quantities[field_name] = cell_dict['v'][:,i]
      except IndexError:
        pass
      else:
        self.field_names.append(field_names[i])
        self.field_types.append(field_types[i])
  
  def amrsort(self,particle=None,**kw):
    
    from .amr import amrsort
    
    if self.amrtree is not None:
      raise ValueError('amrtree is not None. Apparently amrtree ran already')
    if self._pos is None:
      raise ValueError('Cell class is empty. Have you not run amr2cell or loaded data yet')
    
    if particle is not None:
      indices, amrtree, cellid = amrsort(self.pos.to(self.code_length).value,self.dx.to(self.code_length).value,particle=particle,**kw)
    else:
      indices, amrtree = amrsort(self.pos.to(self.code_length).value,self.dx.to(self.code_length).value,**kw)
    
    self.full_oct = kw['full_oct'] if 'full_oct' in kw else False
    self.amrtree = amrtree
    self._pos = self._pos[indices,:]
    self._dx  = self._dx[indices]
    self.level = self.level[indices]
    self.lvlmin = self.level.min()
    for key in self.quantities:
      self.quantities[key] = self.quantities[key][indices]
    if self._velocity is not None:
      self._velocity = self._velocity[indices,:]
    if self._Bleft is not None:
      self._Bleft = self._Bleft[indices,:]
    if self._Bright is not None:
      self._Bright = self._Bright[indices,:]
    
    if particle is not None:
      return cellid
  
  def __getattr__(self,name):
    
    if name in self.field_names:
      
      field_type = [ft for fn,ft in zip(self.field_names,self.field_types) if fn == name][0]
      
      if field_type == 'density':
        code_unit = self.code_density
      elif field_type == 'velocity':
        code_unit = self.code_velocity
      elif field_type == 'Bfield':
        code_unit = self.code_Bfield
      elif field_type == 'pressure':
        code_unit = self.code_pressure
      elif field_type == 'mass':
        code_unit = self.code_mass
      
      return self.quantities[name] * code_unit
    else:
      raise AttributeError
  
  @property
  def numdens(self):
    """
    Gas number density. A mean molecular weight of 2.37 is assumed. This can be calculated
    Like so:
    ngas = rho/(mu*mH)
    ni   = Xi*rho/(Ai*mH) where Xi is the mass abundance of element i, and Ai the molecular weight of that element
    so 
    1/mu = X/2 + Y/4 + Z/12 => mu = 2.37 if X = 0.7, Y = 0.28, and Z = 0.02
    """
    
    return (self.density / 2.37 / (1. * u.Da)).cgs
  
  @property
  def H2numdens(self):
    """
    H2 number density. Mass abundance of hydrogren is assumed to be 0.7 and all Hydrogren
    is assumed to be molecular
    """
    
    return (self.density * 0.7 / (1. * u.Da)).cgs
  
  @property
  def pos(self):
    """
    position relative to reference position
    """
    if self._pos is None:
      return None
    else:
      if isinstance(self.reference['abspos'],astropy.units.quantity.Quantity):
        return ((self._pos * self.code_length - self.reference['abspos'] + 0.5 * self.code_length) % (1*self.code_length)) - 0.5 * self.code_length
      elif isinstance(self.reference['abspos'],np.ndarray):
        return (((self._pos - self.reference['abspos'] + 0.5) % 1) - 0.5) * self.code_length
      else:
        raise ValueError("reference['abspos'] must be either numpy array or astropy units quantity")

  @property
  def abspos(self):
    """
    absolute positions in box
    """
    if self._pos is None:
      return None
    else:
      return self._pos * self.code_length
  
  @property
  def radius(self):
    """
    distance from reference
    """
    return np.sqrt((self.pos**2).sum(axis=1))
  
  @property
  def dx(self):
    """
    cell sizes
    """
    if self._dx is None:
      return None
    else:
      return self._dx * self.code_length
  
  @property
  def volume(self):
    """
    cell volumes
    """
    return self.dx**3

  @property
  def limit(self):
    """
    [[xmin, xmax], [ymin, ymax], [zmin, zmax]]
    """
    
    return (self.pos - self.dx[:,np.newaxis]/2).min(axis=0), (self.pos + self.dx[:,np.newaxis]/2).max(axis=0)
  
  @property
  def xlim(self):
    """
    (xmin, xmax)
    """
    
    return (self.pos[:,0] - self.dx/2).min(), (self.pos[:,0] + self.dx/2).max()

  @property
  def ylim(self):
    """
    (ymin, ymax)
    """
    
    return (self.pos[:,1] - self.dx/2).min(), (self.pos[:,1] + self.dx/2).max()

  @property
  def zlim(self):
    """
    (zmin, zmax)
    """
    
    return (self.pos[:,2] - self.dx/2).min(), (self.pos[:,2] + self.dx/2).max()

  @property
  def xrange(self):
    """
    Length of region in x direction
    """
    lim = self.xlim
    return lim[1] - lim[0]

  @property
  def yrange(self):
    """
    Length of region in y direction
    """
    lim = self.ylim
    return lim[1] - lim[0]

  @property
  def zrange(self):
    """
    Length of region in x direction
    """
    lim = self.zlim
    return lim[1] - lim[0]

  @property
  def nx(self):
    """
    Number of base grid cells in x direction
    """
    from numpy import isclose
    n = (self.xrange/self.dx.max()).value
    assert isclose(n,round(n)), "nx appears not to be a whole number"
    
    return int(round(n))

  @property
  def ny(self):
    """
    Number of base grid cells in y direction
    """
    from numpy import isclose
    n = (self.yrange/self.dx.max()).value
    assert isclose(n,round(n)), "ny appears not to be a whole number"
    
    return int(round(n))

  @property
  def nz(self):
    """
    Number of base grid cells in z direction
    """
    from numpy import isclose
    n = (self.zrange/self.dx.max()).value
    assert isclose(n,round(n)), "nz appears not to be a whole number"
    
    return int(round(n))

  @property
  def x_wall(self):
    """
    position of grid walls of base cells in x direction
    """
    n   = self.nx
    lim = self.xlim
    return np.linspace(lim[0].value,lim[1].value,num=n+1) * self.code_length

  @property
  def y_wall(self):
    """
    position of grid walls of base cells in y direction
    """
    n   = self.ny
    lim = self.ylim
    return np.linspace(lim[0].value,lim[1].value,num=n+1) * self.code_length    

  @property
  def z_wall(self):
    """
    position of grid walls of base cells in z direction
    """
    n   = self.nz
    lim = self.zlim
    return np.linspace(lim[0].value,lim[1].value,num=n+1) * self.code_length

  @property
  def mass(self):
    """
    cell masses
    """
    return (self.density * self.volume).to(self.code_mass)
  
  @property
  def absvel(self):
    """
    absolute cell velocities
    """
    if self._velocity is None:
      self._velocity = np.vstack((self.quantities['vx'],self.quantities['vy'],self.quantities['vz'])).T
    return self._velocity * self.code_velocity

  @property
  def velocity(self):
    """
    cell velocities relative to reference velocity
    """
    if self._velocity is None:
      self._velocity = np.vstack((self.quantities['vx'],self.quantities['vy'],self.quantities['vz'])).T
    if isinstance(self.reference['absvel'],astropy.units.quantity.Quantity):
      return self._velocity * self.code_velocity - self.reference['absvel']
    elif isinstance(self.reference['absvel'],np.ndarray):
      return (self._velocity - self.reference['absvel']) * self.code_velocity
    else:
      raise ValueError("reference['absvel'] must be either numpy array or astropy units quantity")
  
  @property
  def absvelmagnitude(self):
    """
    Absolute velocity magnitude
    """
    return np.sqrt((self.absvel**2).sum(axis=1))

  @property
  def velocitymagnitude(self):
    """
    Velocity magnitudes relative to reference velocity
    """
    return np.sqrt((self.velocity**2).sum(axis=1))

  @property
  def Bleft(self):
    """
    Left magnetic field
    """
    if self._Bleft is None:
      self._Bleft = np.vstack((self.quantities['Blx'],self.quantities['Bly'],self.quantities['Blz'])).T
    return self._Bleft * self.code_Bfield

  @property
  def Bright(self):
    """
    Right magnetic field
    """
    if self._Bright is None:
      self._Bright = np.vstack((self.quantities['Brx'],self.quantities['Bry'],self.quantities['Brz'])).T
    return self._Bright * self.code_Bfield

##########################################################################################
######################## Sink Container ##################################################
##########################################################################################
class Sink(Container,object):

  def __init__(self,*args,**kw):
    Container.__init__(self,*args,**kw)
    
    self._pos           = None
    self._velocity      = None
    self._mass          = None
    self._snapshot_time = None
    self._tcreate       = None
    self._texplode      = None
    self._dm            = None
    self._tflush        = None
    self.level          = None
    self.version        = None
    
    # Manually calculated accretion rates
    self._dm_manual        = None
    self._accretion_window = None
    
  def rsink(self,sink_data=None):
    """
    Read sink data using rsink
    Keywords:
      sink_data : sink dictionary from rsink. This is useful for situations where you want
                  to insert a sink record that does not come from an output directory
    """
    
    from .sink import rsink
    
    if sink_data is None:
      sink = rsink(self.nout,datadir=self.datadir)
    else:
      sink = sink_data
      self._time = sink['snapshot_time']
      self.nout  = 0
  
    self._pos           = np.vstack((sink['x'],sink['y'],sink['z'])).T
    self._velocity      = np.vstack((sink['ux'],sink['uy'],sink['uz'])).T
    self._mass          = sink['m']
    self._snapshot_time = sink['snapshot_time']
    self._tcreate       = sink['tcreate']
    self._texplode      = sink['texplode']
    self._dm            = sink['dm']
    self._tflush        = sink['tflush'] if 'tflush' in sink else None
    self.level          = sink['il']
    self.version        = sink['version']
  
  def save(self,filename='sink.hdf5',directory='.',group='/'):
    """
    Save method
    """
    
    f = h5py.File(os.path.join(directory,filename), "w")
    try:
      grp = f if group == '/' else f.create_group(group)
      dset = grp.create_dataset('pos',data=self._pos,compression='gzip')
      dset = grp.create_dataset('velocity',data=self._velocity,compression='gzip')
      dset = grp.create_dataset('mass',data=self._mass,compression='gzip')
      dset = grp.create_dataset('tcreate',data=self._tcreate,compression='gzip')
      dset = grp.create_dataset('texplode',data=self._texplode,compression='gzip')
      dset = grp.create_dataset('dm',data=self._dm,compression='gzip')
      dset = grp.create_dataset('level',data=self.level,compression='gzip')
            
      grp.attrs['snapshot_time'] = self._snapshot_time
      if self._tflush is not None:
        grp.attrs['tflush']        = self._tflush
      grp.attrs['version']         = self.version
      
      # Manual accretion rates
      if self._dm_manual is not None:
        dset = grp.create_dataset('dm_manual',data=self._dm_manual,compression='gzip')
      if self._accretion_window is not None:
        grp.attrs['accretion_window'] = self._accretion_window
      
      # Call Container save      
      Container.save(self,grp)
    finally:
      f.close()

  def load(self,filename='sink.hdf5',directory='.',group='/'):
    """
    load method
    """
    
    f = h5py.File(os.path.join(directory,filename), 'r')
    try:
      grp = f if group == '/' else f.require_group(group)
      self._pos      = np.asarray(grp['pos'])
      self._velocity = np.asarray(grp['velocity'])
      self._mass     = np.asarray(grp['mass'])
      self._tcreate  = np.asarray(grp['tcreate'])
      self._texplode = np.asarray(grp['texplode'])
      self._dm       = np.asarray(grp['dm'])
      self.level     = np.asarray(grp['level'])
      
      self._snapshot_time = grp.attrs['snapshot_time']
      if 'tflush' in grp.attrs.keys():
        self._tflush = grp.attrs['tflush']
      self.version = grp.attrs['version']
      
      # manual accretion rates
      if 'dm_manual' in grp:
        self._dm_manual = np.asarray(grp['dm_manual'])
      if 'accretion_window' in grp.attrs.keys():
        self._accretion_window = grp.attrs['accretion_window']
      
      # Call Container load      
      Container.load(self,grp)
    finally:
      f.close()

  def reference_sink(self,sink_number):
    
    assert self._pos is not None, "Sink data not yet loaded"
    
    return dict(sink_number = sink_number,
                reftype     = 'sink{:03d}'.format(sink_number),
                pos         = self.pos[sink_number,:],
                abspos      = self.abspos[sink_number,:],
                velocity    = self.velocity[sink_number,:],
                absvel      = self.absvel[sink_number,:],
                age         = self.age[sink_number],
                mass        = self.mass[sink_number],
                logdmdt     = self.logdmdt[sink_number])
  
  @property
  def pos(self):
    if self._pos is None:
      return None
    else:
      if isinstance(self.reference['abspos'],astropy.units.quantity.Quantity):
        return ((self._pos * self.code_length - self.reference['abspos'] + 0.5 * self.code_length) % (1*self.code_length)) - 0.5 * self.code_length
      elif isinstance(self.reference['abspos'],np.ndarray):
        return (((self._pos - self.reference['abspos'] + 0.5) % 1) - 0.5) * self.code_length
      else:
        raise ValueError("reference['abspos'] must be either numpy array or astropy units quantity")

  @property
  def abspos(self):
    if self._pos is None:
      return None
    else:
      return self._pos * self.code_length
  
  @property
  def radius(self):
    return np.sqrt((self.pos**2).sum(axis=1))

  @property
  def absvel(self):
    if self._velocity is None:
      self._velocity = np.vstack((self.quantities['vx'],self.quantities['vy'],self.quantities['vz'])).T
    return self._velocity * self.code_velocity

  @property
  def velocity(self):
    if self._velocity is None:
      self._velocity = np.vstack((self.quantities['vx'],self.quantities['vy'],self.quantities['vz'])).T
    if isinstance(self.reference['absvel'],astropy.units.quantity.Quantity):
      return self._velocity * self.code_velocity - self.reference['absvel']
    elif isinstance(self.reference['absvel'],np.ndarray):
      return (self._velocity - self.reference['absvel']) * self.code_velocity
    else:
      raise ValueError("reference['absvel'] must be either numpy array or astropy units quantity")
  
  @property
  def absvelmagnitude(self):
    return np.sqrt((self.absvel**2).sum(axis=1))

  @property
  def velocitymagnitude(self):
    return np.sqrt((self.velocity**2).sum(axis=1))
  
  @property
  def mass(self):
    if self._mass is None:
      return None
    else:
      return (self._mass * self.jet_factor)*self.code_mass
  
  @property
  def snapshot_time(self):
    if self._snapshot_time is None:
      return None
    else:
      return self._snapshot_time * self.code_time
  
  @property
  def age(self):
    if (self._snapshot_time is None) or (self._tcreate is None):
      return None
    else:
      return (self._snapshot_time - self._tcreate) * self.code_time
  
  @property
  def tcreate(self):
    if self._tcreate is None:
      return None
    else:
      return self._tcreate * self.code_time
  
  @property
  def texplode(self):
    if self._texplode is None:
      return None
    else:
      return self._texplode * self.code_time
  
  @property
  def dm(self):
    if self._dm is None:
      return None
    else:
      return self._dm * self.code_mass
  
  @property
  def tflush(self):
    if self._tflush is None:
      return None
    else:
      return self._tflush * self.code_time
  
  @property
  def logdmdt(self):
    if (self._tflush is None) or (self._dm is None):
      return None
    else:
      return (np.array([np.log10(_dm) if _dm > 0 else -np.inf for _dm in self._dm]) + 
              np.log10(self.jet_factor) - 
              np.log10(self._snapshot_time - self._tflush)) * self.code_logAccretion

  @property
  def dm_manual(self):
    if self._dm_manual is None:
      return None
    else:
      return self._dm_manual * self.code_mass
  
  @dm_manual.setter
  def dm_manual(self,value):
    self._dm_manual = value

  @property
  def accretion_window(self):
    if self._accretion_window is None:
      return None
    else:
      return self._accretion_window * self.code_time
  
  @accretion_window.setter
  def accretion_window(self,value):
    self._accretion_window = value

  @property
  def logdmdt_manual(self):
    if (self._accretion_window is None) or (self._dm_manual is None):
      return None
    else:
      return (np.array([np.log10(_dm) if _dm > 0 else -np.inf for _dm in self._dm_manual]) + 
              np.log10(self.jet_factor) - 
              np.log10(self._accretion_window)) * self.code_logAccretion
  
  @property
  def nsink(self):
    """
    Number of sink particles
    """
    return len(self._mass)


##########################################################################################
######################## Particle Container ##############################################
##########################################################################################
class Particle(Container,object):
  """
  Particle class for loading RAMSES tracer particles
  """
  def __init__(self,*args,**kw):
    
    # ------- Run parent class init
    Container.__init__(self,*args,**kw)
    
    # ------- Set default properties
    self.npart       = None
    self.ntracer     = None
    self.ndim        = None
    self._pos        = None
    self._velocity   = None
    self._mass       = None
    self.id          = None
    self.cellid      = None
    self.level       = None
    self.field_names = None
    self.field_types = None
    self.quantities  = {}
  
  def save(self,filename='particle.hdf5',directory='.',group='/'):
    """
    save method
    """
    
    f = h5py.File(os.path.join(directory,filename), "w")
    try:
      grp = f if group == '/' else f.create_group(group)
      dset = grp.create_dataset('pos',data=self._pos,compression='gzip')
      dset = grp.create_dataset('velocity',data=self._velocity,compression='gzip')
      dset = grp.create_dataset('mass',data=self._mass,compression='gzip')
      dset = grp.create_dataset('id',data=self.id,compression='gzip')
      dset = grp.create_dataset('level',data=self.level,compression='gzip')
      dset = grp.create_dataset('cellid',data=self.cellid,compression='gzip')
      
      grp.attrs['npart']   = self.npart
      grp.attrs['ntracer'] = self.ntracer
      grp.attrs['ndim']    = self.ndim
            
      Container.save(self,grp)
      
      grp2 = grp.create_group('quantities')
      for fn,ft in zip(self.field_names,self.field_types):
        dset = grp2.create_dataset(fn,data=self.quantities[fn],compression='gzip')
        dset.attrs['field_type'] = ft
    finally:
      f.close()
    
  def load(self,filename='particle.hdf5',directory='.',group='/'):
    """
    load method
    """
    
    f = h5py.File(os.path.join(directory,filename), 'r')
    try:
      grp = f if group == '/' else f.require_group(group)
      self._pos      = np.asarray(grp['pos'])
      self._velocity = np.asarray(grp['velocity'])
      self._mass     = np.asarray(grp['mass'])
      self.id        = np.asarray(grp['id'])
      self.level     = np.asarray(grp['level'])
      self.cellid    = np.asarray(grp['cellid'])

      self.npart   = grp.attrs['npart']
      self.ndim    = grp.attrs['ndim']
      self.ntracer = grp.attrs['ntracer']
      
      Container.load(self,grp)
      
      self.field_names, self.field_types = [], []
      
      grp2 = grp.require_group('quantities')
      for fn in grp2.keys():
        self.quantities[fn] = np.asarray(grp2[fn])
        self.field_names.append(fn)
        self.field_types.append(grp2[fn].attrs['field_type'])
    finally:
      f.close()
      
  def rd_particle(self,**kw):
    """
    Method calling rd_particle from particle.py
    """
    from .particle import rd_particle
    
    assert self.nout is not None, "Requires nout to read particle data"
    
    # ------- Handle keywords
    field_names    = kw.get('field_names',['density','vx','vy','vz','pressure','Bx','By','Bz','26Al','60Fe'])
    field_types    = kw.get('field_types',['density','velocity','velocity','velocity','pressure','Bfield','Bfield','Bfield','abundance','abundance'])
    
    # ------- Read particle data    
    particle_dict = rd_particle(self.nout,datadir=self.datadir)
    
    # ------- Make the output quantities ready for the class
    self.npart     = particle_dict['npart']
    self.ntracer   = particle_dict['ntracer']
    self.ndim      = particle_dict['ndim']
    self._pos      = particle_dict['xp']
    self._velocity = particle_dict['vp']
    self._mass     = particle_dict['mp']
    self.id        = particle_dict['id']
    self.level     = particle_dict['level']
    
    self.field_names = []
    self.field_types = []
    for i,field_name in enumerate(field_names):
      try:
        self.quantities[field_name] = particle_dict['up'][:,i]
      except IndexError:
        pass
      else:
        self.field_names.append(field_names[i])
        self.field_types.append(field_types[i])
  
  def __getattr__(self,name):
    
    if name in self.field_names:
      
      field_type = [ft for fn,ft in zip(self.field_names,self.field_types) if fn == name][0]
      
      if field_type == 'density':
        code_unit = self.code_density
      elif field_type == 'velocity':
        code_unit = self.code_velocity
      elif field_type == 'Bfield':
        code_unit = self.code_Bfield
      elif field_type == 'pressure':
        code_unit = self.code_pressure
      elif field_type == 'mass':
        code_unit = self.code_mass
      elif field_type == 'abundance':
        code_unit = 1.
      
      return self.quantities[name] * code_unit
    else:
      raise AttributeError
  
  ############## properties
  @property
  def pos(self):
    """
    position relative to reference position
    """
    if self._pos is None:
      return None
    else:
      if isinstance(self.reference['abspos'],astropy.units.quantity.Quantity):
        return ((self._pos * self.code_length - self.reference['abspos'] + 0.5 * self.code_length) % (1*self.code_length)) - 0.5 * self.code_length
      elif isinstance(self.reference['abspos'],np.ndarray):
        return (((self._pos - self.reference['abspos'] + 0.5) % 1) - 0.5) * self.code_length
      else:
        raise ValueError("reference['abspos'] must be either numpy array or astropy units quantity")

  @property
  def abspos(self):
    """
    absolute positions in box
    """
    if self._pos is None:
      return None
    else:
      return self._pos * self.code_length
  
  @property
  def radius(self):
    """
    distance from reference
    """
    return np.sqrt((self.pos**2).sum(axis=1))
  
  @property
  def limit(self):
    """
    [[xmin, xmax], [ymin, ymax], [zmin, zmax]]
    """
    
    return np.vstack((self.pos.min(axis=0),self.pos.max(axis=0)))
  
  @property
  def mass(self):
    """
    cell masses
    """
    return self._mass * self.code_mass
  
  @property
  def absvel(self):
    """
    absolute cell velocities
    """
    if self._velocity is None:
      self._velocity = np.vstack((self.quantities['vx'],self.quantities['vy'],self.quantities['vz'])).T
    return self._velocity * self.code_velocity

  @property
  def velocity(self):
    """
    cell velocities relative to reference velocity
    """
    if self._velocity is None:
      self._velocity = np.vstack((self.quantities['vx'],self.quantities['vy'],self.quantities['vz'])).T
    if isinstance(self.reference['absvel'],astropy.units.quantity.Quantity):
      return self._velocity * self.code_velocity - self.reference['absvel']
    elif isinstance(self.reference['absvel'],np.ndarray):
      return (self._velocity - self.reference['absvel']) * self.code_velocity
    else:
      raise ValueError("reference['absvel'] must be either numpy array or astropy units quantity")
  
  @property
  def absvelmagnitude(self):
    """
    Absolute velocity magnitude
    """
    return np.sqrt((self.absvel**2).sum(axis=1))

  @property
  def velocitymagnitude(self):
    """
    Velocity magnitudes relative to reference velocity
    """
    return np.sqrt((self.velocity**2).sum(axis=1))

##########################################################################################
######################## Map Container ###################################################
##########################################################################################
class Map(Container,object):
  """
  Map class for loading RAMSES data
  """
  def __init__(self,*args,**kw):
    
    # ------- Run parent class init
    Container.__init__(self,*args,**kw)
    # ------- Set default properties
    self.dir         = None
    self._dr         = None
    self.field_names = None
    self.field_types = None
    self.quantities = {}
  
  def save(self,filename='map.hdf5',directory='.',group='/'):
    """
    save method
    """
    
    f = h5py.File(os.path.join(directory,filename), "w")
    try:
      grp = f if group == '/' else f.create_group(group)

      grp.attrs['dir'] = self.dir
      grp.attrs['dr']  = self._dr   
         
      Container.save(self,grp)
      
      grp2 = grp.create_group('quantities')
      for fn,ft in zip(self.field_names,self.field_types):
        dset = grp2.create_dataset(fn,data=self.quantities[fn],compression='gzip')
        dset.attrs['field_type'] = ft
    finally:
      f.close()
    
  def load(self,filename='map.hdf5',directory='.',group='/'):
    """
    load method
    """
    
    f = h5py.File(os.path.join(directory,filename), 'r')
    try:
      grp = f if group == '/' else f.require_group(group)
      
      self.dir = grp.attrs['dir']
      self._dr = grp.attrs['dr']
      
      Container.load(self,grp)
      
      self.field_names, self.field_types = [], []
      
      grp2 = grp.require_group('quantities')
      for fn in grp2.keys():
        self.quantities[fn] = np.asarray(grp2[fn])
        self.field_names.append(fn)
        self.field_types.append(grp2[fn].attrs['field_type'])
    finally:
      f.close()
      
  def amr2map(self,**kw):
    """
    Method calling amr2map through the make_map function in map.py. Takes the same
    keywords as make_cell except for 'r', 'datadir', and 'lvlmax', which are set
    during the call to __init__
    """
    from .map import make_map
  
    # ------- Handle keywords
    deltar         = kw.get('deltar',[1.0, 1.0, 1.0]) # alias
    dr             = kw.get('dr',deltar)              # whole box
    field_names    = kw.get('field_names',['density'])
    field_types    = kw.get('field_types',['density'])
    
    if isinstance(self.reference['abspos'],astropy.units.quantity.Quantity):
      r = self.reference['abspos'].to(self.code_length).value
    elif isinstance(self.reference['abspos'],np.ndarray):
      r = self.reference['abspos']
    
    # ------- Handle dr
    # if dr is string extract length and convert
    # examples are dr = '11e22 cm', dr = '20000 au', dr = '0.2 parsec'
    if isinstance(dr,str):
      dr, unit = dr.lower().split(' ')
      dr = float(dr) # convert to number
      if not unit in ['cm', 'au', 'parsec']:
        raise ValueError("length unit %s not in list of allowed units (cm, au, parsec)" % unit)
      if unit == 'cm':
        dr = (dr * u.cm).to(self.code_length).value
      elif unit == 'au':
        dr = (dr * u.au).to(self.code_length).value
      elif unit == 'parsec':
        dr = (dr * u.pc).to(self.code_length).value
    
    # if dr is scalar make it a three element list
    if isinstance(dr,float):
      dr = [dr, dr, dr]
  
    # check if any elements in dr is greater than box length
    if any([i > 1 for i in dr]):
      raise IOError("One or more elements in dr greater than box size")
    
    # ------- set keywords for make_map
    kw['r']  = r
    kw['dr'] = dr
    kw['datadir'] = self.datadir
    kw['lvlmax'] = self.lvlmax
    kw['lvlmin'] = self.lvlmin
    
    # ------- Call make_map
    map_array = make_map(self.nout,**kw)
    
    # ------- Make the output quantities ready for the class
    self.dir    = kw['dir'] if 'dir' in kw else 'z'
    self._dr    = kw['dr']
    
    self.field_names = []
    self.field_types = []
    for i,field_name in enumerate(field_names):
      try:
#         self.quantities[field_name] = cell_dict['v'][:,i]
        self.quantities[field_name] = map_array
      except IndexError:
        pass
      else:
        self.field_names.append(field_names[i])
        self.field_types.append(field_types[i])
  
  def __getattr__(self,name):
    
    if name in self.field_names:
      
      field_type = [ft for fn,ft in zip(self.field_names,self.field_types) if fn == name][0]
      
      if field_type == 'density':
        code_unit = self.code_density
      elif field_type == 'velocity':
        code_unit = self.code_velocity
      elif field_type == 'Bfield':
        code_unit = self.code_Bfield
      elif field_type == 'pressure':
        code_unit = self.code_pressure
      elif field_type == 'mass':
        code_unit = self.code_mass
      
      return self.quantities[name] * code_unit
    else:
      raise AttributeError
  
  @property
  def cdFactor(self):
    """
    column density length
    """
    if self.dir == 'x':
      return self._dr[0] * self.code_length
    elif self.dir == 'y':
      return self._dr[1] * self.code_length
    elif self.dir == 'z':
      return self._dr[2] * self.code_length
  
  @property
  def columnDensity(self):
    """
    Gas column density
    """
    
    return self.density * self.cdFactor
  
  @property
  def numdens(self):
    """
    Gas number density. A mean molecular weight of 2.37 is assumed. This can be calculated
    Like so:
    ngas = rho/(mu*mH)
    ni   = Xi*rho/(Ai*mH) where Xi is the mass abundance of element i, and Ai the molecular weight of that element
    so 
    1/mu = X/2 + Y/4 + Z/12 => mu = 2.37 if X = 0.7, Y = 0.28, and Z = 0.02
    """
    
    return (self.density / 2.37 / (1. * u.Da)).cgs
  
  @property
  def H2numdens(self):
    """
    H2 number density. Mass abundance of hydrogren is assumed to be 0.7 and all Hydrogren
    is assumed to be molecular
    """
    
    return (self.density * 0.7 / (1. * u.Da)).cgs
  
  @property
  def dx(self):
    """
    cell sizes
    """
    
    return 1/2**self.lvlmax * self.code_length
  
  @property
  def area(self):
    """
    cell areas
    """
    return self.dx**2
  
  @property
  def volume(self):
    """
    cell volumes
    """
    return self.dx**2*self.cdFactor
  
  @property
  def xlim(self):
    """
    (xmin, xmax)
    """
    n = self.nx
    return np.array([-n*self.dx.value/2,n*self.dx.value/2]) * self.code_length

  @property
  def ylim(self):
    """
    (ymin, ymax)
    """
    n = self.ny
    return np.array([-n*self.dx.value/2,n*self.dx.value/2]) * self.code_length

  @property
  def zlim(self):
    """
    (zmin, zmax)
    """
    if self.dir == 'x':
      return np.array([-self._dr[0]/2, self._dr[0]/2]) * self.code_length
    elif self.dir == 'y':
      return np.array([-self._dr[1]/2, self._dr[1]/2]) * self.code_length
    elif self.dir == 'z':
      return np.array([-self._dr[2]/2, self._dr[2]/2]) * self.code_length

  @property
  def xrange(self):
    """
    Length of region in x direction
    """
    lim = self.xlim
    return lim[1] - lim[0]

  @property
  def yrange(self):
    """
    Length of region in y direction
    """
    lim = self.ylim
    return lim[1] - lim[0]

  @property
  def nx(self):
    """
    Number cells in x direction
    """
    return self.quantities['density'].shape[1]
    #return self.density.shape[1]

  @property
  def ny(self):
    """
    Number of cells in y direction
    """
    return self.quantities['density'].shape[0]
    #return self.density.shape[0]

  @property
  def x_wall(self):
    """
    position of grid walls of base cells in x direction
    """
    n   = self.nx
    lim = self.xlim
    return np.linspace(lim[0].value,lim[1].value,num=n+1) * self.code_length

  @property
  def y_wall(self):
    """
    position of grid walls of base cells in y direction
    """
    n   = self.ny
    lim = self.ylim
    return np.linspace(lim[0].value,lim[1].value,num=n+1) * self.code_length
  
  @property
  def x(self):
    """
    position of cell centres in x direction
    """
    x = self.x_wall
    return (x[1:] + x[:-1])/2

  @property
  def y(self):
    """
    position of cell centres in y direction
    """
    y = self.y_wall
    return (y[1:] + y[:-1])/2
  
  @property
  def mass(self):
    """
    cell masses
    """
    return self.density * self.volume
