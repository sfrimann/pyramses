#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, absolute_import, division

from matplotlib.colors import LogNorm, Normalize
from .utils import make_profile
from math import ceil, floor
from matplotlib.patches import Rectangle, Ellipse
from matplotlib.offsetbox import AnchoredOffsetbox, AuxTransformBox, VPacker, TextArea, DrawingArea
from matplotlib import rc

import numpy as np
from . import constants as c
import matplotlib.pyplot as mp
import matplotlib.cm as cm

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

def plotmap(maps, **kw):
  """
  Function for plotting RAMSES maps.
  In addition all keywords accepted by plotmap_core are also accepted by plotmap
  Calling Sequence:
    plotmap(maps,keywords ...)
  Input:
    maps           :  map dictionary or list of map dictionaries from get_map()
  Keywords:
    extension      :  extension for file ('pdf' or 'png'. Default 'pdf')
    grid           :  sbplot grid (nx, ny)
    width          :  width of plot (Default 15 cm)
    height         :  height of plot
    colorbar       :  None, 'vertical', or 'horizontal'
    filename       :  Default: 'map'
    title          :  plot title. Default: None
    text           :  list of subplot texts
    show           :  show plot instead of saving file. True/False. Default: False
  Returns:
    Nothing
  """

  # ------- Keywords
  dpc       = kw.get('dpc',150.)            # distance in parsec
  extension = kw.get('extension','pdf')     # extension of file (pdf or png)
  unit_l    = kw.get('unit_l','au')         # unit for axes (cm, au, parsec, arcsec. default au)
  grid      = kw.get('grid',None)           # subplot grid (x,y)
  width     = kw.get('width',15.)           # figure width in cm
  height    = kw.get('height',None)         # figure height in cm
  hideaxes  = kw.get('hideaxes',False)
  colorbar  = kw.get('colorbar','vertical') # Plot colorbar (vertical, horizontal)
  filename  = kw.get('filename',None)       # filename
  title     = kw.get('title',None)          # figure title
  text      = kw.get('text', None)          # text for figure
  show      = kw.get('show',False)          # show instead of saving file
  nd        = kw.get('number_density',True) # units cm-3 instead of g cm-3
  cd        = kw.get('column_density',True) # units cm-2 instead of cm-3
  
  # ------- Constants
  fig_aspect     = 0.97
  inch2cm        = c.inch2cm
  cm2au          = c.cm2au
  cm2pc          = c.cm2pc
  molecular_mass = c.molecular_mass # molecular mass of H2 plus "some more"
  amu            = c.unit_mass_in_g # atomic mass unit in g
  
  # ------- Decide size of plotting grid
  if grid is None:
    if isinstance(maps,dict):
      nx, ny = 1, 1
      ntot   = 1
    else:
      ntot   = len(maps)
      assert ntot > 1, "if you only plot one map, don't put it i a list"
      if ntot == 2:
        nx, ny = 2, 1
      elif ntot == 3:
        nx, ny = 3, 1
      else:
        nx, ny = 3, int(ceil(ntot/3.))
  else:
    nx, ny = grid
    ntot   = 1 if isinstance(maps,dict) else len(maps)
    assert nx*ny >= ntot, 'not large enough grid to plot all maps %i < %i' % (nx*ny,ntot)
  
  # ------- Size of axes
  imx    = len(maps['xrange']) if ntot == 1 else len(maps[0]['xrange'])
  imy    = len(maps['yrange']) if ntot == 1 else len(maps[0]['yrange'])
  
  # ------- Are we plotting physical units or numerical units
  if ntot > 1:
    assert all([maps[0]['physical_units'] == map['physical_units'] for map in maps]), "don't mix numerical and physical units"
    physical_units = maps[0]['physical_units']
  else:
    physical_units = maps['physical_units']
  
  # ------- Filename
  filename = 'map' if filename is None else filename
  if len(filename.split('.')) == 1:
    assert extension in ['png','pdf'], "extension %s not in list of allowed extensions, (png, pdf)" % extension
    filename += '.' + extension
  
  # ------- Canvas dimensioning
  img_aspect = imx/imy
  figwidth   = width/inch2cm
  aspect     = (fig_aspect)*(img_aspect)*(ny/nx)
  figheight  = figwidth*aspect if height is None else height/inch2cm
  
  # ------- Axis units
  if physical_units == True:
    assert unit_l in ['cm', 'au', 'parsec', 'arcsec'], 'length unit %s not in list of allowed units. (cm, au, parsec, arcsec)' % unit_l
    if unit_l == 'parsec':
      unit = 'Parsec'
    elif unit_l == 'au':
      unit = 'AU'
    elif unit_l == 'cm':
      unit = 'cm'
    elif unit_l == 'arcsec':
      unit = 'Arcsecond'
  else:
    unit = 'Numerical units'
  
  # ------- keywords for core function
  if (ntot > 1) & ('vmin' not in kw):
    if cd == True:
      vmin = min([map['column_density']*map['density'][np.nonzero(map['density'])].min() for map in maps])
    else:
      vmin = min([map['density'][np.nonzero(map['density'])].min() for map in maps])
    kw['vmin'] = vmin/(molecular_mass*amu) if nd is True and physical_units is True else vmin
  if (ntot > 1) & ('vmax' not in kw):
    if cd == True:
      vmax = max([map['column_density']*map['density'].max() for map in maps])
    else:
      vmax = max([map['density'].max() for map in maps])
    kw['vmax'] = vmax/(molecular_mass*amu) if nd is True and physical_units is True else vmax
  
  # ------- Figure object
  fig = mp.figure(figsize=(figwidth,figheight))
  fig.subplots_adjust(wspace=0,hspace=0)
  
  # ------- Base axis
  ax0 = fig.add_subplot(1,1,1)
  
  # ------- Start plotting
  if (ntot == 1):
    args = (ax0, maps) if text is None else (ax0, maps, text)
    ax0, cb = plotmap_core(*args, **kw)
  else:
    ax0.tick_params(labelcolor='w', top='off', bottom='off', left='off', right='off')
    ax0.set_frame_on(False)
    for i, map in enumerate(maps): # several maps
      ax = fig.add_subplot(ny,nx,i+1)
      args = (ax, map) if text is None else (ax, map, text[i])
      ax, cb = plotmap_core(*args, **kw)
      if not ((i+1) % nx) == 1:
        ax.set_yticklabels([])
      if i < ntot-nx:
        ax.set_xticklabels([])
  ax0.set_xlabel(unit)
  ax0.set_ylabel(unit)
  if title is not None:
    ax0.set_title(title)

  if hideaxes:
    ax0.xaxis.set_visible(False)
    ax0.yaxis.set_visible(False)

  # ------- Colorbar
  if colorbar is not None:
    cbar_ax = fig.add_axes([0.95, 0.15, 0.02, 0.7])
    fig.colorbar(cb, cax=cbar_ax, orientation=colorbar)
    if cd == True:
      unit = r'Column density (cm$^{-2}$)' if nd == True else r'Column density (g cm$^{-2}$)'
    else:
      unit = r'cm$^{-3}$' if nd == True else r'g cm$^{-3}$'
    unit = 'Num. units' if physical_units == False else unit
    cbar_ax.set_ylabel(unit)
  
  if show is True:
    mp.show()
  else:
    mp.savefig(filename,dpi=300,bbox_inches='tight')
  mp.clf()
  return 0
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def plotmap_core(*args, **kw):
  """
  core function for plotting Ramses maps
  Calling Sequence:
    plotmap_core(ax,spec,[text],keywords...)
  Input:
    ax             :  matplotlib axes instance
    spec           :  imge dictionary from readimage, or list of image dictionaries
    text           :  string giving text to display in plot window
  Keywords:
    column_density :  plot column density as opposed to mean density (True/False. Default: True)
    Av             :  plot extinction (True/False. Default: False)
    dpc            :  distance in parsec. only relevant if unit_l == 'arcsec'
    unit_l         :  length unit for axes ('cm', 'au', 'parsec', or 'arcsec'. Default 'au')
    hideaxes       :  hide axes and plot a scale bar instead (True/False. Default False)
    norm           :  Use 'log' or 'linear' scaling 
    fill           :  fill vmin/max under/overflow with vmin/max value
    vmin           :  lower cut-off value
    vmax           :  upper cut-off value
    cmap           :  matplotlib colormap to use. Default jYlGnBu_r
    number_density :  density in units cm-3 instead of g/cm3. True/False. Default True
    fontsize       :  fontsize of text
    plotsymbol     :  symbol to use for stars ('text', 'star', 'dot'. Default 'text')
    colorcode      :  differentiate masses by color. Black = Brown dwarfs, green = low mass
                      stars, red = high mass stars. True/False. Default True
  Returns:
    ax             :  axes instance
    colorbar       :  legend object
  """
  # ------- Arguments
  if len(args) == 2:
    ax = args[0]
    mp = args[1]
    text = None
  elif len(args) == 3:
    ax = args[0]
    mp = args[1]
    text = args[2]
  else:
    raise IOError("plotutils.py. plotmap_core. Wrong number of arguments")

  # ------- Keywords
  cd         = kw.get('column_density',True)
  av         = kw.get('Av',False)
  dpc        = kw.get('dpc',150.)
  unit_l     = kw.get('unit_l','au')
  hideaxes   = kw.get('hideaxes',False)
  norm       = kw.get('norm','log')
  vmin       = kw.get('vmin',None)
  vmax       = kw.get('vmax',None)
  fill       = kw.get('fill',True)
  cmap       = kw.get('cmap',cm.cubehelix)
  nd         = kw.get('number_density',True)
  fontsize   = kw.get('fontsize',12)
  plotsymbol = kw.get('plotsymbol','text')
  #colorcode = kw.get('colorcode',True)
  colorlist  = kw.get('colorlist',None) # list of sink_numbers and colors. If not in list it is not plotted
  plotbd     = kw.get('plotbd',False) # plot brown dwarfs

  # ------- g/cm3, cm-3 or Av
  physical_units = mp['physical_units']
  density = mp['density'].copy()/(c.molecular_mass*c.unit_mass_in_g) if any([nd,av]) and physical_units == True else mp['density'].copy()
  density = density*mp['column_density'] if any([cd,av]) else density
  density = density/1e21 if av else density

  # ------- vmin/vmax
  vmin = np.nanmin(density[np.nonzero(density)]) if vmin == None else vmin
  vmax = np.nanmax(density) if vmax == None else vmax
  
  # ------- Fill
  if fill is True:
    index = np.where(density < vmin)
    density[index] = vmin
    index = np.where(density > vmax)
    density[index] = vmax

  # ------- sink coordinates
  dx, dy = np.median(np.diff(mp['xrange']))/2, np.median(np.diff(mp['yrange']))/2
  extent = np.array([mp['xrange'][0]-dx,mp['xrange'][-1]+dx,mp['yrange'][0]-dy,mp['yrange'][-1]+dy])
  sic = np.asarray(mp['sinks_inside_cutout']) # indices of sinks inside cutout
  sinkx, sinky = mp['sinkx'][sic], mp['sinky'][sic]
  mass = mp['sink_mass'][sic] * c.g2msun #sink masses in msun
  xran, yran = mp['xrange'].copy(), mp['yrange'].copy()
  if unit_l == 'parsec' and physical_units == True:
    extent *= c.cm2pc
    sinkx  *= c.cm2pc
    sinky  *= c.cm2pc
    xran   *= c.cm2pc
    yran   *= c.cm2pc
    unit_l = 'Parsec'
  elif unit_l == 'au' and physical_units == True:
    extent *= c.cm2au
    sinkx  *= c.cm2au
    sinky  *= c.cm2au
    xran   *= c.cm2au
    yran   *= c.cm2au
    unit_l = 'AU'
  elif unit_l == 'arcsec' and physical_units == True:
    extent *= c.cm2au/dpc
    sinkx  *= c.cm2au/dpc
    sinky  *= c.cm2au/dpc
    xran   *= c.cm2au/dpc
    yran   *= c.cm2au/dpc
    unit_l = 'Arcsec'
  
  # ------- norm and colormap
  norm = LogNorm(vmin=vmin,vmax=vmax) if norm == 'log' else Normalize(vmin=vmin,vmax=vmax)
  
  # ------- plot image
  cb = ax.imshow(density,extent=extent,origin='lower',norm=norm,cmap=cmap)
  
  # ------- plot stars
  index_bd = mass > c.brown_dwarf_limit_in_msun
  
  if not plotbd:
    sic, mass, sinkx, sinky = sic[index_bd], mass[index_bd], sinkx[index_bd], sinky[index_bd]
  
  if colorlist != None:
    ssinkx, ssinky, ccolor = [], [], []
    for s,clist in colorlist:
      if s in sic:
        index = s == sic
        ssinkx.append(sinkx[index])
        ssinky.append(sinky[index])
        ccolor.append(clist)
  else:
    ssinkx, ssinky, ccolor = sinkx, sinky, np.tile('white',len(sinkx))
  
  if plotsymbol == 'text':
    for sx,sy,si,cc in zip(ssinkx,ssinky,sic,ccolor):
      ax.text(sx,sy,str(si),color=cc,ha='center',va='center',fontsize='xx-small')
  elif plotsymbol == 'star':
    ax.scatter(ssinkx,ssinky,marker='*',markersize=10,fillstyle='none',color=ccolor)
  elif plotsymbol == 'dot':
    ax.scatter(ssinkx,ssinky,marker='.',color=ccolor,edgecolor='k')
    #if colorcode == True:
      #if plotbd == True:
        #ax.plot(sinkx[index_bd],sinky[index_bd],'k.')
      #ax.plot(sinkx[index_lms],sinky[index_lms],'g.')
      #ax.plot(sinkx[index_hms],sinky[index_hms],'r.')
    #else:
      #if plotbd == True:
        #ax.plot(sinkx,sinky,'.',markerfacecolor='w',markeredgecolor='k')
      #else:
        ##class0 = np.array([  3,  22,  30,  65,  94, 112, 114, 123, 134, 135, 150, 177, 178,
       ##184, 192, 198, 204, 206, 211, 215, 219, 224, 227, 233, 240, 243,
       ##244, 246, 247, 249, 251, 254, 255, 256, 257, 259, 260, 262, 264,
       ##265, 272, 273, 276, 278, 281, 282, 283, 287, 288, 291, 296, 297,
       ##300, 302, 304, 306, 309, 311, 313, 317, 318, 321, 325, 326, 327,
       ##332, 333, 345, 346, 347, 349, 356, 357, 358, 359, 361])
       
        ##class1 = np.array([ 10,  16,  21,  24,  25,  48,  64,  67,  77,  83,  86,  90,  92,
        ##96,  97, 118, 119, 120, 122, 124, 128, 129, 138, 159, 167, 182,
       ##188, 189, 214, 216, 271])
       
        ##class2 = np.array([  0,   1,   8,  11,  15,  17,  18,  26,  27,  32,  33,  34,  35,
        ##36,  37,  38,  39,  46,  47,  49,  51,  53,  56,  57,  58,  59,
        ##60,  68,  74,  75,  78,  84,  88,  89,  91,  93, 100, 101, 102,
       ##116, 117, 125, 126, 127, 132, 133, 139, 142, 145, 147, 148, 152,
       ##153, 154, 157, 158, 164, 166, 174, 176, 179, 183, 185, 186, 187,
       ##191, 193, 196, 200, 207, 208, 209, 210, 212, 217, 235, 239, 248,
       ##252, 253, 258, 263, 267, 270, 277, 285, 286, 289, 293, 298, 305,
       ##324, 337])
        #ax.plot(sinkx[index_lmsphms],sinky[index_lmsphms],'.',markerfacecolor='w',markeredgecolor='k')
        ##ax.plot(sinkx[class2],sinky[class2],'.',markerfacecolor='w',markeredgecolor='k')
        ##ax.plot(sinkx[class0],sinky[class0],'.',markerfacecolor='b',markeredgecolor='k')
        ##ax.plot(sinkx[class1],sinky[class1],'.',markerfacecolor='r',markeredgecolor='k')
  ##ax.scatter(sinkx[index_lmsphms],sinky[index_lmsphms],marker='o',color='white',edgecolor='black',s=5)
  elif plotsymbol == 'none':
    pass
  ax.axis(extent)

  if text: # print text in upper left corner
    ax.text(0.1,0.9,text,fontsize=fontsize,color='white',transform=ax.transAxes,va='top')
  
  # ------- add scale bar
  if hideaxes or unit_l == 'Arcsec':
    add_sizebar(ax,unit_l,color='white',dpc=dpc)
  
  if hideaxes:
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)

  return ax, cb
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def plotprofile_core(*args, **kw):
  # ------- Arguments
  if len(args) == 2:
    ax      = args[0]
    profile = args[1]
    text    = None
  elif len(args) == 3:
    ax      = args[0]
    profile = args[1]
    text    = args[2]
  else:
    raise IOError("plotutils.py. plotprofile_core. Wrong number of arguments")  

  # ------- Keywords
  cmap           = kw.get('cmap',cm.cubehelix)
  textcolor      = kw.get('textcolor','white')
  fontsize       = kw.get('fontsize',12)
  number_density = kw.get('number_density',True)
  unit_l         = kw.get('unit_l','au')
  
  # ------- Convert Units
  histxy = profile['histxy']
  assert unit_l in ['cm', 'au', 'parsec'], 'length unit %s not in list of allowed units. (cm, au, parsec)' % unit_l
  if unit_l == 'parsec':
    x      = profile['x']*c.cm2pc
    histx  = profile['histx']*c.cm2pc
    unit_l = 'Parsec'
  elif unit_l == 'au':
    x      = profile['x']*c.cm2au
    histx  = profile['histx']*c.cm2au
    unit_l = 'AU'
  elif unit_l == 'cm':
    x      = profile['x']
    histx  = profile['histx']
    unit_l = 'cm'
  
  if number_density is True:
    y      = profile['y']/(c.molecular_mass*c.unit_mass_in_g)
    histy  = profile['histy']/(c.molecular_mass*c.unit_mass_in_g)
    lower_error = profile['lower_error']/(c.molecular_mass*c.unit_mass_in_g)
    upper_error = profile['upper_error']/(c.molecular_mass*c.unit_mass_in_g)
    unit_d = 'cm$^{-3}$'
  else:
    y      = profile['y']
    histy  = profile['histy']
    lower_error = profile['lower_error']
    upper_error = profile['upper_error']
    unit_d = 'g cm$^{-3}$'
  
  # ------- Coefficients
  a, b = profile['coefficients']
  b = b/(c.molecular_mass*c.unit_mass_in_g) if number_density is True else b
  b = b*c.cm2au**(-a) if unit_l == 'AU' else b
  b = b*c.cm2pc**(-a) if unit_l == 'Parsec' else b
  
  # ------- Colormap
  cmap = cm.jet if cmap is None else cmap
  
  # ------- Start plotting
  vmax = np.percentile(histxy,99.9)
  extent = (histx[0], histx[-1], histy[0], histy[-1])
  ax.imshow(histxy,extent=extent,origin='lower',cmap=cmap,vmax=vmax,aspect='auto')
  ax.plot(x,y,'w')
  #ax.plot(x,lower_error,'w--')
  #ax.plot(x,upper_error,'w--')
  ax.plot(x,b*x**(a),'r')
  ax.set_xscale('log')
  ax.set_yscale('log')
  ax.set_xlabel(unit_l)
  ax.set_ylabel(unit_d)
  ax.set_xlim((histx.min(),histx.max()))
  ax.set_ylim((histy.min(),histy.max()))

  if text: # print text in upper right corner
    ax.text(0.9,0.9,text,fontsize=fontsize,color=textcolor,transform=ax.transAxes,\
            ha='right')

  return ax
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
def plotmap_profile(mapx,mapy,maz,map_environment,cell,**kw):
  """
  plot maps and profiles on one page
  """
  # ------- Keywords
  nd    = kw.get('number_density',False)
    
  # ------- Constants
  molecular_mass = 2.37
  unit_mass      = 1.660538922e-24 # g
  kyr2s          = 3.15569e10
  msun2g         = 1.9891e33
  
  # ------- vmin/vmax  
  vmin = min(mapx['density'].min(),mapy['density'].min(),mapz['density'].min())
  vmax = max(mapx['density'].max(),mapy['density'].max(),mapz['density'].max())
  vmin = vmin/(molecular_mass*unit_mass) if nd is True else vmin
  vmax = vmax/(molecular_mass*unit_mass) if nd is True else vmax
  
  # ------- Set-Up Axes
  ax1 = mp.subplot2grid((2,3), (0, 0))
  ax2 = mp.subplot2grid((2,3), (0, 1))
  ax3 = mp.subplot2grid((2,3), (0, 2))
  ax4 = mp.subplot2grid((2,3), (1, 0))
  ax5 = mp.subplot2grid((2,3), (1, 1),colspan=2)
  # ------- Maps
  ax1, cb = plotmap_core(ax1,mapx,'x',au=True,vmin=vmin,vmax=vmax,textcolor='white',number_density=nd)
  ax2, cb = plotmap_core(ax2,mapy,'y',au=True,vmin=vmin,vmax=vmax,textcolor='white',number_density=nd)
  ax3, cb = plotmap_core(ax3,mapz,'z',au=True,vmin=vmin,vmax=vmax,textcolor='white',number_density=nd)
  ax1.set_xlabel('AU') ; ax1.set_ylabel('AU')
  ax2.set_xlabel('AU') ; ax2.set_ylabel('AU')
  ax3.set_xlabel('AU') ; ax3.set_ylabel('AU')
  
  # ------- Map of environment
  ax4, cb2 = p.plotmap_core(ax4,map_environment,parsec=True,number_density=nd)
  ax4.set_xlabel('Parsec') ; ax4.set_ylabel('Parsec')
  
  # ------- Density Profile
  profile     = make_profile(cell['r'],cell['density'],cell['dx'])
  sink_age    = cell['sink_age'][sink_number]/kyr2s
  sink_mass   = cell['sink_mass'][sink_number]/msun2g
  sink_number = cell['sink_number']
  nout        = cell['nout']
  slope       = profile['coefficients'][0]
  txt = """sink number, nout: %i, %i
           sink age: %.2f kyr
           sink mass: %.2f Msun
           slope: %1.2f""" % (sink_number, nout, sink_age, sink_mass, slope)
  ax5 = plotprofile_core(ax5,profile,au=True,number_density=nd)
  ax5.text(0.95,0.73,txt,fontsize=10,color='white',transform=ax5.transAxes,ha='right')
  mp.tight_layout()
  
  cbar_ax = mp.axes([1.0, 0.55, 0.02, 0.35])
  mp.colorbar(cb, cax=cbar_ax, orientation='vertical')
  if nd is True:
    cbar_ax.set_ylabel(r'cm$^{-3}$')
  else:
    cbar_ax.set_ylabel(r'g cm$^{-3}$')
  mp.savefig(os.path.join(directory,'nout%isink%i.png' % (nout,sink_number)),bbox_inches='tight',dpi=200)
  mp.clf()
  mp.close()

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
# def plotmap3(mapx,mapy,mapz):
#   """
#   Plot 3d
#   """
#
#   # ------- vmin/vmax
#   vmin = min([mapx['density'].min(),mapy['density'].min(),mapz['density'].min()])
#   vmax = max([mapx['density'].max(),mapy['density'].max(),mapz['density'].max()])
# 
#   # ------- constants
#   cm2au = 6.68458712e-14
# 
#   # ------- norm and colormap
#   norm = LogNorm(vmin=vmin,vmax=vmax)
#   cmap = cm.YlGn_r
#   
#   # ------- Set-up Figure
#   fig = mp.figure()
#   ax = fig.gca(projection='3d')
# 
#   # ------- Mapx
#   #ax.contourf(mapx['xrange']*cm2au,mapx['yrange']*cm2au,mapx['density'],norm=norm,cmap=cmap,zdir='x')
#   ax.plot(mapx['sinkx']*cm2au,mapx['sinky']*cm2au,'r.',zdir='x',zs=0)
#   ax.set_xlabel('x')
#   ax.set_ylabel('y')
#   ax.set_zlabel('z')
# 
# #ax.set_xlim3d(0, 1)
# #ax.set_ylim3d(0, 1)
# #ax.set_zlim3d(0, 1)
#   mp.show()
#   return 0

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#- Matplotlib auxiliaries #-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
class AnchoredSizeBar(AnchoredOffsetbox):
  def __init__(self, transform, size, label, loc,
               pad=0.1, borderpad=0.1, sep=2, prop=None, frameon=True, color='white'):
      """
      Draw a horizontal bar with the size in data coordinate of the give axes.
      A label will be drawn underneath (center-aligned).

      pad, borderpad in fraction of the legend font size (or prop)
      sep in points.
      """
      self.size_bar = AuxTransformBox(transform)
      self.size_bar.add_artist(Rectangle((0,0), size, 0, fc="none",edgecolor=color))

      self.txt_label = TextArea(label, minimumdescent=False,textprops=dict(color=color))

      self._box = VPacker(children=[self.txt_label,self.size_bar],
                          align="center",
                          pad=0, sep=sep)

      AnchoredOffsetbox.__init__(self, loc, pad=pad, borderpad=borderpad,
                                 child=self._box,
                                 prop=prop,
                                 frameon=frameon)

def add_sizebar(ax,unit_l,color='white',dpc=None):
  """
  add sizebar to axes. It calculates the length scale automatically
  """
  
  lunit = 'AU' if unit_l == 'arcsec' else unit_l
  
  # distance between tick marks
  def f(axis):
    l = axis.get_majorticklocs()
    return len(l)>1 and (l[1] - l[0])

  dx    = f(ax.xaxis)*dpc if unit_l == 'arcsec' else f(ax.xaxis) # distance between tick marks in AU
  dx    = round(dx,-int(floor(np.log10(dx)))) # round to nearest "nice number" eg. 10, 20, 200, 3000
  dx    = int(dx) if dx >= 1 else dx
  if type(dx) is int:
    label = "%i %s" % (dx,lunit) # create nice label string
  else:
    label = "%.1f %s" % (dx,lunit) # create nice label string
  dx      = dx/dpc if unit_l == 'arcsec' else dx

  asb = AnchoredSizeBar(ax.transData, dx, label, loc=4, pad=0.2, borderpad=0.2, sep=5,
                        frameon=False,color=color)
  ax.add_artist(asb)
