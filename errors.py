#!/usr/bin/env python
# -*- coding: utf-8 -*-

def check_sentinels(sentinel1,sentinel2):
  """
  check if sentinels are equal, if not raise exception
  """
  if sentinel1 != sentinel2:
    raise SentinelError("Fortran sentinels do not match %i != %i" % (sentinel1,sentinel2) )
  
  return 

# define SentinelError class
class SentinelError(IOError):
  """
  An IOError subclass for cases where the sentinel bytes in Fortran unformatted files are incorrect.
  """
  pass

class PrecisionError(IOError):
  """
  Precision of element not as expected.
  """
  pass