#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, absolute_import, division

#from .errors import check_sentinels
import os
import numpy as np

def check_sentinels(sentinel1,sentinel2):
  """
  check if sentinels are equal, if not raise exception
  """
  if sentinel1 != sentinel2:
    raise SentinelError("Fortran sentinels do not match %i != %i" % (sentinel1,sentinel2) )
  
  return 

# define SentinelError class
class SentinelError(IOError):
  """
  An IOError subclass for cases where the sentinel bytes in Fortran unformatted files are incorrect.
  """
  pass

def _fromfile(f,dtype,count=1,big_endian=False):
  """
  helper function. Makes it possible to read big_endian files by setting keyword
  """
  if big_endian is True:
    return np.fromfile(f,dtype=dtype,count=count).byteswap()
  else:
    return np.fromfile(f,dtype=dtype,count=count)

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def _read_sink(f,stars):
  """
  Read one data record
  """
  
  #-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
  if stars == 0:
    raise NotImplementedError("stars == 0 is not yet implemented")
  #-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
  elif stars == 1:
    
    #-#-#-#-#-#-# Read headers #-#-#-#-#-#-#
    #-#-#-# Read first header #-#-#-#
    
    # read first element (to check for number of bytes in record and little or big endian)
    sentinel1 = _fromfile(f,'u4',count=1)[0]
    
    # check if the data is small endian or big endian
    big_endian = True if sentinel1 > 32768 else False
    
    if big_endian is True: # byteswap if big endian
      sentinel1 = sentinel1.byteswap()
    
    if sentinel1 == 20: # decide how to read the remainder of record depening on size
      dtype = 'i4, i4, f4, i4, i4, u4'
    else:
      dtype = 'i4, i4, f4, i4, i8, u4'
    
    version, msink, t, sz, lsink, sentinel2 = _fromfile(f,dtype,count=1,big_endian=big_endian)[0]
    
    lsink /= sz*4
    void = check_sentinels(sentinel1,sentinel2)
    
    #-#-#-# Read second header #-#-#-#
    if version in [4, 5, 6]:
      sentinel1, nmetal, sentinel2 = _fromfile(f,'u4, i4, u4',count=1,big_endian=big_endian)[0]
      dp = 4
    elif version in [7, 8]:
      sentinel1, dp, nmetal, tflush, sentinel2 = \
      _fromfile(f,'u4, i4, i4, f8, u4',count=1,big_endian=big_endian)[0]
    elif version == 9:
      sentinel1, dp, nmetal, tflush, t8, sentinel2 = \
      _fromfile(f,'u4, i4, i4, f8, f8, u4',count=1,big_endian=big_endian)[0]
      t = t8
    elif version >= 10:
      raise NotImplementedError("Versions greater than 9 not supported. %i" % version) 

    void = check_sentinels(sentinel1,sentinel2)
    
    #-#-#-# Read third header #-#-#-#
    if version >= 8 and dp == 8:
      sentinel1, do_translate, center_star, r_translate, v_translate, void, sentinel2 = \
        _fromfile(f,'u4, i4, i4, (3,)f8, (3,)f8, (3,)f8, u4',count=1,big_endian=big_endian)[0]
    elif version in [5, 6, 7]:
      sentinel1, do_translate, center_star, r_translate, v_translate, void, sentinel2 = \
        _fromfile(f,'u4, i4, i4, (3,)f4, (3,)f4, (3,)f4, u4',count=1,big_endian=big_endian)[0]
    
    void = check_sentinels(sentinel1,sentinel2)
    
    #-#-#-# Create data-type for basic records #-#-#-#
    if version == 1:
      raise NotImplementedError("Version %i not yet implemented" % version)
    
    elif version == 2:
      raise NotImplementedError("Version %i not yet implemented" % version)
    
    elif version == 3:
      raise NotImplementedError("Version %i not yet implemented" % version)
    
    elif version in [4, 5, 6] and nmetal == 0:
      dt = np.dtype([('sentinel1','u4'),('x','f4'),('y','f4'),('z','f4'),\
                     ('px','f4'),('py','f4'),('pz','f4'),\
                     ('dpx','f4'),('dpy','f4'),('dpz','f4'),\
                     ('m','f4'),('dm','f4'),('phi','f4'),('rho','f4'),\
                     ('tcreate','f4'),('texplode','f4'),\
                     ('gid','i4'),('father','i4'),\
                     ('ilevel','i4'),('old_ilevel','i4'),\
                     ('has_moved','i4'),('beta_create','f4'),\
                     ('owner','i4'),('sentinel2','u4')])
      record_size = 96 #bytes 24*4
    elif version in [4, 5, 6] and nmetal != 0:
      dt = np.dtype([('sentinel1','u4'),('x','f4'),('y','f4'),('z','f4'),\
                     ('px','f4'),('py','f4'),('pz','f4'),\
                     ('dpx','f4'),('dpy','f4'),('dpz','f4'),\
                     ('m','f4'),('dm','f4'),('phi','f4'),('rho','f4'),\
                     ('tcreate','f4'),('texplode','f4'),\
                     ('metal','(%i,)f4' % nmetal), ('dmetal','(%i,)f4' % nmetal),\
                     ('gid','i4'),('father','i4'),\
                     ('ilevel','i4'),('old_ilevel','i4'),\
                     ('has_moved','i4'),('beta_create','f4'),\
                     ('owner','i4'),('sentinel2','u4')])
      record_size = 96 + nmetal*8 #bytes 24*4 + 2*nmetal*4
    elif version in [7, 8, 9] and nmetal == 0 and dp == 4:
      dt = np.dtype([('sentinel1','u4'),('x','f8'),('y','f8'),('z','f8'),\
                     ('px','f4'),('py','f4'),('pz','f4'),\
                     ('dpx','f4'),('dpy','f4'),('dpz','f4'),\
                     ('m','f4'),('dm','f4'),('phi','f4'),('rho','f4'),\
                     ('tcreate','f4'),('texplode','f4'),\
                     ('gid','i4'),('father','i4'),
                     ('ilevel','i4'),('old_ilevel','i4'),\
                     ('owner','i4'),('has_moved','i4'),('beta_create','f4'),\
                     ('dtold','f4'),('sentinel2','u4')])
      record_size = 112 #bytes 3*8 + 22*4
    elif version in [7, 8, 9] and nmetal == 0 and dp == 8:
      dt = np.dtype([('sentinel1','u4'),('x','f8'),('y','f8'),('z','f8'),\
                     ('px','f8'),('py','f8'),('pz','f8'),\
                     ('dpx','f8'),('dpy','f8'),('dpz','f8'),\
                     ('m','f8'),('dm','f8'),('phi','f8'),('rho','f8'),\
                     ('tcreate','f8'),('texplode','f8'),\
                     ('gid','i4'),('father','i4'),\
                     ('ilevel','i4'),('old_ilevel','i4'),\
                     ('owner','i4'),('has_moved','i4'),('beta_create','f8'),\
                     ('dtold','f8'),('sentinel2','u4')])
      record_size = 168 #bytes 17*8 + 8*4
    elif version in [7, 8, 9] and nmetal != 0 and dp == 4:
      dt = np.dtype([('sentinel1','u4'),('x','f8'),('y','f8'),('z','f8'),\
                     ('px','f4'),('py','f4'),('pz','f4'),\
                     ('dpx','f4'),('dpy','f4'),('dpz','f4'),\
                     ('m','f4'),('dm','f4'),('phi','f4'),('rho','f4'),\
                     ('tcreate','f4'),('texplode','f4'),\
                     ('metal','(%i,)f4' % nmetal), ('dmetal','(%i,)f4' % nmetal),\
                     ('gid','i4'),('father','i4'),
                     ('ilevel','i4'),('old_ilevel','i4'),\
                     ('owner','i4'),('has_moved','i4'),('beta_create','f4'),\
                     ('dtold','f4'),('sentinel2','u4')])
      record_size = 112 + 8*nmetal #bytes 3*8 + 22*4 + 2*nmetal*4
    elif version in [7, 8, 9] and nmetal != 0 and dp == 8:
      dt = np.dtype([('sentinel1','u4'),('x','f8'),('y','f8'),('z','f8'),\
                     ('px','f8'),('py','f8'),('pz','f8'),\
                     ('dpx','f8'),('dpy','f8'),('dpz','f8'),\
                     ('m','f8'),('dm','f8'),('phi','f8'),('rho','f8'),\
                     ('tcreate','f8'),('texplode','f8'),\
                     ('metal','(%i,)f8' % nmetal), ('dmetal','(%i,)f8' % nmetal),\
                     ('gid','i4'),('father','i4'),\
                     ('ilevel','i4'),('old_ilevel','i4'),\
                     ('owner','i4'),('has_moved','i4'),('beta_create','f8'),\
                     ('dtold','f8'),('sentinel2','u4')])
      record_size = 168 + 16*nmetal #bytes 17*8 + 8*4 + 2*nmetal*8
    
    # read data
    s = _fromfile(f,dt,count=msink,big_endian=big_endian)
    
    # negative mass indicates that the star has exploded
    # ------ removed 22 April 2016. I do not know why I ever added this, maybe I was on drugs?
#     if any(s['m'] < 0):
#       index, = np.where(s['m'] < 0)
#       s['x'][index], s['y'][index], s['z'][index] = -0.1, -0.1, -0.1 # place positions outside of box
    
    # check sentinels
    if not np.array_equal(s['sentinel1'],s['sentinel2']):
      raise SentinelError("Fortran sentinels do not match")
    # work with data
    ux = s['px']/s['m']
    uy = s['py']/s['m']
    uz = s['pz']/s['m']
    uu = np.sqrt(ux**2+uy**2+uz**2)

    # write dictionary
    if version >= 7:
      ss = {'x':s['x'],'y':s['y'],'z':s['z'],'ux':ux,'uy':uy,'uz':uz,'px':s['px'],\
            'py':s['py'],'pz':s['pz'],'m':s['m'],'dm':s['dm'],'il':s['ilevel'],'u':uu,\
            'rho':s['rho'],'tcreate':s['tcreate'],'texplode':s['texplode'],'r':0.,'mu':0.,\
            'snapshot_time':t,'version':version,'dp':dp,'tflush':tflush,'dtold':s['dtold']}
    else:
      ss = {'x':s['x'],'y':s['y'],'z':s['z'],'ux':ux,'uy':uy,'uz':uz,'px':s['px'],\
            'py':s['py'],'pz':s['pz'],'m':s['m'],'dm':s['dm'],'il':s['ilevel'],'u':uu,\
            'rho':s['rho'],'tcreate':s['tcreate'],'texplode':s['texplode'],'r':0.,'mu':0.,\
            'snapshot_time':t,'version':version,'dp':dp}
  
    return ss, msink, t, lsink
  
  elif stars == 2:
    raise NotImplementedError("stars == 2 is not yet implemented")
  else:
    raise ValueError("stars variable must be equal to 0, 1, or 2. It's: %i" % stars)

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def rsink(*args, **kw):
  """
  Read sink data given snapshot number and data directory.
  This version of the code assumes that files are named stars_output.dat, and that the
  version is 4 or 6, and that nmetal = 0
  Soeren Frimann May 2013. Original code in IDL by Troels Haugboelle.
  September 2013. Major update of _read_sink to make easier to read, and include more versions
  February 2017. Removed restructure keyword. Moved to a different function instead
  """

  # ------- Handle keywords
  datadir     = kw.get('datadir','.')
  all         = kw.get('all',False)
  monotonic   = kw.get('monotonic', True)
  forward     = kw.get('forward', False) # step forward in stars.dat as opposed to backward
  fname       = kw.get('filename',None) # define filename
  
  # ------- Check arguments
  if len(args) == 0:
    nout = None
  elif len(args) == 1:
    nout = args[0]
  else:
    raise IOError("Wrong number of arguments")
  
  # ------- Handle filenames
  if fname is not None:
    fname = os.path.join(datadir,fname)
    stars = 1
  elif os.path.isfile(os.path.join(datadir,'stars.dat')):
    fname = os.path.join(datadir,'stars.dat')
    stars = 1
  elif os.path.isfile(os.path.join(datadir,'sink.dat')):
    fname = os.path.join(datadir,'sink.dat')
    stars = 0
  elif os.path.isfile(os.path.join(datadir,'hms.dat')):
    fname = os.path.join(datadir,'hms.dat')
    stars = 2
  if nout is not None:
    fname = os.path.join(datadir,'output_'+str(nout).zfill(5),'stars_output.dat')
    stars = 1
  if fname is None:
    raise NameError("fname not defined. Are you in the right directory?")
  
  # ------- Open file
  f = open(fname,mode='rb')
  
  try:
    # ------- File positions
    f.seek(0,2) # go to end of file to determine file size
    eof = f.tell() # file size in bytes
    f.seek(0,0) # go back to beginning
  
    if all is False:
      s, msink, t, lsink = _read_sink(f,stars)
    else:
      s = []
      t = []
      while f.tell() != eof:
        ss, msink, tt, lsink = _read_sink(f,stars)
        s.append(ss)
        t.append(tt)
      if monotonic is True:
        if forward is True:
          tmax = t[0]
          j = 0
          for i in range(1,len(t)):
            if t[i] > tmax:
              tmax = t[i]
            else:
              s.pop(i-j)
              j += 1
        else:
          tmin = t[-1]
          for i in range(len(t)-2,-1,-1):
            if t[i] < tmin:
              tmin = t[i]
            else:
              s.pop(i)
  finally:
    # ------- Close file
    f.close()
  
  return s

def restructure(sink):
  """
  Given a list of sink data from the stars.dat file reorganize it to follow individual sinks
  """
  
  # ------- Order sinks after sink_number, not snapshot
  nsink = [len(ss['x']) for ss in sink]
  sinks = []
  tflush_on = True if 'tflush' in sink[0] else False
  for sink_number in range(nsink[-1]):
    print(sink_number,nsink[-1])
    # ------- Start Working
    index = [i for i in range(len(sink)) if nsink[i] > sink_number]
    dic   = {}
    
    dic['sink_number'] = sink_number
    dic['il'] = np.asarray([sink[i]['il'][sink_number] for i in index])
    
    dic['x'] = np.asarray([sink[i]['x'][sink_number] for i in index])
    dic['y'] = np.asarray([sink[i]['y'][sink_number] for i in index])
    dic['z'] = np.asarray([sink[i]['z'][sink_number] for i in index])
    
    dic['tcreate']  = min([sink[i]['tcreate'][sink_number] for i in index])
    dic['texplode'] = max([sink[i]['texplode'][sink_number] for i in index])
    dic['snapshot_time'] = np.asarray([sink[i]['snapshot_time'] for i in index])
    if tflush_on:
      dic['tflush'] = np.asarray([sink[i]['tflush'] for i in index])
    
    dic['m']  = np.asarray([sink[i]['m'][sink_number] for i in index])
    dic['dm'] = np.asarray([sink[i]['dm'][sink_number] for i in index])
    
    dic['u']  = np.asarray([sink[i]['u'][sink_number] for i in index])
    dic['ux'] = np.asarray([sink[i]['ux'][sink_number] for i in index])
    dic['uy'] = np.asarray([sink[i]['uy'][sink_number] for i in index])
    dic['uz'] = np.asarray([sink[i]['uz'][sink_number] for i in index])
    
    dic['r']  = max([sink[i]['r'] for i in index])
    dic['mu'] = max([sink[i]['r'] for i in index])
    
    dic['px'] = np.asarray([sink[i]['px'][sink_number] for i in index])
    dic['py'] = np.asarray([sink[i]['py'][sink_number] for i in index])
    dic['pz'] = np.asarray([sink[i]['pz'][sink_number] for i in index])
    
    dic['rho'] = np.asarray([sink[i]['rho'][sink_number] for i in index])
    
    sinks.append(dic)
  
  return sinks
