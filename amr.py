#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, absolute_import, division

import numpy as np

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def _allocate_more(nmore):
  """
  Helper Function.
  Allocates more Array space for AMR tree
  """
  # ------- Set Global Variables
  global namrtree, amrtree
  
  namrtree += nmore
  amrtree   = np.append(amrtree,np.empty(nmore,dtype=np.int8))
  return 0
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#
def amrsort(r,dx,strip_edge=True,particle=None,timing=False,full_oct=False):
  """
  Sorts Ramses AMR data into format that can be read by Radmc3d.
  Note that the outermost rim of the AMR cube is removed since it may not be filled
  Input:
    r          : data coordinates (ndata,3) numpy array
    dx         : cell sizes (ndata) numpy array
  Keywords:
    strip_edge : Strip away the edges of the grid. This is needed if the edges are not
                 completely filled octs.
    particle   : coordinates for potential particle field. (nparticle,3) numpy array. If
                 set the particles are sorted simultaneously with the amr grid so that one
                 knows exactly which amr cell each particle belongs to. The result is
                 saved to the array 'cellid', which, for each particle, labels the id of
                 the cell it resides in. The cell IDs correspond to the order of the sorted
                 AMR grid.
    timing     : Record the time it takes for the routine to run and print the result.
                 (boolean, default: False).
  Returns:
    indices    : sorted indices for data
    amrtree    : amrtree of ones and zeros which is needed for radmc input files. Zeros
                 indicates the leaf cells, while ones indicate branches for going down one
                 level of refinement. See RADMC-3D manual for details.
    cellid     : (nparticle,) shaped array giving the id of the cells that each particle
                 reside in. If the particle lies outside the grid the ID is set to -1.
  """
  # ------- Set Global Variables
  global indices, pos1, pos2, namrtree, amrtree, cellid
  
  # ------- Record time
  if timing:
    import time
    current_time = time.time()
  
  # ------- min/max in all directions
  minx, miny, minz = np.min(r-dx[:,np.newaxis]/2,axis=0)
  maxx, maxy, maxz = np.max(r+dx[:,np.newaxis]/2,axis=0)

  # ------- grid size and grid edge of a base grid cell
  gs = np.max(dx) # grid size of base grid
  ge = r[np.argmax(dx),:] + gs/2 # grid edges of one of the base grid cells
  
  # ------- Re-calculate min/max in all directions if necessary
  nbxl, nbxr = (ge[0]-minx)/gs, (maxx-ge[0])/gs #nbinx_left, nbinx_right
  nbyl, nbyr = (ge[1]-miny)/gs, (maxy-ge[1])/gs #nbiny_left, nbiny_right
  nbzl, nbzr = (ge[2]-minz)/gs, (maxz-ge[2])/gs #nbinz_left, nbinz_right
  
  minx = ge[0] - gs*np.ceil(nbxl) if abs(nbxl - round(nbxl)) > 1e-10 else minx
  maxx = ge[0] + gs*np.ceil(nbxr) if abs(nbxr - round(nbxr)) > 1e-10 else maxx
  miny = ge[1] - gs*np.ceil(nbyl) if abs(nbyl - round(nbyl)) > 1e-10 else miny
  maxy = ge[1] + gs*np.ceil(nbyr) if abs(nbyr - round(nbyr)) > 1e-10 else maxy
  minz = ge[2] - gs*np.ceil(nbzl) if abs(nbzl - round(nbzl)) > 1e-10 else minz
  maxz = ge[2] + gs*np.ceil(nbzr) if abs(nbzr - round(nbzr)) > 1e-10 else maxz
  
  # ------- number of bins in xyz
  nbinx     = (maxx-minx)/gs # number of bins
  nbiny     = (maxy-miny)/gs
  nbinz     = (maxz-minz)/gs
  
  # ------- Check if nbin(xyz) are whole numbers
  if abs(nbinx - round(nbinx)) > 1e-10 or \
     abs(nbiny - round(nbiny)) > 1e-10 or \
     abs(nbinz - round(nbinz)) > 1e-10:
    print("nbinx, nbiny, nbinz", nbinx, nbiny, nbinz)
    raise ValueError("nbin(xyz) must be whole numbers")
  else:
    nbinx = int(round(nbinx))
    nbiny = int(round(nbiny))
    nbinz = int(round(nbinz))
  
  # ------- if any nbin less than 3 the area is not big enough and return empty indices
  if any([np.abs(nbin - 3) < 1e-10 for nbin in [nbinx,nbiny,nbinz]]):
    if strip_edge:
      return [], []

  # ------- Bin edges
  binx = np.linspace(minx,maxx,num=int(round(nbinx))+1)
  biny = np.linspace(miny,maxy,num=int(round(nbiny))+1)
  binz = np.linspace(minz,maxz,num=int(round(nbinz))+1)
  
  if strip_edge: # strip away outer bins since these may not be completely filled
    binx = binx[1:-1]
    biny = biny[1:-1]
    binz = binz[1:-1]
    # ----- New grid min/max
    minx, maxx = binx[0], binx[-1]
    miny, maxy = biny[0], biny[-1]
    minz, maxz = binz[0], binz[-1]
  
  if full_oct:
    power = 0
    while 2**power <= (min(binx.size,biny.size,binz.size)-1):
      power += 1
    nbinx, nbiny, nbinz = 2**(power-1), 2**(power-1), 2**(power-1)
    if binx.size > (nbinx+1):
      shorten = binx.size - nbinx - 1
      # Not the most efficient way of doing this, but the easiest
      for i in range(shorten): # CAREFUL: This assumes the coordinate grid is centered around zero!
        if abs(binx[0]) > abs(binx[-1]):
          binx = binx[1:]
        else:
          binx = binx[:-1]
    if biny.size > (nbiny+1):
      shorten = biny.size - nbiny - 1
      # Not the most efficient way of doing this, but the easiest
      for i in range(shorten): # CAREFUL: This assumes the coordinate grid is centered around zero!
        if abs(biny[0]) > abs(biny[-1]):
          biny = biny[1:]
        else:
          biny = biny[:-1]
    if binz.size > (nbinz+1):
      shorten = binz.size - nbinz - 1
      # Not the most efficient way of doing this, but the easiest
      for i in range(shorten): # CAREFUL: This assumes the coordinate grid is centered around zero!
        if abs(binz[0]) > abs(binz[-1]):
          binz = binz[1:]
        else:
          binz = binz[:-1]
    binx = binx[[0,-1]]
    biny = biny[[0,-1]]
    binz = binz[[0,-1]]
  
  # ------- Indices that fall into the bins
  ix = np.searchsorted(binx,r[:,0],side='right')
  iy = np.searchsorted(biny,r[:,1],side='right')
  iz = np.searchsorted(binz,r[:,2],side='right')
  
  if full_oct:
  # ------- Cells inside grid limit
    inside_grid = np.vstack((ix > 0,ix < len(binx),\
                             iy > 0,iy < len(biny),\
                             iz > 0,iz < len(binz))).all(axis=0)
    # ------- Number of data points
    ndata = inside_grid.sum()
  elif strip_edge:
    # ------- Cells inside grid limit
    inside_grid = np.vstack((ix > 0,ix < len(binx),\
                             iy > 0,iy < len(biny),\
                             iz > 0,iz < len(binz))).all(axis=0)
    if gs != np.max(dx[inside_grid]): # one of the edge grid cells were larger than remaining cells
#       ratio = round(gs/np.max(dx[inside_grid])) # ratio between new and root grid size
      gs    = np.max(dx[inside_grid]) # new root grid size
    
      # ------- number of bins in xyz
      nbinx     = (maxx-minx)/gs # number of bins
      nbiny     = (maxy-miny)/gs
      nbinz     = (maxz-minz)/gs
  
      # ------- Check if nbin(xyz) are whole numbers
      if abs(nbinx - round(nbinx)) > 1e-10 or \
         abs(nbiny - round(nbiny)) > 1e-10 or \
         abs(nbinz - round(nbinz)) > 1e-10:
        print("nbinx, nbiny, nbinz", nbinx, nbiny, nbinz)
        raise ValueError("nbin(xyz) must be whole numbers")
    
      # ------- Bin edges (strip away outer bins since these may not be completely filled)
      binx = np.linspace(minx,maxx,num=int(round(nbinx))+1)
      biny = np.linspace(miny,maxy,num=int(round(nbiny))+1)
      binz = np.linspace(minz,maxz,num=int(round(nbinz))+1)

      # ------- Indices that fall into the bins
      ix = np.searchsorted(binx,r[:,0],side='right')
      iy = np.searchsorted(biny,r[:,1],side='right')
      iz = np.searchsorted(binz,r[:,2],side='right')
    
    # ------- Number of data points
    ndata = inside_grid.sum()
  else:
    ndata = len(dx)
  
  # ------- particle indices
  if particle is not None:
    ipx = np.searchsorted(binx,particle[:,0],side='right')
    ipy = np.searchsorted(biny,particle[:,1],side='right')
    ipz = np.searchsorted(binz,particle[:,2],side='right')
  
  # ------- Initialise arrays
  indices  = np.empty(ndata,dtype=np.int32)
  indarr   = np.arange(len(dx))
  pos1     = 0
  pos2     = 0
  namrtree = round(10000)
  amrtree  = np.empty(namrtree,dtype=np.int8)
  
  if particle is not None:
    pindarr = np.arange(particle.shape[0],dtype=np.int32)
    cellid  = -1*np.ones(pindarr.size,dtype=np.int32)
  
  # ------- Base loop
  zz,yy,xx = np.mgrid[1:len(binz),1:len(biny),1:len(binx)]
  for iix,iiy,iiz in zip(xx.ravel(),yy.ravel(),zz.ravel()):
    index = (ix == iix) & (iy == iiy) & (iz == iiz)
    if particle is not None:
      pindex = (ipx == iix) & (ipy == iiy) & (ipz == iiz)
    count = np.count_nonzero(index)
    if count == 1:
      indices[pos1] = indarr[index]
      amrtree[pos2] = 0
      if particle is not None:
        if np.count_nonzero(pindex) > 0:
          cellid[pindarr[pindex]] = pos1
      pos1 += 1; pos2 += 1
      if pos2 == namrtree: _allocate_more(10000)
    elif count > 7:
      amrtree[pos2] = 1
      pos2 += 1
      if pos2 == namrtree: _allocate_more(10000)
      if particle is not None:
        void = _amrsort(r[index,:],dx[index],indarr[index],particle[pindex,:],pindarr[pindex])
      else:
        void = _amrsort(r[index,:],dx[index],indarr[index])
    else:
      raise ValueError("Something happened in the AMR levels. Non-Recursive %i" % count)
  
  amrtree = amrtree[0:pos2] # remove unused space in amrtree
  
  # ------- Assert that we've been through all data
  if pos1 != ndata:
    raise ValueError("pos1 and ndata are not equal. Have we been through all data?")
  
  # ------- print timing
  if timing:
    m, s = divmod(time.time()-current_time,60)
    h, m = divmod(m, 60)
    print('amrsort finished in {:.0f}h{:02.0f}m{:02.0f}s'.format(h,m,s))
  
  # ------- Return
  if particle is not None:
    return indices, amrtree, cellid
  else:
    return indices, amrtree

def _amrsort(*args):
  """
  Helper function for amrsort. Deals with recursive calls
  """
  # ------- Set Global Variables
  global indices, pos1, pos2, namrtree, amrtree, cellid
  
  # ------- Handle input
  if len(args) == 3:
    r, dx, indarr = args
    part = False
  else:
    r, dx, indarr, particle, pindarr = args
    part = True
  
  # ------- min/max in all directions
  minx, miny, minz = np.min(r-dx[:,np.newaxis]/2,axis=0)
  maxx, maxy, maxz = np.max(r+dx[:,np.newaxis]/2,axis=0)
#   amin = np.argmin(r,axis=0)
#   amax = np.argmax(r,axis=0)
#   
#   minx, maxx = r[amin[0],0]-dx[amin[0]]/2, r[amax[0],0]+dx[amax[0]]/2
#   miny, maxy = r[amin[1],1]-dx[amin[1]]/2, r[amax[1],1]+dx[amax[1]]/2
#   minz, maxz = r[amin[2],2]-dx[amin[2]]/2, r[amax[2],2]+dx[amax[2]]/2

  grid_size = (maxx-minx)/2
  # ------- Bin edges
  binx = np.linspace(minx,maxx,num=3)
  biny = np.linspace(miny,maxy,num=3)
  binz = np.linspace(minz,maxz,num=3)
    
  # ------- Indices that fall into the bins
  ix = np.searchsorted(binx,r[:,0],side='right')
  iy = np.searchsorted(biny,r[:,1],side='right')
  iz = np.searchsorted(binz,r[:,2],side='right')
  
  # ------- Particle indices that fall into the bins
  if part:
    ipx = np.searchsorted(binx,particle[:,0],side='right')
    ipy = np.searchsorted(biny,particle[:,1],side='right')
    ipz = np.searchsorted(binz,particle[:,2],side='right')

  # ------- Looping
  zz = np.array([1, 1, 1, 1, 2, 2, 2, 2])
  yy = np.array([1, 1, 2, 2, 1, 1, 2, 2])
  xx = np.array([1, 2, 1, 2, 1, 2, 1, 2])
  nndata = len(dx)
  for iix,iiy,iiz in zip(xx,yy,zz):
    index = (ix == iix) & (iy == iiy) & (iz == iiz)
    if part:
      pindex = (ipx == iix) & (ipy == iiy) & (ipz == iiz)
    count = np.count_nonzero(index)
    if count == 1:
      indices[pos1] = indarr[index]
      amrtree[pos2] = 0
      if part:
        if np.count_nonzero(pindex) > 0:
          cellid[pindarr[pindex]] = pos1
      pos1 += 1; pos2 += 1
      if pos2 == namrtree: _allocate_more(10000)
    elif count > 7:
      amrtree[pos2] = 1
      pos2 += 1
      if pos2 == namrtree: _allocate_more(10000)
      if part:
        void = _amrsort(r[index,:],dx[index],indarr[index],particle[pindex,:],pindarr[pindex])
      else:
        void = _amrsort(r[index,:],dx[index],indarr[index])
    else:
      raise ValueError("Something happened in the AMR levels. Recursive %i" % count)

  return 0