#!/usr/bin/env python

from __future__ import print_function, absolute_import, division

import numpy as np
import os

import astropy.constants as cnst
import astropy.units as unit

from hyperion.dust import SphericalDust
from hyperion.model import Model

def writeHyperionModel(*args,**kw):
  """
  Write Hyperion model from pyramses data
  """
  
  # ------- Handle keywords
  
  # Bookkeeping
  modeldir  = kw.get('modeldir','.')
  modelname = kw.get('modelname','RAMSES')
  overwrite = kw.get('overwrite',False)
  
  # Dust
  dustname  = kw.get('dustname','')
  Tsub      = kw.get('Tsub',0.) # Dust sublimation temperature (0. means off)
  mode      = kw.get('mode','fast')
  
  # Density
  densityFactor = kw.get('densityFactor',1.) # factor to multiply density with (e.g. dust2gas ratio. Depends on what is assumed by the opacity)
  
  # Source
  luminosity = kw.get('luminosity',[1.])
  Teff       = kw.get('Teff',[2500.])
  
  # RT settings
  mrw_gamma        = kw.get('mrw_gamma',4.) # Modified random walk gamma factor. If < 2 MRW is not used
  use_pda          = kw.get('use_pda',True)
  sample_sources_evenly = kw.get('sample_sources_evenly',True)
  nphot            = kw.get('nphot',1000000)
  n_iter           = kw.get('n_iter',10) # number of lucy iterations
  max_interactions = kw.get('max_interactions',100000000) # max interactions before photon is destroyed
  warn             = kw.get('warn',True) # print warning message when max_interaction is reached
  Tmin             = kw.get('Tmin',0.) # set minimum temperature
  
  # convergence
  percentile = kw.get('percentile',99.)
  absolute   = kw.get('absolute',2.)
  relative   = kw.get('relative',1.02)

  
  if len(args) == 1:
    cell = args[0]
  else:
    raise ValueError("Only one input supported at the moment")
  
  if not cell.full_oct:
    raise ValueError("Cell data needs to be sorted with full_oct option")
  
  # ------- Dust
  if Tsub > 0:
    d = SphericalDust('{}.hdf5'.format(dustname))
    d.set_sublimation_temperature(mode,temperature=Tsub)
    rstar = np.sqrt(np.sum(luminosity))/(Teff[0]/5778)**2 * cnst.R_sun.to('AU').value
    r_Tsub = 0.5*rstar/(Tsub/Teff[0])**2
  else:
    d = '{}.hdf5'.format(dustname)
    r_Tsub = 0

  # ------- Initialise model
  m = Model()
  
  # ------- Add source
#   if isinstance(luminosity,(list,np.ndarray)):
  for l,t in zip(luminosity,Teff):
    source = m.add_point_source()
    source.luminosity = l * cnst.L_sun.cgs.value
    source.temperature = t
    source.position = (0., 0., 0.)
#   else:
#     source = m.add_point_source()
#     source.luminosity  = luminosity * cnst.L_sun.cgs.value # [ergs/s]
#     source.temperature = Teff  # [K]
#     source.position    = (0., 0., 0.)
  
  # ------- Set up grid
  dx = cell.x_wall.to('cm')[[0,-1]].value
  dy = cell.y_wall.to('cm')[[0,-1]].value
  dz = cell.z_wall.to('cm')[[0,-1]].value
  
  x, y, z    = np.mean(dx), np.mean(dy), np.mean(dz)
  dx, dy, dz = np.diff(dx)[0]/2, np.diff(dy)[0]/2, np.diff(dz)[0]/2
  refined    = cell.amrtree.astype(np.bool)
  
  m.set_octree_grid(x, y, z, dx, dy, dz, refined)
  
  # ------- Density
  cellRadius = cell.radius.to('AU').value
  dens = cell.density.cgs.value
  dens[np.where(cellRadius <= r_Tsub)] = 0.
  print(cellRadius.min(),r_Tsub,(cellRadius <= r_Tsub).sum())
  
  density = np.zeros(refined.size,dtype=np.float64)
  density[~refined] = dens
  density *= densityFactor
  
  m.add_density_grid(density, d)
  
  # ------- RT settings
  # modified random walk
  if mrw_gamma >= 2:
    m.set_mrw(True, gamma=mrw_gamma)

  if use_pda:
    m.set_pda(True)
  
  if sample_sources_evenly:
    m.set_sample_sources_evenly(True)
  
  # Set number of photons
  m.set_n_photons(initial=nphot,imaging=1e4)

  m.set_n_initial_iterations(n_iter)
  m.set_convergence(True, percentile=percentile, absolute=absolute, relative=relative)
  m.set_max_interactions(max_interactions,warn=warn)
  
  if Tmin > 0:
    m.set_minimum_temperature(Tmin)
  
  # ------- logfile parameters
  log = [('name'   ,'hyperion'                    , "Model type"),\
         ('mname'  ,modelname                     , "Model name"),\
         ('dname'  ,dustname                      , "Dust name"),\
         ('Tsub'   ,Tsub                          , "Dust sublimation temperature"),\
         ('mode'   ,mode                          , "Dust sublimation mode"),\
         ('dfactor',densityFactor                 , "Model density multiplication factor"),\
         ('mrwgamma',mrw_gamma                   , "Modified Random Walk gamma parameter (<2 no MRW)"),\
         ('use_pda' ,use_pda                     , "Use PDA"),\
         ('sse'     ,sample_sources_evenly      , "Sample Sources Evenly"),\
         ('nphot'   ,nphot                       , "Number of photons"),\
         ('niter'   ,n_iter                      , "Initial number of iterations"),\
         ('mxinter' ,max_interactions            , "Maximum number of interactions before photon is destroyed"),\
         ('warn'     ,warn                        , "Issue warning when mxinter is reached"),\
         ('percent'  ,percentile                 , "Convergence"),\
         ('absolute'  ,absolute                   , "Convergence"),\
         ('relative'  ,relative                   , "Convergence"),\
         ('Tmin'      ,Tmin                       , "Minimum dust temperature")]
  i = 1
  for l,t in zip(luminosity,Teff):
    log.append(('lum{:d}'.format(i),l,"Luminosity (Lsun)"))
    log.append(('Teff{:d}'.format(i),t,"Effective temperature (K)"))
    i += 1
  
  writeParameters(log,filename='hyperionLog.fits',modeldir=modeldir,overwrite=overwrite)
  
  m.write(os.path.join(modeldir,'{:s}.rtin'.format(modelname)), overwrite=overwrite)
  
  return m

def writeParameters(parameters,**kw):
  """
  write important parameters, given as list of tuples with format (key,value,explanation)
  to the model directory. The parameters are saved as an empty fits file
  """
  # ------- Handle keywords
  filename     = kw.get('filename','log.fits')
  use_existing = kw.get('use_existing',False)
  overwrite    = kw.get('overwrite',False)
  modeldir     = kw.get('modeldir','.')
  
  from astropy.io import fits as pyfits

  if os.path.exists(os.path.join(modeldir,filename)):
    if use_existing:
      return
    elif not overwrite:
      raise IOError("{} already exists in modeldir, and overwrite or use_existing are not set".format(filename))

  hdu = pyfits.PrimaryHDU(None)

  for header in parameters:
    if len(header) == 2:
      key, value = header
      hdu.header[key] = value
    elif len(header) == 3:
      key, value, comment = header
      hdu.header[key] = value
      hdu.header.comments[key] = comment  
    else:
      raise ValueError("header must consist of (key, value) or (key, value, comment)")

  hdulist = pyfits.HDUList([hdu])
  hdulist.writeto(os.path.join(modeldir,filename),overwrite=True)