#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, absolute_import, division

from matplotlib.colors import LogNorm, Normalize
from matplotlib.patches import Rectangle, Ellipse
from matplotlib.offsetbox import AnchoredOffsetbox, AuxTransformBox, VPacker, TextArea, DrawingArea
from matplotlib import rc
from math import ceil, floor

import numpy as np
import astropy.units as unit

import matplotlib.pyplot as mp
import matplotlib.cm as cm

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

def plotMapCore(map,**kw):
  """
  Description:
    Function to plot density maps from RAMSES data
  Input:
    map  : pyramses Map instance (contains the map data)
  Keywords:
    sink : pyramses Sink instance for overplotting sink data (Default: None)
    ax   : matplotlib axes object if a given set of axes are desired (Default: None)
  """
  
  # ------- Handle keywords
  sink     = kw.get('sink',None)
  ax       = kw.get('ax',None)
  figwidth = kw.get('figwidth',12)
  unit_l   = kw.get('unit_l','pc')
  vmin     = kw.get('vmin',None)
  vmax     = kw.get('vmax',None)
  norm     = kw.get('norm','log')
  hideaxes = kw.get('hideaxes',False)
  filename = kw.get('filename',None)
  colorbar = kw.get('colorbar',False)
  xlabel   = kw.get('xlabel',None)
  ylabel   = kw.get('ylabel',None)
  clabel   = kw.get('clabel',None)
  dpc      = kw.get('dpc',None)
  scalebar = kw.get('scalebar',False)
  text     = kw.get('text',None)
  title    = kw.get('title',None)
  
  # ------- Constants
  cm2inch = 0.393701
  
  # ------- Figure axes
  if ax is None:
    figwidth *= cm2inch
    figheight = figwidth
    fig = mp.figure(figsize=(figwidth,figheight))
    ax  = fig.add_subplot(111)
  
  # ------- length unit
  if unit_l.lower() in ['pc','parsec']:
    extent = np.hstack((map.xlim.to('pc').value,map.ylim.to('pc').value))
  elif unit_l.lower() == 'au':
    extent = np.hstack((map.xlim.to('au').value,map.ylim.to('au').value))
  else:
    raise ValueError('{} is not a valid length unit'.format(unit_l))
  
  # ------- Density array
  density = map.columnDensity.cgs
  
  # ------- norm
  vmin = density.value.min() if vmin is None else vmin
  vmax = density.value.max() if vmax is None else vmax
  norm = LogNorm(vmin=vmin,vmax=vmax) if norm == 'log' else Normalize(vmin=vmin,vmax=vmax)
  
  # ------- plot map
  cb = ax.imshow(density.value,extent=extent,origin='lower',norm=norm)
  
  # ------- Add sink particles to map
  if sink is not None:
    sink.reference = map.reference # set sink reference position equal to map reference position
    # ----- Set sink x and y coordinates depending on the projection direction
    if map.dir == 'x':
      sinkx = sink.pos[:,1]
      sinky = sink.pos[:,2]
      sinkz = sink.pos[:,0]
    elif map.dir == 'y':
      sinkx = sink.pos[:,2]
      sinky = sink.pos[:,0]
      sinkz = sink.pos[:,1]
    elif map.dir == 'z':
      sinkx = sink.pos[:,0]
      sinky = sink.pos[:,1]
      sinkz = sink.pos[:,2]
    else:
      raise ValueError('{} is not a recognized projection direction'.format(map.dir))
    
    
    # ---- Only take sinks inside the plotted region
    sinkIndex = (sinkx >= map.xlim[0]) & (sinkx <= map.xlim[1]) & \
                (sinky >= map.ylim[0]) & (sinky <= map.ylim[1]) & \
                (sinkz >= map.zlim[0]) & (sinkz <= map.zlim[1])
    
    # ---- Convert units
    if unit_l.lower() in ['pc','parsec']:
      sinkx = sinkx.to('pc').value
      sinky = sinky.to('pc').value
    elif unit_l.lower() == 'au':
      sinkx = sinkx.to('au').value
      sinky = sinky.to('au').value
    else:
      raise ValueError('{} is not a valid length unit'.format(unit_l))
    
    # ---- plot
#     ax.scatter(sinkx[sinkIndex],sinky[sinkIndex],s=np.pi*0.5**2,c='C1')
    ax.scatter(sinkx[sinkIndex],sinky[sinkIndex],s=np.pi*0.3**2,c='white',edgecolor='black')
    
    # ---- reset axis limits (may have been altered by scatter)
    ax.set_xlim(left=extent[0],right=extent[1])
    ax.set_ylim(bottom=extent[2],top=extent[3])
    
  # ------- hide axes
  if hideaxes:
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)
  
  # ------- add colorbar
  if colorbar:
    cax = fig.add_axes([0.95, 0.15, 0.02, 0.7])
    fig.colorbar(cb,cax=cax,orientation='vertical')
    if isinstance(clabel,str):
      cax.set_ylabel(clabel)
  
  # ------- labelx
  if isinstance(xlabel,str):
    ax.set_xlabel(xlabel)
  if isinstance(ylabel,str):
    ax.set_ylabel(ylabel)
  
  # ------- scale bar
  if scalebar:
    add_sizebar(ax,unit_l,color='white',dpc=dpc)
  
  # ------- text
  if text is not None:
    #props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
    props = dict()
    ax.text(0.05, 0.95, text, transform=ax.transAxes, fontsize=14,
          verticalalignment='top', bbox=props,color='white')
  
  # ------- title
  if title is not None:
    ax.set_title(title)
  
  if filename is None:
    return ax, cb
  else:
    mp.savefig(filename,bbox_inches='tight')
    mp.close()

#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#- Matplotlib auxiliaries #-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-
class AnchoredSizeBar(AnchoredOffsetbox):
  def __init__(self, transform, size, label, loc,
               pad=0.1, borderpad=0.1, sep=2, prop=None, frameon=True, color='white'):
      """
      Draw a horizontal bar with the size in data coordinate of the give axes.
      A label will be drawn underneath (center-aligned).

      pad, borderpad in fraction of the legend font size (or prop)
      sep in points.
      """
      self.size_bar = AuxTransformBox(transform)
      self.size_bar.add_artist(Rectangle((0,0), size, 0, fc="none",edgecolor=color))

      self.txt_label = TextArea(label, minimumdescent=False,textprops=dict(color=color))

      self._box = VPacker(children=[self.txt_label,self.size_bar],
                          align="center",
                          pad=0, sep=sep)

      AnchoredOffsetbox.__init__(self, loc, pad=pad, borderpad=borderpad,
                                 child=self._box,
                                 prop=prop,
                                 frameon=frameon)

def add_sizebar(ax,unit_l,color='white',dpc=None):
  """
  add sizebar to axes. It calculates the length scale automatically
  """
  
  lunit = 'AU' if unit_l == 'arcsec' else unit_l
  
  # distance between tick marks
  def f(axis):
    l = axis.get_majorticklocs()
    return len(l)>1 and (l[1] - l[0])

  dx    = f(ax.xaxis)*dpc if unit_l == 'arcsec' else f(ax.xaxis) # distance between tick marks in AU
  dx    = round(dx,-int(floor(np.log10(dx)))) # round to nearest "nice number" eg. 10, 20, 200, 3000
  dx    = int(dx) if dx >= 1 else dx
  if type(dx) is int:
    label = "%i %s" % (dx,lunit) # create nice label string
  else:
    label = "%.1f %s" % (dx,lunit) # create nice label string
  dx      = dx/dpc if unit_l == 'arcsec' else dx

  asb = AnchoredSizeBar(ax.transData, dx, label, loc=4, pad=0.2, borderpad=0.2, sep=5,
                        frameon=False,color=color)
  ax.add_artist(asb)
