#!/usr/bin/env python
# -*- coding: utf-8 -*-

rsun2cm            = 6.955e10
cm2rsun            = 1/rsun2cm

au_to_cm           = 1.49597871e13
au2cm              = au_to_cm
cm_to_au           = 1./au_to_cm
cm2au              = cm_to_au

parsec_to_cm       = 3.08567758e18
parsec2cm          = parsec_to_cm
pc2cm              = parsec_to_cm
cm_to_parsec       = 1./parsec_to_cm
cm2parsec          = cm_to_parsec
cm2pc              = cm_to_parsec

parsec_to_meter    = 3.08567758e16
parsec2m           = parsec_to_meter
pc2m               = parsec_to_meter
meter_to_parsec    = 1./parsec_to_meter
m2parsec           = meter_to_parsec
m2pc               = meter_to_parsec

parsec_to_au       = 2.06264806e5
parsec2au          = parsec_to_au
pc2au              = parsec_to_au
au_to_parsec       = 1./parsec_to_au
au2parsec          = au_to_parsec
au2pc              = au_to_parsec

solar_mass_to_kg   = 1.9891e30
msun_to_kg         = solar_mass_to_kg
msun2kg            = solar_mass_to_kg
kg_to_solar_mass   = 1./solar_mass_to_kg
kg_to_msun         = kg_to_solar_mass
kg2msun            = kg_to_solar_mass

solar_mass_to_g    = 1.9891e33
msun_to_g          = solar_mass_to_g
msun2g             = solar_mass_to_g
g_to_solar_mass    = 1./solar_mass_to_g
g_to_msun          = g_to_solar_mass
g2msun             = g_to_solar_mass

second_to_year     = 3.16887646e-8
s2yr               = second_to_year
s2kyr              = second_to_year/1000
s2Myr              = second_to_year*1e-6
year_to_second     = 1./second_to_year
yr2s               = year_to_second
kyr2s              = year_to_second*1000
Myr2s              = year_to_second*1e6

inch_to_cm         = 2.54
inch2cm            = inch_to_cm
cm_to_inch         = 1./inch_to_cm
cm2inch            = cm_to_inch

radian_to_arcsec   = 206264.806
rad2arcsec         = radian_to_arcsec
rad2as             = radian_to_arcsec
arcsec_to_radian   = 4.84813681e-6
arcsec2rad         = arcsec_to_radian
as2rad             = arcsec_to_radian

molecular_mass     = 2.37e0
unit_mass_in_g     = 1.660538922e-24
unit_mass_in_kg    = 1.660538922e-27
brown_dwarf_limit_in_msun = 0.07
massive_star_limit_in_msun = 8.
massdens2numdens   = 1./unit_mass_in_g/molecular_mass
numdens2massdens   = unit_mass_in_g*molecular_mass