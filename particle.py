#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function, absolute_import, division

import numpy as np
import os

from .errors import check_sentinels

def rd_particle(nout, **kw):
  
  # ------- Handle keywords
  main_directory = kw.get('main_directory','.') # alias
  datadir = kw.get('datadir',main_directory)
  icpu    = kw.get('icpu',1)
  verbose = kw.get('verbose',False)
  
  npart_tot, mstar_tot = 0, 0. # initialize values
  npartp_list = []
  
  # ------- Read the first file header to get information such as the number of cpus
  #         and hydro variables
  filename = os.path.join(datadir,'output_{0:05d}/part_{0:05d}.out{1:05d}'.format(nout,icpu))
  
  ncpu, ndim, npartp, nstar, mstar, nsink = rd_header(filename) # read header
  # ------- print stuff
  if verbose:
    print(filename)
    print('ncpu   = ', ncpu)
    print('ndim   = ',ndim)
    print('npartp = ',npartp)
    print('nstar  = ',nstar)
    print('mstar  = ',mstar)
    print('nsink  = ',nsink)
  
  ntracer = -nsink
  npart_tot += npartp
  mstar_tot += mstar
  npartp_list.append(npartp)
  
  # ------- Read the headers of the remaining files to get info about the total number
  #         of particles
  for i in range(icpu+1,ncpu+1):
    filename = os.path.join(datadir,'output_{0:05d}/part_{0:05d}.out{1:05d}'.format(nout,i))
  
    ncpu, ndim, npartp, nstar, mstar, nsink = rd_header(filename) # read header
    if verbose: #print stuff
      print(filename)
      print('ncpu   = ',ncpu)
      print('ndim   = ',ndim)
      print('npartp = ',npartp)
      print('nstar  = ',nstar)
      print('mstar  = ',mstar)
      print('nsink  = ',nsink)

    npart_tot += npartp
    mstar_tot += mstar
    npartp_list.append(npartp)
  
  if verbose:
    print('Total number of particles = ',npart_tot)
  
  # ------- Initialize dict
  result = dict(npart=npart_tot,ntracer=ntracer,ndim=ndim,
                xp=np.empty((npart_tot,3),dtype='f8'),
                vp=np.empty((npart_tot,3),dtype='f8'),
                mp=np.empty(npart_tot,dtype='f8'),
                id=np.empty(npart_tot,dtype='i4'),
                level=np.empty(npart_tot,dtype='i4'),
                up=np.empty((npart_tot,ntracer),dtype='f8'))
  
  ind_end   = 0
  ind_begin = 0
  # ------- Start reading data for real
  for i,npartp in zip(range(icpu,ncpu+1),npartp_list):
    filename = os.path.join(datadir,'output_{0:05d}/part_{0:05d}.out{1:05d}'.format(nout,i))
    with open(filename, mode='rb') as f:
      
      # ------- skip header
      f.seek(116,0)
      
      # ------- read position, velocity and mass
      dt = np.dtype([('sentinel1', 'u4'), ('field', 'f8', (npartp,)), ('sentinel2', 'u4')])
      output = np.fromfile(f,dtype=dt,count=7)
      for s1, s2 in zip(output['sentinel1'],output['sentinel2']):
        void = check_sentinels(s1,s2)
      position = output['field'].T[:,0:3]
      velocity = output['field'].T[:,3:6]
      mass     = output['field'].T[:,6] 
      
      # ------- read particle id and level data
      dt = np.dtype([('sentinel1', 'u4'), ('field', 'i4', (npartp,)), ('sentinel2', 'u4')])
      output = np.fromfile(f,dtype=dt,count=2)
      for s1, s2 in zip(output['sentinel1'],output['sentinel2']):
        void = check_sentinels(s1,s2)
      id    = output['field'][0,:]
      level = output['field'][1,:]
      
      # ------- read stars
      if nstar > 0:
        raise NotImplementedError("Not yet implemented. See rd_part.pro for instructions")
      
      # ------- read hydro variables
      dt = np.dtype([('sentinel1', 'u4'), ('field', 'f8', (npartp,)), ('sentinel2', 'u4')])
      output = np.fromfile(f,dtype=dt,count=ntracer)
      for s1, s2 in zip(output['sentinel1'],output['sentinel2']):
        void = check_sentinels(s1,s2)
      up = output['field'].T
    
    ind_end += npartp
    result['xp'][ind_begin:ind_end,:]  = position
    result['vp'][ind_begin:ind_end,:]  = velocity
    result['mp'][ind_begin:ind_end]    = mass
    result['id'][ind_begin:ind_end]    = id
    result['level'][ind_begin:ind_end] = level
    result['up'][ind_begin:ind_end,:]  = up
    ind_begin += npartp
  
  return result

def rd_header(filename):

  #open file
  with open(filename, mode='rb') as f:
    
    # ------- read header begin
    #### first
    dt = np.dtype('u4, i4, u4')
    sentinel1, ncpu, sentinel2 = np.fromfile(f,dtype=dt,count=1)[0]
#     print(sentinel1, ncpu, sentinel2)
    void = check_sentinels(sentinel1,sentinel2)
    
    #### second
    dt = np.dtype('u4, i4, u4')
    sentinel1, ndim, sentinel2 = np.fromfile(f,dtype=dt,count=1)[0]
#     print(sentinel1, ndim, sentinel2)
    void = check_sentinels(sentinel1,sentinel2)

    #### third
    dt = np.dtype('u4, i4, u4')
    sentinel1, npartp, sentinel2 = np.fromfile(f,dtype=dt,count=1)[0]
#     print(sentinel1, npartp, sentinel2)
    void = check_sentinels(sentinel1,sentinel2)
    
    #### fourth
    dt = np.dtype('u4, i4, i4, i4, i4, u4')
    sentinel1, void1, void2, void3, void4, sentinel2 = np.fromfile(f,dtype=dt,count=1)[0]
#     print(sentinel1, void1, void2, void3, void4, sentinel2)
    void = check_sentinels(sentinel1,sentinel2)
    
    #### fifth
    dt = np.dtype('u4, i4, u4')
    sentinel1, nstar, sentinel2 = np.fromfile(f,dtype=dt,count=1)[0]
#     print(sentinel1, nstar, sentinel2)
    void = check_sentinels(sentinel1,sentinel2)
    
    #### sixth
    dt = np.dtype('u4, f8, u4')
    sentinel1, mstar, sentinel2 = np.fromfile(f,dtype=dt,count=1)[0]
#     print(sentinel1, mstar, sentinel2)
    void = check_sentinels(sentinel1,sentinel2)
    
    #### seventh
    dt = np.dtype('u4, i4, i4, u4')
    sentinel1, void1, void2, sentinel2 = np.fromfile(f,dtype=dt,count=1)[0]
#     print(sentinel1, void1, void2, sentinel2)
    void = check_sentinels(sentinel1,sentinel2)
    
    #### eight
    dt = np.dtype('u4, i4, u4')
    sentinel1, nsink, sentinel2 = np.fromfile(f,dtype=dt,count=1)[0]
#     print(sentinel1, nsink, sentinel2)
    void = check_sentinels(sentinel1,sentinel2)
  
  return ncpu, ndim, npartp, nstar, mstar, nsink